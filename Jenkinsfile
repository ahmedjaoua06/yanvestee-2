pipeline {
    agent {
        kubernetes {
            label "nodejs"
            inheritFrom "nodejs-sonar-template" 
        }
    }
    options { 
        disableConcurrentBuilds() 
        buildDiscarder(logRotator(daysToKeepStr: '10', numToKeepStr: '5', artifactNumToKeepStr: '1'))
    }

    stages {
        stage('Check node') { 
            steps {
                container('nodejs') {
                    sh 'node --version'
                }
            }
        }

        stage('npm install') {  
            steps {
                container('nodejs') {
                    sh '''
                    npm install
                    npm install -g @angular/cli
                    '''
                }
            }
        }
        
        stage('Quality Analysis') {
        when {
                branch 'develop'
            }

            steps {
                container('sonar-scanner') {
                    withSonarQubeEnv('sonarqube') {
                        sh """
                        sonar-scanner \
                        -D sonar.projectKey=Inspinia-New-front \
                        -D sonar.projectName=Inspinia-New-front \
                        -D sonar.branch=${env.BRANCH_NAME} \
                        -D sonar.language=ts \
                        -D sonar.projectBaseDir=./ \
                        -D sonar.tests=test \
                        -D sonar.javascript.lcov.reportPaths=coverage/lcov.info \
                        || exit 0
                        """
                    }
                }
            }
        }


        stage('npm build') { 
            steps {
                container('nodejs') {
                    sh "ng build --output-path=./docker/dist"
                }
            }
        }

        stage('Build docker') { 
            steps {
                container('docker') {
                    sh "docker build -t nexus-registry.sintegralabs.net/inspinia-frontend:${env.BRANCH_NAME} ."
                }
            }
        }

        stage('Push docker') { 
            steps {
                container('docker') {
                    withDockerRegistry([credentialsId: 'jenkins', url: 'https://nexus-registry.sintegralabs.net']) {
                        sh "docker push nexus-registry.sintegralabs.net/inspinia-frontend:${env.BRANCH_NAME}"
                    }
                    //sh "docker rmi nexus-registry.sintegralabs.net/inspinia-frontend:${env.BRANCH_NAME}" // Clean de l'image generé
                }
            }
        }
    }
}
