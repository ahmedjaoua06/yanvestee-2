export class User {
    _id: number;
    email: string;
    firstname: string;
    lastname: string;
    tel: number;
    mobile: number;
    role: string;
    categorie: string;
    fonction: String;
}