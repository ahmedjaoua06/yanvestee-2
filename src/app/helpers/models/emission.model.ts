

export class Emission {
    emetteurId: string;
    type : string;
    minimumAmount : Number;
    minimumRate : Number;
    placementPeriod : Number;
    creationDate: Date
    updateDate: Date;
}
