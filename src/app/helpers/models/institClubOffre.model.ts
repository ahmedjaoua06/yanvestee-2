
export class InstitClubOffre {
    amount: Number;
    type: Number[];
    period: Number;
    availableDate: Date;
    blacklist: String[];
    investorId: String;
    creationDate: Date;
    updateDate: Date;
}
