export class Constants {
    //Default image user
    static default_image_user = "assets/images/avatars/Velazquez.jpg";
    static default_image_user_em = "assets/images/avatars/em.jpg";
    static default_image_user_iv = "assets/images/avatars/iv.png";
    //Default Value Emetteur
    static emetteur = "Emetteur";
    //Default Value Investor
    static investor = "Investisseur";
    //Role User
    static roleUser = [
        {
            id: 1,
            label: "Investisseur",
            value: "Investisseur"
        },
        {
            id: 2,
            label: "Émetteur",
            value: "Emetteur"
        }
    ];
    //Category User
    static categoriesUser = [
        {
            id: 1,
            parent: 2,
            label: "Banque",
            value: "Banque"
        },
        {
            id: 2,
            parent: 2,
            label: "Leasing",
            value: "Leasing, factoring"
        },
        {
            id: 3,
            parent: 2,
            label: "IMF",
            value: "IMF"
        },
        {
            id: 4,
            parent: 2,
            label: "Conseil",
            value: "Conseil"
        },
        {
            id: 5,
            parent: 2,
            label: "Corporate",
            value: "Corporates"
        },
        {
            id: 6,
            parent: 1,
            label: "Asset Manager",
            value: "Asset Managers"
        },
        {
            id: 7,
            parent: 1,
            label: "Assurance",
            value: "Assurance"
        },
        {
            id: 8,
            parent: 1,
            label: "Avertis",
            value: "Avertis"
        },
        {
            id: 9,
            parent: 1,
            label: "Corporate",
            value: "Corporates"
        },
        {
            id: 10,
            parent: 1,
            label: "Personne physique",
            value: "Public"
        }
    ]
    //Email Pattern
    static emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
    //Type Listing
    static TypeListing = [
        { label: "CD", value: "Certificat de dépôt" },
        { label: "EO", value: "Emprunt obligataire" },
        { label: "BT", value: "Billets de trésorerie" },
        { label: "CT", value: "Comptes à terme" },
        { label: "PL", value: "Pension livrée" }
    ]
    //List Horizon
    static ListHorizon = [
        {
            label: "Court terme",
            value: 1
        },
        {
            label: "Long terme",
            value: 2
        },
    ]
    //List Period Horizon
    static ListHorizonPeriod = [
        {
            horizonId: 1,
            label: 3,
            value: 3
        },
        {
            horizonId: 1,
            label: 6,
            value: 6
        },
        {
            horizonId: 1,
            label: 9,
            value: 9
        },
        {
            horizonId: 1,
            label: 12,
            value: 12
        },
        {
            horizonId: 2,
            label: 24,
            value: 24
        },
        {
            horizonId: 2,
            label: 36,
            value: 36
        },
        {
            horizonId: 2,
            label: 48,
            value: 48
        },
        {
            horizonId: 2,
            label: 60,
            value: 60
        },
    ]
    //List Anticipated termination
    static ListAnticipatedTermination = [
        {
            label: "Oui",
            value: 1
        },
        {
            label: "Non",
            value: 0
        }
    ]
    //List Payment of interests
    static ListPaymentInterest = [
        {
            label: "A maturité",
            value: 1
        },
        {
            label: "A la souscription",
            value: 2
        },
        {
            label: "A l'echeance ",
            value: 3
        }
    ]
    //List Categories Contact
    static ListCategoriesContact = [
        {
            label: "Suspect",
            value: "suspect"
        },
        {
            label: "Prospect",
            value: "prospect"
        },
        {
            label: "Lead",
            value: "lead"
        },
        {
            label: "Client",
            value: "client"
        },
    ]
    //List Taxation of interests
    static ListTaxationInter = [
        {
            label: "A maturité",
            value: 1
        },
        {
            label: "A la souscription",
            value: 2
        },
    ]
    //List Subscr Option
    static ListSubscOption = [
        {
            label: "Personne physique",
            value: 1
        },
        {
            label: "Personne morale",
            value: 2
        }
    ]
    //List Tri IC
    static ListTriIc = [
        {
            label: 'Montant',
            value: 1
        },
        {
            label: 'Durée',
            value: 2
        },
        {
            label: 'Date valeur',
            value: 3
        }
    ]

    //List Type
    static ListTypeCard = [
        {
            label: "Compte à terme",
            value: 1
        },
        {
            label: "Certificat de Dépot",
            value: 2
        },
        {
            label: "Billet trésorerie",
            value: 3
        },
        {
            label: "Pension livrée",
            value: 4
        }
    ]
    //Status Echeance
    static ListEcheance = [
        {
            label: "Tout",
            value: undefined
        },
        {
            label: "En cours",
            value: 1
        },
        {
            label: "Clôturé",
            value: 0
        },
    ]

    static ListSecteur = [
        {
            label: "Agroalimentaire",
            value: "Agroalimentaire"
        },
        {
            label: "Assets Management",
            value: "assetsmanagement"
        },
        {
            label: "Assurance",
            value: "Assurance"
        },
        {
            label: "Banque",
            value: "Banque"
        },

        {
            label: "Bois / Papier / Carton / Imprimerie",
            value: "Bois / Papier / Carton / Imprimerie"
        },
        {
            label: "BTP / Matériaux de construction",
            value: "BTP / Matériaux de construction"
        },
        {
            label: "Chimie / Parachimie",
            value: "Chimie / Parachimie"
        },
        {
            label: "Commerce / Négoce / Distribution",
            value: "Commerce / Négoce / Distribution"
        },
        {
            label: "Conseils Financiers",
            value: "conseil"
        },
        {
            label: "Édition / Communication / Multimedia",
            value: "Édition / Communication / Multimedia"
        },
        {
            label: "Électronique / Électricité",
            value: "Électronique / Électricité"
        },
        {
            label: "Études et conseils",
            value: "Études et conseils"
        },
        {
            label: "Industrie pharmaceutique",
            value: "Industrie pharmaceutique"
        },
        {
            label: "Informatique / Télécoms",
            value: "Informatique / Télécoms"
        },
        {
            label: "Leasing",
            value: "Leasing"
        },
        {
            label: "Machines et équipements / Automobile",
            value: "Machines et équipements / Automobile"
        },
        {
            label: "Métallurgie / Travail du métal",
            value: "Métallurgie / Travail du métal"
        },
        {
            label: "Micro finance",
            value: "Micro finance"
        },
        {
            label: "Plastique / Caoutchouc",
            value: "Plastique / Caoutchouc"
        },
        {
            label: "Services aux entreprises",
            value: "Services aux entreprises"
        },
        {
            label: "Textile / Habillement / Chaussure",
            value: "Textile / Habillement / Chaussure"
        },
        {
            label: "Transport / Logistique",
            value: "Transport / Logistique"
        },
        {
            label: "Autre",
            value: "autre"
        }
    ]

    static ListJuridicForm = [
        {
            label: "SA",
            value: "SA"
        },
        {
            label: "SARL",
            value: "SARL"
        },
        {
            label: "SUARL",
            value: "SUARL"
        },
        {
            label: "Autres",
            value: "Autres"
        }

    ]

    static ListRatingForm = [
        {
            label: "Fitch",
            value: "Fitch",
            ratingLong: [
                //5 stars
                {
                    label: "AAA",
                    value: "AAA",
                    index: 0,
                    grade: "Grade Investissement",
                    qualité: "De premier ordre",
                    stars: 5
                },
                {
                    label: "AA+",
                    value: "AA+",
                    index: 1,
                    grade: "Grade Investissement",
                    qualité: "Haute qualité",
                    stars: 5
                },
                {
                    label: "AA",
                    value: "AA",
                    index: 2,
                    grade: "Grade Investissement",
                    qualité: "Haute qualité",
                    stars: 5
                },
                {
                    label: "AA-",
                    value: "AA-",
                    index: 3,
                    grade: "Grade Investissement",
                    qualité: "Haute qualité",
                    stars: 5
                },
                {
                    label: "A+",
                    value: "A+",
                    index: 4,
                    grade: "Grade Investissement",
                    qualité: "Qualité moyenne",
                    stars: 5
                },
                //4 stars
                {
                    label: "A",
                    value: "A",
                    index: 5,
                    grade: "Grade Investissement",
                    qualité: "Qualité moyenne",
                    stars: 4
                },
                {
                    label: "A-",
                    value: "A-",
                    index: 6,
                    grade: "Grade Investissement",
                    qualité: "Qualité moyenne",
                    stars: 4
                },
                {
                    label: "BBB+",
                    value: "BBB+",
                    index: 7,
                    grade: "Grade Investissement",
                    qualité: "Qualité moyenne inférieure",
                    stars: 4
                },
                {
                    label: "BBB",
                    value: "BBB",
                    index: 8,
                    grade: "Grade Investissement",
                    qualité: "Qualité moyenne inférieure",
                    stars: 4
                },
                {
                    label: "BBB-",
                    value: "BBB-",
                    index: 9,
                    grade: "Grade Investissement",
                    qualité: "Qualité moyenne inférieure",
                    stars: 4
                },
                //3 stars
                {
                    label: "BB+",
                    value: "BB+",
                    index: 10,
                    grade: "Grade Spéculatif",
                    qualité: "Spéculatif",
                    stars: 3
                },
                {
                    label: "BB",
                    value: "BB",
                    index: 11,
                    grade: "Grade Spéculatif",
                    qualité: "Spéculatif",
                    stars: 3
                },
                {
                    label: "BB-",
                    value: "BB-",
                    index: 12,
                    grade: "Grade Spéculatif",
                    qualité: "Spéculatif",
                    stars: 3
                },
                {
                    label: "B+",
                    value: "B+",
                    index: 13,
                    grade: "Grade Spéculatif",
                    qualité: "Hautement spéculatif",
                    stars: 3
                },
                //2 stars
                {
                    label: "B",
                    value: "B",
                    index: 14,
                    grade: "Grade Spéculatif",
                    qualité: "Hautement spéculatif",
                    stars: 2
                },
                {
                    label: "B-",
                    value: "B-",
                    index: 15,
                    grade: "Grade Spéculatif",
                    qualité: "Hautement spéculatif",
                    stars: 2
                },
                {
                    label: "CCC+",
                    value: "CCC+",
                    index: 16,
                    grade: "Grade Spéculatif",
                    qualité: "Risques élevés",
                    stars: 2
                },
                {
                    label: "CCC",
                    value: "CCC",
                    index: 17,
                    grade: "Grade Spéculatif",
                    qualité: "Risques élevés",
                    stars: 2
                },
                {
                    label: "CCC-",
                    value: "CCC-",
                    index: 18,
                    grade: "Grade Spéculatif",
                    qualité: "Risques élevés",
                    stars: 2
                },
                //1 star
                {
                    label: "CC",
                    value: "CC",
                    index: 19,
                    grade: "Grade Spéculatif",
                    qualité: "Ultra spéculatif",
                    stars: 1
                },
                {
                    label: "C",
                    value: "C",
                    index: 20,
                    grade: "Grade Spéculatif",
                    qualité: "Faibles perspectives",
                    stars: 1

                },
                {
                    label: "SD",
                    value: "SD",
                    index: 21,
                    grade: "Grade Spéculatif",
                    qualité: "En défaut",
                    stars: 1

                },
                {
                    label: "D",
                    value: "D",
                    index: 22,
                    grade: "Grade Spéculatif",
                    qualité: "",
                    stars: 1

                }
            ],
            ratingCourt: [{
                label: "F1+",
                value: "F1+"
            },
            {
                label: "F1",
                value: "F1"
            }]
        },
        {
            label: "S&P",
            value: "S&P",
            ratingLong: [
                //5 stars
                {
                    label: "AAA",
                    value: "AAA",
                    index: 0,
                    grade: "Grade Investissement",
                    qualité: "De premier ordre",
                    stars: 5
                },
                {
                    label: "AA+",
                    value: "AA+",
                    index: 1,
                    grade: "Grade Investissement",
                    qualité: "Haute qualité",
                    stars: 5
                },
                {
                    label: "AA",
                    value: "AA",
                    index: 2,
                    grade: "Grade Investissement",
                    qualité: "Haute qualité",
                    stars: 5
                },
                {
                    label: "AA-",
                    value: "AA-",
                    index: 3,
                    grade: "Grade Investissement",
                    qualité: "Haute qualité",
                    stars: 5
                },
                {
                    label: "A+",
                    value: "A+",
                    index: 4,
                    grade: "Grade Investissement",
                    qualité: "Qualité moyenne",
                    stars: 5
                },
                //4 stars
                {
                    label: "A",
                    value: "A",
                    index: 5,
                    grade: "Grade Investissement",
                    qualité: "Qualité moyenne",
                    stars: 4
                },
                {
                    label: "A-",
                    value: "A-",
                    index: 6,
                    grade: "Grade Investissement",
                    qualité: "Qualité moyenne",
                    stars: 4
                },
                {
                    label: "BBB+",
                    value: "BBB+",
                    index: 7,
                    grade: "Grade Investissement",
                    qualité: "Qualité moyenne inférieure",
                    stars: 4
                },
                {
                    label: "BBB",
                    value: "BBB",
                    index: 8,
                    grade: "Grade Investissement",
                    qualité: "Qualité moyenne inférieure",
                    stars: 4
                },
                {
                    label: "BBB-",
                    value: "BBB-",
                    index: 9,
                    grade: "Grade Investissement",
                    qualité: "Qualité moyenne inférieure",
                    stars: 4
                },
                //3 stars
                {
                    label: "BB+",
                    value: "BB+",
                    index: 10,
                    grade: "Grade Spéculatif",
                    qualité: "Spéculatif",
                    stars: 3
                },
                {
                    label: "BB",
                    value: "BB",
                    index: 11,
                    grade: "Grade Spéculatif",
                    qualité: "Spéculatif",
                    stars: 3
                },
                {
                    label: "BB-",
                    value: "BB-",
                    index: 12,
                    grade: "Grade Spéculatif",
                    qualité: "Spéculatif",
                    stars: 3
                },
                {
                    label: "B+",
                    value: "B+",
                    index: 13,
                    grade: "Grade Spéculatif",
                    qualité: "Hautement spéculatif",
                    stars: 3
                },
                //2 stars
                {
                    label: "B",
                    value: "B",
                    index: 14,
                    grade: "Grade Spéculatif",
                    qualité: "Hautement spéculatif",
                    stars: 2
                },
                {
                    label: "B-",
                    value: "B-",
                    index: 15,
                    grade: "Grade Spéculatif",
                    qualité: "Hautement spéculatif",
                    stars: 2
                },
                {
                    label: "CCC+",
                    value: "CCC+",
                    index: 16,
                    grade: "Grade Spéculatif",
                    qualité: "Risques élevés",
                    stars: 2
                },
                {
                    label: "CCC",
                    value: "CCC",
                    index: 17,
                    grade: "Grade Spéculatif",
                    qualité: "Risques élevés",
                    stars: 2
                },
                {
                    label: "CCC-",
                    value: "CCC-",
                    index: 18,
                    grade: "Grade Spéculatif",
                    qualité: "Risques élevés",
                    stars: 2
                },
                //1 star
                {
                    label: "CC",
                    value: "CC",
                    index: 19,
                    grade: "Grade Spéculatif",
                    qualité: "Ultra spéculatif",
                    stars: 1
                },
                {
                    label: "C",
                    value: "C",
                    index: 20,
                    grade: "Grade Spéculatif",
                    qualité: "Faibles perspectives",
                    stars: 1

                },
                {
                    label: "SD",
                    value: "SD",
                    index: 21,
                    grade: "Grade Spéculatif",
                    qualité: "En défaut",
                    stars: 1

                },
                {
                    label: "D",
                    value: "D",
                    index: 22,
                    grade: "Grade Spéculatif",
                    qualité: "",
                    stars: 1

                }
            ],
            ratingCourt: [{
                label: "F1+",
                value: "F1+"
            },
            {
                label: "F1",
                value: "F1"
            }]
        },
        {
            label: "Moody's",
            value: "Moody's",
            ratingLong: [
                //5 stars
                {
                    label: "Aaa",
                    value: "Aaa",
                    index: 0,
                    grade: "Grade Investissement",
                    qualité: "De premier ordre",
                    stars: 5
                },
                {
                    label: "Aa1",
                    value: "Aa1",
                    index: 1,
                    grade: "Grade Investissement",
                    qualité: "Haute qualité",
                    stars: 5
                },
                {
                    label: "Aa2",
                    value: "Aa2",
                    index: 2,
                    grade: "Grade Investissement",
                    qualité: "Haute qualité",
                    stars: 5
                },
                {
                    label: "Aa3",
                    value: "Aa3",
                    index: 3,
                    grade: "Grade Investissement",
                    qualité: "Haute qualité",
                    stars: 5
                },
                {
                    label: "A1",
                    value: "A1",
                    index: 4,
                    grade: "Grade Investissement",
                    qualité: "Qualité moyenne",
                    stars: 5
                },
                //4 stars
                {
                    label: "A2",
                    value: "A2",
                    index: 5,
                    grade: "Grade Investissement",
                    qualité: "Qualité moyenne",
                    stars: 4
                },
                {
                    label: "A3",
                    value: "A3",
                    index: 6,
                    grade: "Grade Investissement",
                    qualité: "Qualité moyenne",
                    stars: 4
                },
                {
                    label: "Baa1",
                    value: "Baa1",
                    index: 7,
                    grade: "Grade Investissement",
                    qualité: "Qualité moyenne inférieure",
                    stars: 4
                },
                {
                    label: "Baa2",
                    value: "Baa2",
                    index: 8,
                    grade: "Grade Investissement",
                    qualité: "Qualité moyenne inférieure",
                    stars: 4
                },
                {
                    label: "Baa3",
                    value: "Baa3",
                    index: 9,
                    grade: "Grade Investissement",
                    qualité: "Qualité moyenne inférieure",
                    stars: 4
                },
                //3 stars
                {
                    label: "Ba1",
                    value: "Ba1",
                    index: 10,
                    grade: "Grade Spéculatif",
                    qualité: "Spéculatif",
                    stars: 3
                },
                {
                    label: "Ba2",
                    value: "Ba2",
                    index: 11,
                    grade: "Grade Spéculatif",
                    qualité: "Spéculatif",
                    stars: 3
                },
                {
                    label: "Ba3",
                    value: "Ba3",
                    index: 12,
                    grade: "Grade Spéculatif",
                    qualité: "Spéculatif",
                    stars: 3
                },
                {
                    label: "B1",
                    value: "B1",
                    index: 13,
                    grade: "Grade Spéculatif",
                    qualité: "Hautement spéculatif",
                    stars: 3
                },
                //2 stars
                {
                    label: "B2",
                    value: "B2",
                    index: 14,
                    grade: "Grade Spéculatif",
                    qualité: "Hautement spéculatif",
                    stars: 2
                },
                {
                    label: "B3",
                    value: "B3",
                    index: 15,
                    grade: "Grade Spéculatif",
                    qualité: "Hautement spéculatif",
                    stars: 2
                },
                {
                    label: "Caa1",
                    value: "Caa1",
                    index: 16,
                    grade: "Grade Spéculatif",
                    qualité: "Risques élevés",
                    stars: 2
                },
                {
                    label: "Caa2",
                    value: "Caa2",
                    index: 17,
                    grade: "Grade Spéculatif",
                    qualité: "Risques élevés",
                    stars: 2
                },
                {
                    label: "Caa3",
                    value: "Caa3",
                    index: 18,
                    grade: "Grade Spéculatif",
                    qualité: "Risques élevés",
                    stars: 2
                },
                //1 stars
                {
                    label: "Ca",
                    value: "Ca",
                    index: 19,
                    grade: "Grade Spéculatif",
                    qualité: "Ultra spéculatif",
                    stars: 1
                },
                {
                    label: "C",
                    value: "C",
                    index: 20,
                    grade: "Grade Spéculatif",
                    qualité: "Faibles perspectives",
                    stars: 1
                }
            ],
            ratingCourt: [{
                label: "Prime 1",
                value: "Prime 1"
            },
            {
                label: "Prime 2",
                value: "Prime 2"
            },
            {
                label: "Prime 3",
                value: "Prime 3"
            },
            {
                label: "Note prime",
                value: "Note prime"
            }]
        }
    ]

    static RatingForm = [
        {
            label: "AA+",
            value: "AA+"
        },
        {
            label: "AA-",
            value: "AA-"
        },
        {
            label: "A",
            value: "A"
        },
        {
            label: "A+",
            value: "A+"
        },
        {
            label: "A-",
            value: "A-"
        },
        {
            label: "BBB",
            value: "BBB"
        },
        {
            label: "BBB+",
            value: "BBB+"
        },
        {
            label: "BBB-",
            value: "BBB-"
        },
        {
            label: "BB",
            value: "BB"
        },
        {
            label: "BB+",
            value: "BB+"
        },
        {
            label: "BB-",
            value: "BB-"
        },
        {
            label: "B",
            value: "B"
        },
        {
            label: "B+",
            value: "B+"
        },
        {
            label: "B-",
            value: "B-"
        },
        {
            label: "CCC",
            value: "CCC"
        },
        {
            label: "CCC+",
            value: "CCC+"
        },
        {
            label: "CCC-",
            value: "CCC-"
        },
        {
            label: "CC",
            value: "CC"
        },
        {
            label: "C",
            value: "C"
        },
        {
            label: "SD",
            value: "D"
        }
    ]

    static NumberForm = [
        {
            label: "1",
            value: 1
        },
        {
            label: "2",
            value: 2
        },
        {
            label: "3",
            value: 3
        },
        {
            label: "4",
            value: 4
        },
        {
            label: "5",
            value: 5
        },
    ]
    //Min minimum investment
    static minimumInvestment_BT = 50000;
    static minimumInvestment_CD = 500000;
    static minimumInvestment_EO = 100;

    //Type Status
    static typeStatusEmission = [
        {
            label: "en attente",
            value: 1
        },
        {
            label: "Accepté",
            value: 3
        }
        ,
        {
            label: "Refusé",
            value: 2
        }
    ]
    //Type Statuc IC
    static typeEmissionStatusIC = [
        {
            label: "Tout",
            value: undefined
        },
        {
            label: "Nouvelles offres",
            value: 1
        },
        {
            label: "En attente",
            value: 2
        },
        {
            label: "Accepté",
            value: 3
        },
        {
            label: "Décliné",
            value: 4
        }
    ]
    //Type Rate
    static typeRateEmission = [
        {
            label: "Taux Fixe",
            value: 1
        },
        {
            label: "Taux Variable",
            value: 2
        },
        {
            label: "Taux d'intérêt",
            value: 3
        }
    ]
    //Moody's Long terme
    static RatingFormLongMoody = [
        //5 stars
        {
            label: "Aaa",
            value: "Aaa",
            rate: 5
        },
        {
            label: "Aa1",
            value: "Aa1",
            rate: 5
        },
        {
            label: "Aa2",
            value: "Aa2",
            rate: 5
        },
        {
            label: "Aa3",
            value: "Aa3",
            rate: 5
        },
        {
            label: "A1",
            value: "A1",
            rate: 5
        },
        //4 stars
        {
            label: "A2",
            value: "A2",
            rate: 4
        },
        {
            label: "A3",
            value: "A3",
            rate: 4
        },
        {
            label: "Baa1",
            value: "Baa1",
            rate: 4
        },
        {
            label: "Baa2",
            value: "Baa2",
            rate: 4
        },
        {
            label: "Baa3",
            value: "Baa3",
            rate: 4
        },
        //3 stars
        {
            label: "Ba1",
            value: "Ba1",
            rate: 3
        },
        {
            label: "Ba2",
            value: "Ba2",
            rate: 3
        },
        {
            label: "Ba3",
            value: "Ba3",
            rate: 3
        },
        {
            label: "B1",
            value: "B1",
            rate: 3
        },
        //2 stars
        {
            label: "B2",
            value: "B2",
            rate: 2
        },
        {
            label: "B3",
            value: "B3",
            rate: 2
        },
        {
            label: "Caa1",
            value: "Caa1",
            rate: 2
        },
        {
            label: "Caa2",
            value: "Caa2",
            rate: 2
        },
        {
            label: "Caa3",
            value: "Caa3",
            rate: 2
        },
        //1 star
        {
            label: "Ca",
            value: "Ca",
            rate: 1
        },
        {
            label: "C",
            value: "C",
            rate: 1
        }
    ]

    //Fitch rating S&P long terme 
    static RatingFormLongFitch_SP = [
        {
            label: "AAA",
            value: "AAA"
        },
        {
            label: "AA",
            value: "AA"
        },
        {
            label: "AA+",
            value: "AA+"
        },
        {
            label: "AA-",
            value: "AA-"
        },
        {
            label: "A",
            value: "A"
        },
        {
            label: "A+",
            value: "A+"
        },
        {
            label: "A-",
            value: "A-"
        },
        {
            label: "BBB",
            value: "BBB"
        },
        {
            label: "BBB+",
            value: "BBB+"
        },
        {
            label: "BBB-",
            value: "BBB-"
        },
        {
            label: "BB",
            value: "BB"
        },
        {
            label: "BB+",
            value: "BB+"
        },
        {
            label: "BB-",
            value: "BB-"
        },
        {
            label: "B",
            value: "B"
        },
        {
            label: "B+",
            value: "B+"
        },
        {
            label: "B-",
            value: "B-"
        },
        {
            label: "CCC",
            value: "CCC"
        },
        {
            label: "CCC+",
            value: "CCC+"
        },
        {
            label: "CCC-",
            value: "CCC-"
        },
        {
            label: "CC",
            value: "CC"
        },
        {
            label: "C",
            value: "C"
        },
        {
            label: "SD",
            value: "D"
        }
    ]
    //Fitch rating S&P court terme 
    static RatingFormCourtFitch_SP = [
        {
            label: "F1+",
            value: "F1+"
        },
        {
            label: "F1",
            value: "F1"

        }

    ]

    //Moody's Long terme
    static RatingFormCourtMoody = [
        {
            label: "Prime 1",
            value: "Prime 1"
        },
        {
            label: "Prime 2",
            value: "Prime 2"
        },
        {
            label: "Prime 3",
            value: "Prime 3"
        },
        {
            label: "Note prime",
            value: "Note prime"
        },
        {
            label: "-",
            value: "-"
        }


    ]


    //Perspective

    static RatingFormPerspective = [
        {
            label: "Positives",
            value: "Positives"
        },
        {
            label: "Stables",
            value: "Stables"
        },
        {
            label: "Négatives",
            value: "Négatives"
        },
        {
            label: "-",
            value: "-"
        },
        {
            label: "na",
            value: "na"
        },
    ]

    //File Product
    static FileProduct = [
        //Désignation du produit
        {
            title: 'Désignation du produit',
            content: 'Compte à terme «CT»'
        },
        //Type du Produit
        {
            title: 'Type du Produit',
            content: 'titre de créance'
        },
        //Description du CT
        {
            title: 'Description du CT ',
            content: `
        Un compte à terme est un placement à durée déterminée auprès d’un établissement bancaire. Il s'agit, comme son nom l'indique, d'un placement financier à court ou moyen terme, rémunéré.
  Le montant, l’échéance et le taux d’intérêt sont fixés dès l’ouverture du compte. 
  Les placements ne peuvent être à effet rétroactifs.
        `
        },
        //Caractéristiques du CT
        {
            title: 'Caractéristiques du CT',
            children: [
                {
                    title: 'Devise',
                    content: 'TND'
                },
                {
                    title: 'Nature des titres',
                    content: 'titres de créance'
                },
                {
                    title: 'Forme des titres',
                    content: 'Nominative'
                },
                {
                    title: 'Négociabilité',
                    content: 'NON'
                },
                {
                    title: 'Durée',
                    content: 'La durée est comprise entre 3 mois et 5 ans.'
                },
                {
                    title: 'Investissement minimum',
                    content: '1000 DT.'
                },
                {
                    title: 'Investissement maximum',
                    content: 'N/A'
                },
                {
                    title: 'Taux',
                    content: 'Taux fixe si durée <= 1 an'
                },
                {
                    title: 'Résiliation-Remboursement Anticipé',
                    content: `
            Le Compte à terme ne peut pas être remboursé par  anticipation ni renouvelé par tacite reconduction.
          La banque peut consentir une avance au titulaire d'un dépôt à terme. Dans ce cas, la banque perçoit au moins quinze (15) jours d'intérêts calculés au taux appliqué au compte à terme, comportant une échéance majoré d'un (1) point de pourcentage.
          `
                },
                {
                    title: 'Versement des intérêts',
                    content: 'Les intérêts sont payables à terme échu :',
                    children: [
                        {
                            content: "Lorsque la durée du placement est inférieure à une année, l'intérêt est payable en une seule fois à terme échu"
                        },
                        {
                            content: "Lorsque la durée du placement est supérieure à une année, l'intérêt est payable à la fin de chaque période d'une année et à l'échéance pour la fraction d'année restante."
                        }
                    ]
                },
                {
                    title: 'Imposition des intérêts',
                    content: "Les intérêts servis sont assujettis à une  retenue à la source au taux en vigueur soit 20%.",
                },
                {
                    title: 'Qui peut souscrire',
                    content: "Personne Physique / Personne Morale / Non résident Hors US",
                },
                {
                    title: 'Prix d’émission',
                    content: "",
                },
                {
                    title: 'Versement de Fonds',
                    content: "L’investisseur prend attache directement avec l’émetteur pour concrétiser son investissement. Yanvestee n'assure pas des services de tenue de compte, de dépôt, de gestion ou de transfert de fonds.",
                },
                {
                    title: 'Inscription en compte',
                    content: "Obligatoire",
                },
                {
                    title: 'Rémunération',
                    content: `Les intérêts sont calculés en fonction de la durée du placement sur la base du Taux défini lors de l’ouverture du compte.
              Rémunération à l’échéance : remboursement du capital placé augmenté des intérêts.
              Les intérêts sont calculés en fonction de la durée du placement sur la base du Taux défini lors de l’ouverture du compte. Ils sont automatiquement versés au terme de chaque année suivant la date d’ouverture, et enfin à l’échéance.`,
                },
                {
                    title: 'Identification',
                    children: [
                        {
                            title: 'Code ISIN',
                            content: 'NON'
                        },
                        {
                            title: 'Code ISIN',
                            content: 'NON'
                        },
                        {
                            title: 'FISN',
                            content: ''
                        }
                    ]
                }
            ]
        },
        //Risques
        {
            title: "Risques",
            children: [
                {
                    title: "Risque de Défaut",
                    content: `le remboursement du placement est soumis au risque d’insolvabilité de l'émetteur.`
                },
                {
                    title: "Risque de taux",
                    content: ``
                },
                {
                    title: "Risque de liquidité",
                    content: `risque existants vu que le produit est non négociable.`
                },
                {
                    title: "Risque de variation des prix  ou risque de marché",
                    content: ``
                },
                {
                    title: "Risque commercial",
                    content: `n’est pas encouru`
                },
                {
                    title: "Risque de change",
                    content: `Ce risque s’applique aux placements étrangers`
                },
                {
                    title: "Risque lié à l’inflation",
                    content: ``
                }
            ]
        },
        //Frais
        {
            title: "Frais",
            children: [
                {
                    title: "YANVESTEE n’applique pas de frais pour l’investisseur"
                },
                {
                    title: "YANVESTEE facture l’émetteur en appliquant 0,2 % sur le montant souscrit."
                }
            ]
        },
        //Fiscalité
        {
            title: "Fiscalité",
            content: "Les intérêts servis sont assujettis à une  retenue à la source au taux en vigueur soit 20%."
        },
        //Exigences particulières de l’émetteur
        {
            title: "Exigences particulières de l’émetteur",
            content: `Le compte à terme doit faire l'objet d'un contrat écrit entre la banque et son client fixant les conditions de dépôt en termes de montant, de taux d’intérêt et de durée de placement.`
        },
        //Avantages
        {
            title: "Avantages",
            children: [
                {
                    title: "Rentabilité",
                    content: "rendement connu d’avance."
                },
                {
                    title: "Sécurité",
                    content: "capital fructifie en toute sécurité."
                },
                {
                    title: "Exonération fiscale",
                    content: "des intérêts en cas de compte à terme en dinar convertible."
                }
            ]
        },
        //Cadre réglementaire
        {
            title: "Cadre réglementaire",
            content: "CIRCULAIRE de la BCT AUX BANQUES N° 91-22 DU 17 DECEMBRE 1991"
        }
    ]
    // 
    /*******************
     *      API
     *****************/
    //Marketplace
    static SEND_MESSAGE_CONTACT_MARKETPLACE = "emissions/sendMessage";
    //API Crud Listing
    static GEt_MY_EMISSION = "emissions/myEmissions";
    static CREATE_EMISSION_CT = "emissions/createCT";
    static CREATE_EMISSION_BT = "emissions/createBT";
    static CREATE_EMISSION_CD = "emissions/createCD";
    static CREATE_EMISSION_EO = "emissions/createEO";
    static DELETE_EMISSION = "emissions/deleteEmission";
    static EXTEND_PERIOD_EMISSION = "emissions/extendEndDateEmission";
    static GET_ALL_CONTACT_EM = "contact/getAllContact";
    //API Contact
    static GET_ALL_CONTACTS = "contact/getContact";
    static CREATE_CONTACT = "contact";
    static DELETE_CONTACT_ID = "contact";
    static UPDATE_CONTACT = "contact/updateContact";
    static GET_CONTACT_ID = "contact/getContactById";
    //API Upload Logo 
    static USER_LOGO = "users/uploadImage";
    //API Instit Club
    static GET_LIST_USERS_INSTIT_CLUB = "institClub/getListUserByRole";
    static CREATE_INSTIT_CLUB = "institClub/create";
    static GET_LIST_IC_MY_OFFRE = "institClub/getMyOffres";
    static GET_LIST_IC_OFFRE_EM = "institClub/getListOffresByEM";
    static GET_INSTIT_OFFRE_ID = "institClub/getOffreById";
    static ADD_INVESTMENT_IC = "institClub/investmentIC";
    static CANCEL_INVESTMENT_IC = "institClub/cancelOfferEM";
    static GET_LIST_INV_INVESTOR = "institClub/getListInvestment";
    static ACCEPT_OR_REFUSE_OFFRE = "institClub/acceptRefuseInvestment";
    static CHECK_OWNER_OFFRE = "institClub/checkOffreIc";
    static CHECK_INVESTED = "institClub/checkInvested";
    static GET_ALL_INVESTMENTS_EM = "institClub/getlistOffersByEmetteur";
    static GET_INFORMATION_INVES = "institClub/getInfoOffers";
    //API INVESTMENTS
    static GET_MY_INVESTMENTS_IV = "investments/myinvestmentsInvestor";
    //API USER
    static UPDATE_OUT_STANDING = "users/updateOutStanding";
    //API Notifications
    static GET_ALL_NOTIFICATION = "notifications/getAllNotification";
    static GET_NOTIFICATION = "notifications/getNotifications";
    static CHANGE_STATUS_NOTIF = "notifications/changeStatusNotif";
}