import { NgModule, LOCALE_ID } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import localeFr from "@angular/common/locales/fr";

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule, MatIconModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';

import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';

//Inteceptor Service
import { ErrorInterceptorService } from './helpers/interceptor/error.interceptor.service';
import { JwtInterceptorService } from './helpers/interceptor/jwt.interceptor.service';
import { BrowserModule } from '@angular/platform-browser';
//Import Modules
import { registerLocaleData } from '@angular/common';
//Import Guard
import { AuthGuardService } from './core/_guard/auth.guard.service';
//Emetteur Guard
import { EmAllGuard } from '@guradEmetteur/em-all.guard';
//Investor Guard
import { IvAllGuard } from '@guradInvestor/iv-all.guard';

import { ToastrModule } from 'ngx-toastr';
const appRoutes: Routes = [
    {
        path: 'notifications',
        loadChildren: './main/notification/notification.module#NotificationModule',
        canActivate: [AuthGuardService]
    },
    {
        path: 'faq',
        loadChildren: './main/faq/faq.module#FaqModule',
        canActivate: [AuthGuardService]
    },
    {
        path: 'instit-club',
        loadChildren: './main/instit-club/instit-club.module#InstitClubModule',
        canActivate: [AuthGuardService]
    },
    {
        path: 'marketPlace',
        loadChildren: './main/marketPlace/marketPlace.module#MarketPlaceModule',
        canActivate: [AuthGuardService]
    },
    {
        path: 'listing',
        loadChildren: './main/listing/listing.module#ListingModule',
        canActivate: [AuthGuardService, EmAllGuard]
    },
    {
        path: 'contact',
        loadChildren: './main/contact/contact.module#ContactModule',
        canActivate: [AuthGuardService]
    },
    {
        path: 'login',
        loadChildren: './main/authentication/login/login.module#LoginModule',

    },
    {
        path: 'profil',
        loadChildren: './main/profile/profile.module#ProfileModule',
        canActivate: [AuthGuardService]
    },
    {
        path: 'forgot-password',
        loadChildren: './main/authentication/forgot-password/forgot-password.module#ForgotPasswordModule'
    },
    {
        path: 'reset-password',
        loadChildren: './main/authentication/reset-password/reset-password.module#ResetPasswordModule'
    },
    {
        path: 'inscription',
        loadChildren: './main/authentication/register-2/register-2.module#Register2Module'
    },
    {
        path: '**',
        redirectTo: 'login'
    }
];

registerLocaleData(localeFr);

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),
        ToastrModule.forRoot(),
        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
    ],
    bootstrap: [
        AppComponent
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptorService, multi: true },
        { provide: LOCALE_ID, useValue: "fr" },
        
    ],
    
    exports: []
})
export class AppModule {
}
