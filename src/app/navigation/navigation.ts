import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id: 'listing',
        title: 'listing',
        translate: 'NAV.LISTING.TITLE',
        type: 'collapsable',
        icon: 'list',
        children: [
            {
                id   : 'empruntsOb',
                title: 'Emprunts obligataire',
                translate: 'NAV.EMPOB.TITLE',
                type : 'item',
                url  : '/listing/emprunts'
            },
            {
                id   : 'certiDepo',
                title: 'Certificats de dépôt',
                translate: 'NAV.CERTIFDEPO.TITLE',
                type : 'item',
                url  : '/listing/certif'
            },
            {
                id   : 'commPaper',
                title: 'Billets de trésorerie',
                translate: 'NAV.COMMPAPER.TITLE',
                type : 'item',
                url  : '/listing/billet-tresorie'
            },
            {
                id   : 'termAccounts',
                title: 'Comptes à terme',
                translate: 'NAV.TERMACCOUNTS.TITLE',
                type : 'item',
                url  : '/listing/compte-terme'
            }
        ]
    }
];
