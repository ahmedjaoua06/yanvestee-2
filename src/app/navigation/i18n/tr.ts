export const locale = {
    lang: 'tr',
    data: {
        'NAV': {
            'LISTING': {
                'TITLE': 'Listing',
            },
            "INSClUB":{
                'TITLE': 'Instit club',
            },
            'EMPOB': {
                'TITLE': 'Emprunts oblig'
            },
            'CERTIFDEPO': {
                'TITLE': 'Certificates of deposit'
            },
            'COMMPAPER': {
                'TITLE': 'Commercial paper'
            },
            'TERMACCOUNTS': {
                'TITLE': 'Term Accounts'
            },
            'FAQ': {
                'TITLE': "FAQ"
            }
        }
    }
};
