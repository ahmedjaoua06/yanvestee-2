export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'LISTING': {
                'TITLE': 'Listing',
            },
            "INSClUB":{
                'TITLE': 'Instit club',
            },
            'EMPOB': {
                'TITLE': 'Emprunts oblig'
            },
            'CERTIFDEPO': {
                'TITLE': 'Certificats de dépôt'
            },
            'COMMPAPER': {
                'TITLE': 'Billets de trésorerie'
            },
            'TERMACCOUNTS': {
                'TITLE': 'Comptes à terme'
            },
            'FAQ': {
                'TITLE': "FAQ"
            }
        }
    }
};
