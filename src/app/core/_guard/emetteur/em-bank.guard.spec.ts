import { TestBed, async, inject } from '@angular/core/testing';

import { EmBankGuard } from './em-bank.guard';

describe('EmBankGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmBankGuard]
    });
  });

  it('should ...', inject([EmBankGuard], (guard: EmBankGuard) => {
    expect(guard).toBeTruthy();
  }));
});
