import { TestBed, async, inject } from '@angular/core/testing';

import { EmConseilGuard } from './em-conseil.guard';

describe('EmConseilGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmConseilGuard]
    });
  });

  it('should ...', inject([EmConseilGuard], (guard: EmConseilGuard) => {
    expect(guard).toBeTruthy();
  }));
});
