import { TestBed, async, inject } from '@angular/core/testing';

import { EmLeasingGuard } from './em-leasing.guard';

describe('EmLeasingGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmLeasingGuard]
    });
  });

  it('should ...', inject([EmLeasingGuard], (guard: EmLeasingGuard) => {
    expect(guard).toBeTruthy();
  }));
});
