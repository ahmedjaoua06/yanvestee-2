import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationServiceService } from '@serviceHelper/authentication.service.service';
import { Constants } from '@helper/constants';

@Injectable({
  providedIn: 'root'
})
export class EmImfGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationServiceService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    //Check if user is emetteur
    const currentUser = this.authenticationService.currentUserValue;
    //Type IMF
    let typeIMF = Constants.categoriesUser.find(a=> a.id == 3);
    //Check if current user exist and role is emetteur and type is imf
    if (currentUser && currentUser.role == Constants.emetteur && currentUser.type == typeIMF.value) {
      // role approved
      return true;
    }

    // redirect to marketplace
    this.router.navigate(['/marketPlace/marketCards']);
    return false;
  }
}
