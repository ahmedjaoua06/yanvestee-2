import { TestBed, async, inject } from '@angular/core/testing';

import { EmAllGuard } from './em-all.guard';

describe('EmAllGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmAllGuard]
    });
  });

  it('should ...', inject([EmAllGuard], (guard: EmAllGuard) => {
    expect(guard).toBeTruthy();
  }));
});
