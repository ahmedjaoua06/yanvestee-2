import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationServiceService } from '@serviceHelper/authentication.service.service';
//Import Constant
import { Constants } from '@helper/constants';
@Injectable({
  providedIn: 'root'
})
export class EmAllGuard implements CanActivate {

  constructor(
    private router: Router,
    private authenticationService: AuthenticationServiceService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    console.log(next.data.role);

    //Check if user is emetteur
    const currentUser = this.authenticationService.currentUserValue;
    if (currentUser && currentUser.role == Constants.emetteur) {
      // role approved
      return true;
    }

    // redirect to marketplace
    this.router.navigate(['/marketPlace/marketCards']);
    return false;
  }
}
