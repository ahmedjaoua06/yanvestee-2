import { TestBed, async, inject } from '@angular/core/testing';

import { EmImfGuard } from './em-imf.guard';

describe('EmImfGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmImfGuard]
    });
  });

  it('should ...', inject([EmImfGuard], (guard: EmImfGuard) => {
    expect(guard).toBeTruthy();
  }));
});
