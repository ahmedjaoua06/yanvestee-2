import { TestBed, async, inject } from '@angular/core/testing';

import { EmCorporatesGuard } from './em-corporates.guard';

describe('EmCorporatesGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmCorporatesGuard]
    });
  });

  it('should ...', inject([EmCorporatesGuard], (guard: EmCorporatesGuard) => {
    expect(guard).toBeTruthy();
  }));
});
