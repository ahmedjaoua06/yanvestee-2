import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationServiceService } from '@serviceHelper/authentication.service.service';
import { Constants } from '@helper/constants';

@Injectable({
  providedIn: 'root'
})
export class EmBankGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationServiceService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    //Check if user is emetteur
    const currentUser = this.authenticationService.currentUserValue;
    //Type Bank
    let typeBank = Constants.categoriesUser.find(a=> a.id == 1);
    //Check if current user exist and role is emetteur and type is bank
    if (currentUser && currentUser.role == Constants.emetteur && currentUser.type == typeBank.value) {
      // role approved
      return true;
    }

    // redirect to marketplace
    this.router.navigate(['/marketPlace/marketCards']);
    return false;
  }
}
