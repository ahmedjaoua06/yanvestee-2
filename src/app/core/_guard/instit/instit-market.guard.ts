import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Constants } from '@helper/constants';
import { AuthenticationServiceService } from '@serviceHelper/authentication.service.service';

@Injectable({
  providedIn: 'root'
})
export class InstitMarketGuard implements CanActivate {

  constructor(
    private router: Router,
    private authenticationService: AuthenticationServiceService
  ) { }

  canActivate(
    
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {





    //Check if user is investor
    const currentUser = this.authenticationService.currentUserValue;
    //Type Assurance
    let typeAssurance = Constants.categoriesUser.find(a => a.id == 7);
    //Type Asset Manager
    let typeManager = Constants.categoriesUser.find(a => a.id == 6);
    //Check if current user exist and role is investor and type is assurance
    if (currentUser && currentUser.role == Constants.investor && (currentUser.type == typeAssurance.value || typeManager.value)) {
      // role approved
      return true;
    }

    // redirect to marketplace
    this.router.navigate(['/marketPlace/marketCards']);
    return false;






    return true;
  }
}
