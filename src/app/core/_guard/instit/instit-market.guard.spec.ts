import { TestBed, async, inject } from '@angular/core/testing';

import { InstitMarketGuard } from './instit-market.guard';

describe('InstitMarketGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InstitMarketGuard]
    });
  });

  it('should ...', inject([InstitMarketGuard], (guard: InstitMarketGuard) => {
    expect(guard).toBeTruthy();
  }));
});
