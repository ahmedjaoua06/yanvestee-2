import { TestBed, async, inject } from '@angular/core/testing';

import { IvAssuranceGuard } from './iv-assurance.guard';

describe('IvAssuranceGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IvAssuranceGuard]
    });
  });

  it('should ...', inject([IvAssuranceGuard], (guard: IvAssuranceGuard) => {
    expect(guard).toBeTruthy();
  }));
});
