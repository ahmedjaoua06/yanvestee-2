import { TestBed, async, inject } from '@angular/core/testing';

import { IvPublicGuard } from './iv-public.guard';

describe('IvPublicGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IvPublicGuard]
    });
  });

  it('should ...', inject([IvPublicGuard], (guard: IvPublicGuard) => {
    expect(guard).toBeTruthy();
  }));
});
