import { TestBed, async, inject } from '@angular/core/testing';

import { IvAllGuard } from './iv-all.guard';

describe('IvAllGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IvAllGuard]
    });
  });

  it('should ...', inject([IvAllGuard], (guard: IvAllGuard) => {
    expect(guard).toBeTruthy();
  }));
});
