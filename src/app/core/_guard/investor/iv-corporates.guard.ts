import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationServiceService } from '@serviceHelper/authentication.service.service';
import { Constants } from '@helper/constants';

@Injectable({
  providedIn: 'root'
})
export class IvCorporatesGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationServiceService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    //Check if user is investor
    const currentUser = this.authenticationService.currentUserValue;
    //Type Corporates
    let typeCorporates = Constants.categoriesUser.find(a => a.id == 9);
    //Check if current user exist and role is investor and type is corporates
    if (currentUser && currentUser.role == Constants.investor && currentUser.type == typeCorporates.value) {
      // role approved
      return true;
    }

    // redirect to marketplace
    this.router.navigate(['/marketPlace/marketCards']);
    return false;
  }
}
