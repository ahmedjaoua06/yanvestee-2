import { TestBed, async, inject } from '@angular/core/testing';

import { IvAssetmanagerGuard } from './iv-assetmanager.guard';

describe('IvAssetmanagerGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IvAssetmanagerGuard]
    });
  });

  it('should ...', inject([IvAssetmanagerGuard], (guard: IvAssetmanagerGuard) => {
    expect(guard).toBeTruthy();
  }));
});
