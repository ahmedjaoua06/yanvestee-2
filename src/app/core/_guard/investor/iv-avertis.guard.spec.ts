import { TestBed, async, inject } from '@angular/core/testing';

import { IvAvertisGuard } from './iv-avertis.guard';

describe('IvAvertisGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IvAvertisGuard]
    });
  });

  it('should ...', inject([IvAvertisGuard], (guard: IvAvertisGuard) => {
    expect(guard).toBeTruthy();
  }));
});
