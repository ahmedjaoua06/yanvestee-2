import { TestBed, async, inject } from '@angular/core/testing';

import { IvCorporatesGuard } from './iv-corporates.guard';

describe('IvCorporatesGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IvCorporatesGuard]
    });
  });

  it('should ...', inject([IvCorporatesGuard], (guard: IvCorporatesGuard) => {
    expect(guard).toBeTruthy();
  }));
});
