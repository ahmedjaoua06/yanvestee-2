import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '@helper/models/user.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router'
//Import Service Helper
import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { Constants } from '@helper/constants';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})

export class AuthenticationServiceService {

  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  public notifications = [];
  public nbrNotif = 0;
  path = environment.api;
  constructor(
    private http: HttpClient,
    private serviceHelper: ServiceHelperService,
    private _fuseNavigationService: FuseNavigationService,
    private route: Router
  ) {
    //current User
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  //CurrentUser
  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }
  //Login
  login(obj) {
    return this.serviceHelper.__post("login", obj)
      .pipe(map((user: any) => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);
        }
        return user;
      }));
  }
  //Logout
  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    this._fuseNavigationService.unregister("main")
    //Redirect to login
    this.route.navigateByUrl('/login');
  }
  //Notification
  getListNotification() {
    return new Promise((resolve, reject) => {
      this.serviceHelper.__post(Constants.GET_NOTIFICATION, {}).subscribe((res) => {
        this.notifications = res.data;

        var defaultImage = Constants.default_image_user_em;
        //Check if logo user exist
        this.notifications.map(a => a.notifId.userSender.logo = a.notifId.userSender.logo != undefined ? this.path + a.notifId.userSender.logo.path : defaultImage);
        this.nbrNotif = res.data.filter(a => !a.seen).length;
        resolve();
      },
        error => {
          console.log("");
        });
    });
  }
}
