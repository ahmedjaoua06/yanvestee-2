import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from  'environments/environment';
@Injectable({
  providedIn: 'root'
})

export class ServiceHelperService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Accept-Language': 'fr',
      'Authorization': ''
    })
  };

  private path;
  currentUser: any;
  userProfil:any;
  constructor(private http: HttpClient) {
    this.path = `${environment.apiUrl}`;
  }
  //Get
  __get(service, id = "") {
    return this.http.get(`${this.path}${service}/${id}`, this.httpOptions);
  }
  //Post
  __post(service, data) {
    return this.http.post<any>(`${this.path}${service}`, data, this.httpOptions);
  }
  //Put
  __put(service, id, data) {
    return this.http.put<any>(`${this.path}${service}/${id}`, data, this.httpOptions);
  }

  //Delete
  __delete(service, id) {
    return this.http.delete(`${this.path}${service}/${id}`, this.httpOptions);
  }
}
