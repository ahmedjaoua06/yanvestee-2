import { Component, OnDestroy, OnInit, ViewEncapsulation ,ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormControlName } from '@angular/forms';
import { Location } from '@angular/common';
import { MatSnackBar, PageEvent, MatChipInputEvent } from '@angular/material';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import * as XLSX from 'xlsx';  


import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';
//Import Service Helper
import { ServiceHelperService } from '@serviceHelper/service-helper.service';
//Import Model Emission
import { Emission } from '@helper/models/emission.model';
//Import List Type Emission
import { Constants } from '@helper/constants';
import Swal from 'sweetalert2'
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';




@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {
  listSecteur = Constants.ListSecteur;
  listCategories = Constants.ListCategoriesContact;


  pageEvent: PageEvent;
  userContact: any[];
  displayedColumns = ["nom", "societe", "fonction", "categorie", "actions"];

  pageType: string;
  limitPage = 10;
  pageIndex = 1;
  total = 0;
  formFilter: FormGroup;
  

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _location: Location,
    private _matSnackBar: MatSnackBar,
    private _serviceHelper: ServiceHelperService,
    private _router: Router,
    private _formBuilder: FormBuilder,
    private http: HttpClient
  ) {
    this._unsubscribeAll = new Subject();
    
  }

  @ViewChild('TABLE', {  }) TABLE: ElementRef;  
  title = 'Excel'; 
  /**
 * On init
 */
  ngOnInit() {
    this.listSecteur.push({label: "Tout",value: undefined});
    this.listCategories.push({label: "Tout",value: undefined});
    //Init form
    this.initForm();
    //Get List Contacts
    this.getListContact();
  }
  /**
   * On destroy
   */
  
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
  //Get List Contact
  getListContact(_pageIndex = this.pageIndex, _limitPage = this.limitPage) {
    var tags = this.formFilter.value.tags.length > 0 ? this.formFilter.value.tags : undefined;
    this._serviceHelper.__post(Constants.GET_ALL_CONTACTS, { pageIndex: _pageIndex, limitPage: _limitPage, tags: tags, searchQuerry: this.formFilter.value }).subscribe((res) => {
      //Get Data
      this.userContact = res.data.docs;
      this.userContact.forEach((el) => {
        let _firstname = el.firstname != undefined ? el.firstname : "";
        let _lastname = el.lastname != undefined ? el.lastname : "";
        el.badge = _lastname.substring(0, 1) + _firstname.substring(0, 1);
      })
      //Set Total
      this.total = res.data.total;
    },
      error => {
        console.log(error);
      })
  }
  //Redirect to add
  redirectToAdd() {
    this._router.navigate(['contact/add']);
  }
  //Remove contact
  removeContact(c) {

    Swal.fire({
      title: 'Suppression?',
      text: "êtes-vous sûr de vouloir supprimer ce contact ??!!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this._serviceHelper.__delete(Constants.DELETE_CONTACT_ID, c._id).subscribe(res => {
          console.log(res);
          Swal.fire({
            position: 'top-end',
            type: 'success',
            title: 'Contact supprimé avec succès',
            showConfirmButton: false,
            timer: 1500
          }).then(() => {
            this.userContact = this.userContact.filter(a => a._id != c._id);
          })
        },
          error => {
            if (error.error.message) {
              Swal.fire({
                title: 'Erreur',
                text: error.error.message,
                type: 'error'
              });
            }
          });
      }
    });
  }
  //Event Click Row
  eventClickRow(r) {
    //Redirect to update contact
    this._router.navigate(['contact/update'], { queryParams: { idContact: r._id } });
  }
  //Event change search query
  changeSearchQuery(e, field) {
    switch (field) {
      case 'name':
        this.formFilter.patchValue({
          name: e.srcElement.value
        });
        break;
      case 'category':
        this.formFilter.patchValue({
          category: e.value
        });
        break;
      case 'company':
        this.formFilter.patchValue({
          company: e.srcElement.value
        });
        break;
      case 'sector':
        this.formFilter.patchValue({
          sector: e.value
        });
        break;
      case 'fonction':
        this.formFilter.patchValue({
          fonction: e.srcElement.value
        });
        break;
    }
    this.getListContact();
  }
  /***********************
 *  Method Paginator
 **********************/
  getServerData(e) {
    this.getListContact((e.pageIndex + 1), e.pageSize)
  }
  //Init form filter
  initForm() {
    this.formFilter = this._formBuilder.group({
      tags: [[]],
      name: [''],
      category: [''],
      company: [''],
      sector: [''],
      fonction: ['']
    });
  }
  //Event add tag
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our tag
    if ((value || '').trim()) {
      this.formFilter.controls.tags.value.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    //Filter
    if (event.value != "") {
      this.getListContact();
    }

  }
  //Event remove tag
  remove(tag): void {
    const index = this.formFilter.controls.tags.value.indexOf(tag);

    if (index >= 0) {
      this.formFilter.controls.tags.value.splice(index, 1);
      this.getListContact();
    }
  }
    //Event Export Exel
    ExportTOExcel() {  
      const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.TABLE.nativeElement);  
      const wb: XLSX.WorkBook = XLSX.utils.book_new();  
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');  
      XLSX.writeFile(wb, 'ContactSheet.xlsx');  
    }
        
          
            
  
        
    }  
    
  

