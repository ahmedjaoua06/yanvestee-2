import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators, FormArray, FormControl } from '@angular/forms';
//Import Service Helper
import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import { Constants } from '@helper/constants';
import Swal from 'sweetalert2'
import { Router } from '@angular/router';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material';

@Component({
  selector: 'app-contact-add',
  templateUrl: './contact-add.component.html',
  styleUrls: ['./contact-add.component.scss']
})
export class ContactAddComponent implements OnInit {
  contactForm: FormGroup;
  listSecteur = Constants.ListSecteur;
  listCategories = Constants.ListCategoriesContact;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  constructor(
    private _formBuilder: FormBuilder,
    private _serviceHelper: ServiceHelperService,
    private _router: Router

  ) {
  }
  
  ngOnInit() {
    //Init Form
    this.initForm();
  }
  //Init Form
  initForm() {
    this.contactForm = this._formBuilder.group({
      societe: [''],
      fonction: [''],
      tags: [['invité']],
      firstname: ['', Validators],
      lastname: ['', Validators],
      email: ['', Validators.required],
      tel: ['', [Validators, Validators.minLength(8)]],
      mobile: ['', [Validators, Validators.minLength(8)]],
      sector: [''],
      categorie: [''],
      note: ['']
    });
  }
  //Event Add Contact
  eventContactAdd(f) {

    Swal.fire({
      title: 'Confirmation',
      type: 'success',
      showCancelButton: true,
      confirmButtonText: 'Oui, Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        if (f.valid) {
          this._serviceHelper.__post(Constants.CREATE_CONTACT, f.value).subscribe((res: any) => {
            console.log(res);
            Swal.fire({
              position: 'top-end',
              type: 'success',
              title: 'Contact enregistrée avec succès',
              showConfirmButton: false,
              timer: 1500
            }).then(() => {
              this._router.navigate(["/contact"]);
            })
          },
            error => {
              if (error.error.message) {
                Swal.fire({
                  title: 'Erreur',
                  text: error.error.message,
                  type: 'error'
                });
              }
            });
        }
      }
    });
  }
  //Event add tag
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our tag
    if ((value || '').trim()) {
      this.contactForm.controls.tags.value.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }
  //Event remove tag
  remove(tag): void {
    const index = this.contactForm.controls.tags.value.indexOf(tag);

    if (index >= 0) {
      this.contactForm.controls.tags.value.splice(index, 1);
    }
  }
}
