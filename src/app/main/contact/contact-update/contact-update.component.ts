import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators, FormArray, FormControl } from '@angular/forms';
//Import Service Helper
import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import { Constants } from '@helper/constants';
import Swal from 'sweetalert2'
import { Router, ActivatedRoute } from '@angular/router';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material';

@Component({
  selector: 'app-contact-update',
  templateUrl: './contact-update.component.html',
  styleUrls: ['./contact-update.component.scss']
})
export class ContactUpdateComponent implements OnInit {
  contact: any;
  contactForm: FormGroup;
  params: any;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  constructor(
    private _formBuilder: FormBuilder,
    private _serviceHelper: ServiceHelperService,
    private _router: Router,
    private _activateRoute: ActivatedRoute

  ) {
  }

  ngOnInit() {
    //Get params
    this._activateRoute.queryParams.subscribe((params) => {
      console.log("params");
      console.log(params);
      //If params exist
      if (params) {
        this.params = params;
      }
    });
    //Init form controls
    this.initForm();
    //Get Contact by id
    this.getContact();
  }

  //Get Contact
  getContact() {

    this._serviceHelper.__post(Constants.GET_CONTACT_ID, { idContact: this.params.idContact }).subscribe((res) => {
      this.contact = res.data;
      //Set Data
      this.contactForm.patchValue({
        societe: this.contact.societe,
        fonction: this.contact.fonction,
        tags: this.contact.tags,
        firstname: this.contact.firstname,
        lastname: this.contact.lastname,
        email: this.contact.email,
        tel: this.contact.tel,
        mobile: this.contact.mobile
      });
      console.log(res);
    },
      error => {
        console.log(error);
      });

  }

  //Init Form
  initForm() {
    this.contactForm = this._formBuilder.group({
      societe: ['', ],
      fonction: ['', ],
      tags: [['invité']],
      firstname: ['', ],
      lastname: ['', ],
      email: ['', Validators.required],
      tel: ['', [ Validators.minLength(8)]],
      mobile: ['', [ Validators.minLength(8)]]
    });
  }
  //Event Update Contact
  eventContactUpdate(f) {

    Swal.fire({
      title: 'Confirmation',
      type: 'success',
      showCancelButton: true,
      confirmButtonText: 'Oui, Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        if (f.valid) {
          this._serviceHelper.__put(Constants.UPDATE_CONTACT, this.contact._id, f.value).subscribe((res: any) => {
            console.log(res);
            Swal.fire({
              position: 'top-end',
              type: 'success',
              title: res.message,
              showConfirmButton: false,
              timer: 1500
            }).then(() => {
              this._router.navigate(["/contact"]);
            })
          },
            error => {
              if (error.error.message) {
                Swal.fire({
                  title: 'Erreur',
                  text: error.error.message,
                  type: 'error'
                });
              }
            });
        }
      }
    });
  }
  //Event add tag
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our tag
    if ((value || '').trim()) {
      this.contactForm.controls.tags.value.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }
  //Event remove tag
  remove(tag): void {
    const index = this.contactForm.controls.tags.value.indexOf(tag);

    if (index >= 0) {
      this.contactForm.controls.tags.value.splice(index, 1);
    }
  }
}