import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListingRoutingModule } from './listing-routing.module';
import { CompteTermeListComponent } from './compte-terme/compte-terme-list/compte-terme-list.component';
import { CompteTermeAddComponent } from './compte-terme/compte-terme-add/compte-terme-add.component';
//Import Material Modules
import {
  MatButtonModule, MatChipsModule, MatExpansionModule, MatFormFieldModule, MatIconModule, MatInputModule, MatPaginatorModule, MatRippleModule, MatSelectModule, MatSnackBarModule,
  MatSortModule,MatTableModule, MatTabsModule,MatCheckboxModule, MatTooltipModule, MatSliderModule, MatMenuModule,MatSlideToggleModule, MatDatepickerModule, MatStepperModule,MatListModule, MatRadioModule, MatPaginatorIntl
} from '@angular/material';

import {MatDialogModule} from '@angular/material/dialog'; 
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared/shared.module';
import { BilletTresoListComponent } from './billet-treso/billet-treso-list/billet-treso-list.component';
import { BilletTresoAddComponent } from './billet-treso/billet-treso-add/billet-treso-add.component';
import { CertifDepoListComponent } from './certif-dep/certif-depo-list/certif-depo-list.component';
import { CertifDepoAddComponent } from './certif-dep/certif-depo-add/certif-depo-add.component';
import { EmpruntObligataireAddComponent } from './emprunt-obligataire/emprunt-obligataire-add/emprunt-obligataire-add.component'
import { EmpruntObligataireListComponent } from './emprunt-obligataire/emprunt-obligataire-list/emprunt-obligataire-list.component'
import { MomentModule } from 'ngx-moment';
import {NgxMaskModule} from 'ngx-mask'


const FrenchRangeLabel = (page: number, pageSize: number, length: number) => {
  if (length == 0 || pageSize == 0) { return `0 de ${length}`; }
  
  length = Math.max(length, 0);

  const startIndex = page * pageSize;

  // If the start index exceeds the list length, do not try and fix the end index to the end.
  const endIndex = startIndex < length ?
      Math.min(startIndex + pageSize, length) :
      startIndex + pageSize;

  return `${startIndex + 1} - ${endIndex} de ${length}`;
}


export function getFrenchPaginatorIntl() {
  const paginatorIntl = new MatPaginatorIntl();
  
  paginatorIntl.itemsPerPageLabel = 'Articles par page:';
  paginatorIntl.nextPageLabel = 'Page suivante';
  paginatorIntl.previousPageLabel = 'Page précédente';
  paginatorIntl.getRangeLabel = FrenchRangeLabel;
  
  return paginatorIntl;
}



@NgModule({
  declarations: [CompteTermeListComponent, CompteTermeAddComponent, BilletTresoListComponent, BilletTresoAddComponent, CertifDepoListComponent, CertifDepoAddComponent, EmpruntObligataireAddComponent,EmpruntObligataireListComponent],
  imports: [
    FuseSharedModule,
    FuseWidgetModule,
    SharedModule,
    TranslateModule,
    ListingRoutingModule,
    MatButtonModule,
    MatChipsModule,
    MatMenuModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MomentModule,
    MatDialogModule,
    MatPaginatorModule,
    MatRippleModule,
    MatListModule,
    MatSelectModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTooltipModule,
    MatTabsModule,
    MatSliderModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    MatStepperModule,
    MatListModule,
    MatRadioModule,
    NgxMaskModule.forRoot()
  ],
  providers: [
    { provide: MatPaginatorIntl, useValue: getFrenchPaginatorIntl() }
  ]
})
export class ListingModule { }
