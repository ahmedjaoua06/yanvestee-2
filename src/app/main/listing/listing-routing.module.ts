import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Import Compnents
import { CompteTermeListComponent } from './compte-terme/compte-terme-list/compte-terme-list.component';
import { CompteTermeAddComponent } from './compte-terme/compte-terme-add/compte-terme-add.component';

import { CertifDepoListComponent } from './certif-dep/certif-depo-list/certif-depo-list.component';
import { CertifDepoAddComponent } from './certif-dep/certif-depo-add/certif-depo-add.component';

import { BilletTresoListComponent } from './billet-treso/billet-treso-list/billet-treso-list.component'
import { BilletTresoAddComponent } from './billet-treso/billet-treso-add/billet-treso-add.component'

import { EmpruntObligataireAddComponent } from './emprunt-obligataire/emprunt-obligataire-add/emprunt-obligataire-add.component'
import { EmpruntObligataireListComponent } from './emprunt-obligataire/emprunt-obligataire-list/emprunt-obligataire-list.component'
//Import guard emetteur
import { EmAllGuard } from '@guradEmetteur/em-all.guard';
import { EmBankGuard } from '@guradEmetteur/em-bank.guard';


const routes: Routes = [
  {
    path: "certif",
    component: CertifDepoListComponent,
    canActivate: [EmAllGuard]
  },
  {
    path: "certif/add",
    component: CertifDepoAddComponent,
    canActivate: [EmAllGuard]
  },
  {
    path: "compte-terme",
    component: CompteTermeListComponent,
    canActivate: [EmBankGuard]
  },
  {
    path: "compte-terme/add",
    component: CompteTermeAddComponent,
    canActivate: [EmBankGuard]
  },
  {
    path: "billet-tresorie",
    component: BilletTresoListComponent,
    canActivate: [EmAllGuard]
  },
  {
    path: "billet-tresorie/add",
    component: BilletTresoAddComponent,
    canActivate: [EmAllGuard]
  },
  {
    path: "emprunt-obligataire",
    component: EmpruntObligataireListComponent,
    canActivate: [EmAllGuard]
  },
  {
    path: "emprunt-obligataire/add",
    component: EmpruntObligataireAddComponent,
    canActivate: [EmAllGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListingRoutingModule { }
