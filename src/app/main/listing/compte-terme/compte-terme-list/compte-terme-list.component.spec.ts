import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompteTermeListComponent } from './compte-terme-list.component';

describe('CompteTermeListComponent', () => {
  let component: CompteTermeListComponent;
  let fixture: ComponentFixture<CompteTermeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompteTermeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompteTermeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
