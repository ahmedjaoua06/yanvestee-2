import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Location } from '@angular/common';
import { MatSnackBar, PageEvent } from '@angular/material';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';
//Import Service Helper
import { ServiceHelperService } from '@serviceHelper/service-helper.service';
//Import Model Emission
import { Emission } from '@helper/models/emission.model';
//Import List Type Emission
import { Constants } from '@helper/constants';

import Swal from 'sweetalert2'

@Component({
  selector: 'app-billet-treso-list',
  templateUrl: './billet-treso-list.component.html',
  styleUrls: ['./billet-treso-list.component.scss']
})
export class BilletTresoListComponent implements OnInit {
  //Filter
  formFilter: FormGroup;
  listStatus = Constants.ListEcheance;
  //End Filter
  pageEvent: PageEvent;
  emissionBT: any[];
  displayedColumns = ["horizon", "period", "endDate", "rateFixedNominal", "anticipatedTerm", "status", "details", "actions"];

  pageType: string;
  limitPage = 10;
  pageIndex = 1;
  total = 0;
  statusSelected: any;
  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _formBuilder: FormBuilder,
    private _location: Location,
    private _matSnackBar: MatSnackBar,
    private _serviceHelper: ServiceHelperService,
    private _router: Router
  ) {

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    //Init Form filter
    this.initFormFilter();
    //Get List
    this.getListEmission();
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Get List Emissions "Billet Tresorie"
   */
  getListEmission(_pageIndex = this.pageIndex, _limitPage = this.limitPage) {
    let _type = Constants.TypeListing.find(a => a.label == "BT").value;
    //Check filter
    this._serviceHelper.__post(Constants.GEt_MY_EMISSION, { type: _type, pageIndex: _pageIndex, limitPage: _limitPage, status: this.statusSelected }).subscribe((res) => {
      this.emissionBT = res.docs;
      this.emissionBT.map(a => {
        //Get Label horizon from List horizon
        a.horizon = Constants.ListHorizon.find(h => h.value == a.horizon).label;
        //Get Label Résiliation anticipée from List Résiliation anticipée
        a.anticipatedTerm = Constants.ListAnticipatedTermination.find(at => at.value == a.anticipatedTerm).label;
      });
      this.total = res.total;
    },
      error => {
        console.log(error);
      })
  }

  //Show Dialog Add
  showDialogAdd() {
    this._router.navigate(['/listing/billet-tresorie/add']);
  }
  /***********************
   *  Method Paginator
   **********************/
  getServerData(e) {
    // this.getListEmission((e.pageIndex + 1), e.pageSize)
  }



  /****************************
  * Filter Forms & Events
  ****************************/
  //Init Form Filter
  initFormFilter() {
    this.formFilter = this._formBuilder.group({
      status: [''],
    });
  }
  //Event status changed
  eventFilterStatusChange(e) {
    switch (e.index) {
      case 1://Filter by status All
        this.statusSelected = 1;
        break;
      case 2://Filter by status active
        this.statusSelected = 0;
        break;
      default://Filter by status ended
        this.statusSelected = undefined;
        break;
    }
    console.log("Status Selected");
    console.log(this.statusSelected);
    this.getListEmission();
  }
  //Details emission
  eventClickRow(e) {
    // this._router.navigate(["/marketPlace/emissionDetails?emissionId=" + e._id])
  }
  //Disabled emission
  disabledEmission(e) {
    Swal.fire({
      title: 'Clôturer emission?',
      text: "êtes-vous sûr de vouloir clôturer cette émission ??!!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this._serviceHelper.__post(Constants.DELETE_EMISSION, { idEmission: e._id }).subscribe((res) => {
          console.log(res);
          Swal.fire({
            position: 'top-end',
            type: 'success',
            title: res.message,
            showConfirmButton: false,
            timer: 1500
          }).then(() => {
            this.emissionBT.find(a => a._id == e._id).active = false;
            if(this.statusSelected == 1)
              this.emissionBT = this.emissionBT.filter(a=> a._id != e._id);
          })
        },
          (error) => {
            console.log(error);
          });
      }
    });
  }
  //Extend period emission
  extendPeriodEmission(e, v) {
    Swal.fire({
      title: 'Prolonger emission?',
      text: "êtes-vous sûr de vouloir prolonger cette émission ?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this._serviceHelper.__post(Constants.EXTEND_PERIOD_EMISSION, { extendPeriod: v, emissionId: e._id }).subscribe((res) => {
          Swal.fire({
            position: 'top-end',
            type: 'success',
            title: res.message,
            showConfirmButton: false,
            timer: 1500
          }).then(() => {
            let index = this.emissionBT.indexOf(e);
            this.emissionBT[index].extendPeriod = res.data.extendPeriod;
            this.emissionBT[index].endEmission = res.data.endEmission;
          })
        },
          error => {
            console.log(error);
          });
      }
    });
  }
  //Redirect to details card
  detailsCard(emissionId) {
    this._router.navigate(['/marketPlace/emissionDetails'], { queryParams: { 'emissionId': emissionId } })
  }
}