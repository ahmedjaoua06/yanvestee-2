import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BilletTresoListComponent } from './billet-treso-list.component';

describe('BilletTresoListComponent', () => {
  let component: BilletTresoListComponent;
  let fixture: ComponentFixture<BilletTresoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BilletTresoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BilletTresoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
