import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BilletTresoAddComponent } from './billet-treso-add.component';

describe('BilletTresoAddComponent', () => {
  let component: BilletTresoAddComponent;
  let fixture: ComponentFixture<BilletTresoAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BilletTresoAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BilletTresoAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
