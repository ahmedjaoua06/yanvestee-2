import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertifDepoListComponent } from './certif-depo-list.component';

describe('CertifDepoListComponent', () => {
  let component: CertifDepoListComponent;
  let fixture: ComponentFixture<CertifDepoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertifDepoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertifDepoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
