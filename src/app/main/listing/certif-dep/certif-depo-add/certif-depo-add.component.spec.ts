import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertifDepoAddComponent } from './certif-depo-add.component';

describe('CertifDepoAddComponent', () => {
  let component: CertifDepoAddComponent;
  let fixture: ComponentFixture<CertifDepoAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertifDepoAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertifDepoAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
