import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators, FormArray, FormControl } from '@angular/forms';
//Import Service Helper
import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import { Constants } from '@helper/constants';
import Swal from 'sweetalert2'
import { Router } from '@angular/router';

@Component({
  selector: 'app-certif-depo-add',
  templateUrl: './certif-depo-add.component.html',
  styleUrls: ['./certif-depo-add.component.scss']
})
export class CertifDepoAddComponent implements OnInit {
  formFileProduct: any;
  tableFicheProduit = [];
  step: number;
  formCertifDepot: FormGroup;
  ListHorizon = Constants.ListHorizon;
  CListHorizonPeriod = Constants.ListHorizonPeriod;
  ListHorizonPeriod = [];
  ListAnticipatedTermination = Constants.ListAnticipatedTermination;
  ListPaymentInterest = Constants.ListPaymentInterest;
  ListTaxationInter = Constants.ListTaxationInter;
  ListSubscOption = Constants.ListSubscOption;

  constructor(
    private _formBuilder: FormBuilder,
    private _serviceHelper: ServiceHelperService,
    private _router: Router
  ) { }

  ngOnInit() {
    //Init Form
    this.initForm();
    this.initStructFileProduct();
  }
  //Init Form
  initForm() {
    this.formCertifDepot = this._formBuilder.group({
      horizon: ['', Validators.required],
      period: [''],
      rateFixedNominal: ['', [Validators.required, Validators.min(0.25), Validators.max(5)]],
      anticipatedTerm: [0, Validators.required],
      rateAntipTerm: ['', [Validators.min(0), Validators.max(99.99)]],
      paymentInterest: ['', Validators.required],
      taxationInter: ['',Validators.required],
      miniInvest: [Constants.minimumInvestment_CD, [Validators.required, Validators.min(Constants.minimumInvestment_CD)]],
      subscribeOpt: this._formBuilder.array([1, 2], Validators.required),
    });
  }

  /**************************
   * Event Form 
   **************************/
  //Event Horizon Changed
  eventHorizonChanged(e) {
    console.log(e);
    this.ListHorizonPeriod = this.CListHorizonPeriod.filter(a => a.horizonId == e.value)
  }
  //Event Subsc Option Change
  onChangeSub(e) {
    const subs = <FormArray>this.formCertifDepot.get('subscribeOpt') as FormArray;
    if (e.checked) {
      subs.push(new FormControl(e.source.value))
    } else {
      const i = subs.controls.findIndex(x => x.value === e.source.value);
      subs.removeAt(i);
    }
  }
  //Event Change Resiliation
  changeResiliation(e) {
    console.log("changeResiliation");
    console.log(e);
    if (e.value == 1) {
      this.setValidatorField("rateAntipTerm");
    }
    else {
      this.ClearValidatorField("rateAntipTerm");
      this.formCertifDepot.patchValue({
        rateAntipTerm: ""
      });
    }
  }
  //Event Add Certif
  eventCertif(f) {

    Swal.fire({
      title: 'Voulez-vous confirmer votre émission ?',
      type: 'success',
      showCancelButton: true,
      confirmButtonText: 'Oui, Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        if (f.valid) {
          f.value.fileProduct = this.formFileProduct;
          this._serviceHelper.__post(Constants.CREATE_EMISSION_CD, { 'emission': f.value }).subscribe((res: any) => {
            console.log(res);
            Swal.fire({
              position: 'top-end',
              type: 'success',
              title: 'Votre émission a été publiée avec succés',
              showConfirmButton: false,
              timer: 1500
            }).then(() => {
              this._router.navigate(["/listing/certif"]);
            })
          },
            error => {
              console.log("Erreur");
              console.log("Error");
            });
        }
      }
    })
  }
  //Set Validator
  setValidatorField(field) {
    this.formCertifDepot.controls[field].setValidators([Validators.required, Validators.min(0), Validators.max(99.99)]);
    this.formCertifDepot.controls[field].updateValueAndValidity();
  }
  //Clear Validator
  ClearValidatorField(field) {
    this.formCertifDepot.controls[field].clearValidators();
    this.formCertifDepot.controls[field].updateValueAndValidity();
  }
  //Round Number
  roundNumber(e, field) {
    let _value = this.formCertifDepot.value[field];
    //Check if value different null
    if (_value != null) {
      _value = _value.toString().substring(0, _value.toString().indexOf(".") + 3)
      switch (field) {
        case 'rateFixedActuarial': {
          this.formCertifDepot.patchValue({ rateFixedActuarial: _value })
          break;
        }
        case 'rateFixedNominal': {
          this.formCertifDepot.patchValue({ rateFixedNominal: _value })
          break;
        }
        case 'rateAntipTerm': {
          this.formCertifDepot.patchValue({ rateAntipTerm: _value })
          break;
        }
      }
    }
  }
    // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Set step
   *
   * @param {number} index
   */
  setStep(index: number): void {
    this.step = index;
  }

  /**
   * Next step
   */
  nextStep(): void {
    this.step++;
  }

  /**
   * Previous step
   */
  prevStep(): void {
    this.step--;
  }
  //Init struct file product
  initStructFileProduct() {
    this.formFileProduct = [
      //Désignation du produit
      {
        title: 'Désignation du produit',
        content: 'Certificat de dépôt «CD»'
      },
      //Type du Produit
      {
        title: 'Type du Produit',
        content: 'titre de créance'
      },
      //Description du CT
      {
        title: 'Description du CT ',
        content: `
      Un compte à terme est un placement à durée déterminée auprès d’un établissement bancaire. Il s'agit, comme son nom l'indique, d'un placement financier à court ou moyen terme, rémunéré.
Le montant, l’échéance et le taux d’intérêt sont fixés dès l’ouverture du compte. 
Les placements ne peuvent être à effet rétroactifs.
      `
      },
      //Caractéristiques du CT
      {
        title: 'Caractéristiques du CT',
        children: [
          {
            title: 'Devise',
            content: 'TND'
          },
          {
            title: 'Nature des titres',
            content: 'titres de créance'
          },
          {
            title: 'Forme des titres',
            content: 'Nominative'
          },
          {
            title: 'Négociabilité',
            content: 'NON'
          },
          {
            title: 'Durée',
            content: 'La durée est comprise entre 3 mois et 5 ans.'
          },
          {
            title: 'Investissement minimum',
            content: '1000 DT.'
          },
          {
            title: 'Investissement maximum',
            content: 'N/A'
          },
          {
            title: 'Taux',
            content: 'Taux fixe si durée <= 1 an'
          },
          {
            title: 'Résiliation-Remboursement Anticipé',
            content: `
          Le Compte à terme ne peut pas être remboursé par  anticipation ni renouvelé par tacite reconduction.
        La banque peut consentir une avance au titulaire d'un dépôt à terme. Dans ce cas, la banque perçoit au moins quinze (15) jours d'intérêts calculés au taux appliqué au compte à terme, comportant une échéance majoré d'un (1) point de pourcentage.
        `
          },
          {
            title: 'Versement des intérêts',
            content: 'Les intérêts sont payables à terme échu :',
            children: [
              {
                content: "Lorsque la durée du placement est inférieure à une année, l'intérêt est payable en une seule fois à terme échu"
              },
              {
                content: "Lorsque la durée du placement est supérieure à une année, l'intérêt est payable à la fin de chaque période d'une année et à l'échéance pour la fraction d'année restante."
              }
            ]
          },
          {
            title: 'Imposition des intérêts',
            content: "Les intérêts servis sont assujettis à une  retenue à la source au taux en vigueur soit 20%.",
          },
          {
            title: 'Qui peut souscrire',
            content: "Personne Physique / Personne Morale / Non résident Hors US",
          },
          {
            title: 'Prix d’émission',
            content: "",
          },
          {
            title: 'Versement de Fonds',
            content: "L’investisseur prend attache directement avec l’émetteur pour concrétiser son investissement. Yanvestee n'assure pas des services de tenue de compte, de dépôt, de gestion ou de transfert de fonds.",
          },
          {
            title: 'Inscription en compte',
            content: "Obligatoire",
          },
          {
            title: 'Rémunération',
            content: `Les intérêts sont calculés en fonction de la durée du placement sur la base du Taux défini lors de l’ouverture du compte.
            Rémunération à l’échéance : remboursement du capital placé augmenté des intérêts.
            Les intérêts sont calculés en fonction de la durée du placement sur la base du Taux défini lors de l’ouverture du compte. Ils sont automatiquement versés au terme de chaque année suivant la date d’ouverture, et enfin à l’échéance.`,
          },
          {
            title: 'Identification',
            children: [
              {
                title: 'Code ISIN',
                content: 'NON'
              },
              {
                title: 'Code ISIN',
                content: 'NON'
              },
              {
                title: 'FISN',
                content: ''
              }
            ]
          }
        ]
      },
      //Risques
      {
        title: "Risques",
        children: [
          {
            title: "Risque de Défaut",
            content: `le remboursement du placement est soumis au risque d’insolvabilité de l'émetteur.`
          },
          {
            title: "Risque de taux",
            content: ``
          },
          {
            title: "Risque de liquidité",
            content: `risque existants vu que le produit est non négociable.`
          },
          {
            title: "Risque de variation des prix  ou risque de marché",
            content: ``
          },
          {
            title: "Risque commercial",
            content: `n’est pas encouru`
          },
          {
            title: "Risque de change",
            content: `Ce risque s’applique aux placements étrangers`
          },
          {
            title: "Risque lié à l’inflation",
            content: ``
          }
        ]
      },
      //Frais
      {
        title: "Frais",
        children: [
          {
            title: "YANVESTEE n’applique pas de frais pour l’investisseur"
          },
          {
            title: "YANVESTEE facture l’émetteur en appliquant 0,2 % sur le montant souscrit."
          }
        ]
      },
      //Fiscalité
      {
        title: "Fiscalité",
        content: "Les intérêts servis sont assujettis à une  retenue à la source au taux en vigueur soit 20%."
      },
      //Exigences particulières de l’émetteur
      {
        title: "Exigences particulières de l’émetteur",
        content: `Le compte à terme doit faire l'objet d'un contrat écrit entre la banque et son client fixant les conditions de dépôt en termes de montant, de taux d’intérêt et de durée de placement.`
      },
      //Avantages
      {
        title: "Avantages",
        children: [
          {
            title: "Rentabilité",
            content: "rendement connu d’avance."
          },
          {
            title: "Sécurité",
            content: "capital fructifie en toute sécurité."
          },
          {
            title: "Exonération fiscale",
            content: "des intérêts en cas de compte à terme en dinar convertible."
          }
        ]
      },
      //Cadre réglementaire
      {
        title: "Cadre réglementaire",
        content: "CIRCULAIRE de la BCT AUX BANQUES N° 91-22 DU 17 DECEMBRE 1991"
      }
    ]
  }
}
