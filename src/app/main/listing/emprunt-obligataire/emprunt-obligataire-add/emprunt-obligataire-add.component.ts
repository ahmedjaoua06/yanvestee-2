import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators, FormArray, FormControl } from '@angular/forms';
//Import Service Helper
import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import { Constants } from '@helper/constants';
import Swal from 'sweetalert2'
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';

export interface Banks {
	name: string;
}
export interface ExternalContact {
	email: string;
}

@Component({
	selector: 'emprunt-obligataire-add',
	templateUrl: './emprunt-obligataire-add.component.html',
	styleUrls: ['./emprunt-obligataire-add.component.scss']
})


export class EmpruntObligataireAddComponent implements OnInit {
	listSecteur = Constants.ListSecteur;
	listCategories = Constants.ListCategoriesContact;
	RatingFormPerspective = Constants.RatingFormPerspective;
	formFilter: FormGroup;
	formCompteTerme: FormGroup;
	form: FormGroup;
	ListSecteur = Constants.ListSecteur;
	ListJuridicForm = Constants.ListJuridicForm;
	RatingForm = [];
	RatingFormCourt = [];
	ListRatingForm = Constants.ListRatingForm;
	ListNumber = Constants.NumberForm;
	ListHorizonPeriod = [];
	regexWebsite: any;
	regexEmail: any;
	statusChecked: boolean;
	toggleChecked: boolean;
	toggleCheckedPublic: boolean;
	empruntModifiable: boolean;
	garantie: boolean;
	appelPublic: boolean;
	secondRating: boolean;
	addConseiller: boolean;
	tauxFixeBoolean: boolean;
	myContacts: any;
	chipList: any;
	categoryNameList: any;
	selectedContacts: any;
	NoteOperation: any;
	DocumentJuridique: any;
	DocumentComptable: any;
	favoriteSeason: String;
	todayDate: Date;
	firstDate: Date;
	yesterdayDate: Date;
	horizontalStepperStep1: FormGroup;
	horizontalStepperStep2: FormGroup;
	horizontalStepperStep3: FormGroup;
	horizontalStepperStep4: FormGroup;
	horizontalStepperStep5: FormGroup;
	horizontalStepperStep6: FormGroup;
	timeout: any = null;
	private _unsubscribeAll: Subject<any>;

	visible = true;
	selectable = true;
	removable = true;
	addOnBlur = true;
	readonly separatorKeysCodes: number[] = [ENTER, COMMA];
	banks: Banks[] = [];
	externalContacts: ExternalContact[] = [];

	add(event: MatChipInputEvent): void {
		const input = event.input;
		const value = event.value;

		// Add our fruit
		if ((value || '').trim()) {
			if (this.banks.length < 5)
				this.banks.push({ name: value.trim() });
		}

		// Reset the input value
		if (input) {
			input.value = '';
		}
	}

	remove(contact: ExternalContact): void {
		const index = this.externalContacts.indexOf(contact);

		if (index >= 0) {
			this.externalContacts.splice(index, 1);
		}
	}

	addContact(event: MatChipInputEvent): void {
		const input = event.input;
		const value = event.value;

		// Add our fruit
		if ((value || '').trim()) {
			this.externalContacts.push({ email: value.trim() });
		}

		// Reset the input value
		if (input) {
			input.value = '';
		}
	}

	removeContact(contact: Banks): void {
		const index = this.banks.indexOf(contact);

		if (index >= 0) {
			this.banks.splice(index, 1);
		}
	}
	initCategory() {
		return this._formBuilder.group({
			maturity: [1],
			amortieCheckbox: [true],
			InFineCheckbox: [false],
			AnuelleCheckbox: [true],
			SemestrielleCheckbox: [false],
			frequenceCoupon: ["Annuelle"],
			ModaliteRemboursementType: ['Amortie'],
			GraceYearBoolean: [false],
			GraceYearValue: [{ value: 0, disabled: true }],
			TauxFixeBoolean: [false],
			TauxFixeValue: [{ value: null, disabled: true }],
			TauxVariableBoolean: [false],
			TauxVaraibleValue: [{ value: 3, disabled: true }],
			ModalitéRemboursement: this._formBuilder.array([1], Validators.required),
			FrequenceCoupon: this._formBuilder.array([1], Validators.required),
			TwoRates: this._formBuilder.array([], Validators.required),
		});
	}
	changeEnable(e, index, typeEnable, typeBoolean) {
		if (e.checked) {
			this.horizontalStepperStep3.get('categories')['controls'][index]['controls'][typeEnable].enable();
			this.horizontalStepperStep3.get('categories')['controls'][index]['controls'][typeEnable].setValidators(Validators.compose([Validators.required, Validators.min(0)]));
			this.horizontalStepperStep3.get('categories')['controls'][index]['controls'][typeEnable].setValue(null);
			this.horizontalStepperStep3.get('categories')['controls'][index]['controls'][typeBoolean].setValue(true);

			if (typeEnable == "GraceYearValue") {
				this.horizontalStepperStep3.get('categories')['controls'][index]['controls'][typeEnable].setValidators(Validators.compose([Validators.required, Validators.min(0), Validators.max(this.horizontalStepperStep3.get('categories')['controls'][index].value.maturity)]));


			}

		}
		else {
			this.horizontalStepperStep3.get('categories')['controls'][index]['controls'][typeEnable].disable();
			this.horizontalStepperStep3.get('categories')['controls'][index]['controls'][typeBoolean].setValue(false)
		}
	}

	ChangeRatingSolution(eltp) {
		console.log(eltp)
		this.RatingForm = eltp.ratingLong;
		this.RatingFormCourt = eltp.ratingCourt;
	}
	ChangeGradeAndQuality(eltp) {
		var GradeInput = this.horizontalStepperStep2.get('Grade');
		GradeInput.setValue(this.RatingForm[eltp].grade)
		GradeInput.updateValueAndValidity();
		var QualitéInput = this.horizontalStepperStep2.get('Qualité');
		QualitéInput.setValue(this.RatingForm[eltp].qualité)
		QualitéInput.updateValueAndValidity();
	}

	checkValidityCheckbox(index, e, validation) {
		const subs = <FormArray>this.horizontalStepperStep3.get('categories')['controls'][index].get(validation) as FormArray;
		if (e.checked) {
			subs.push(new FormControl(e.source.value))
		} else {
			const i = subs.controls.findIndex(x => x.value === e.source.value);
			subs.removeAt(i);
		}
	}
	onSelection(e, v) {
		console.log("Selection list");
		for (let a of v) {
			console.log(a.value);
		}
	}

	convertFileToBase64(file, fileNumber) {
		let base64data: any = "";
		console.log(fileNumber)
		var reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onloadend = (e) => {
			var myFile = { myData: reader.result, name: file.name }
			if (myFile.name.length > 20) {
				myFile.name = myFile.name.slice(0, 20) + "..."
			}
			if (fileNumber == 1) {

				this.NoteOperation.push(myFile);
			} else if (fileNumber == 2) {
				this.DocumentJuridique.push(myFile);
			} else if (fileNumber == 3) {
				this.DocumentComptable.push(myFile);
			} else {
				console.log('error in sending file')
			}

		};
	}

	removeDocument(index, fileNumber) {
		if (fileNumber == 1) {
			this.NoteOperation.splice(index, 1);
		} else if (fileNumber == 2) {
			this.DocumentJuridique.splice(index, 1);
		} else if (fileNumber == 3) {
			this.DocumentComptable.splice(index, 1);
		} else {
			console.log('error in removing file')
		}

	}

	//Event File Selected
	fileSelected(file, fileNumber) {
		console.log('myfile' + fileNumber)
		let validSizeFile = false;
		let validTypeFile = false;
		let acceptType = ["pdf"];
		//Check size image lte 2 Mo
		file[0].size <= 2048576 ? validSizeFile = true : (validSizeFile = false, this._toastr.warning("Taille du fichier invalide (max 2mo)"));
		//Check type image accept only png jpg jpeg
		let type = file[0].type.split('/')[1];
		type = acceptType.find(a => a == type.toLocaleLowerCase());
		type != undefined ? validTypeFile = true : (validTypeFile = false, this._toastr.warning("Type de fichier invalide (type de fichiers autorisés: pdf)"));
		//Check if file valid
		if (validSizeFile && validTypeFile) {
			this.convertFileToBase64(file[0], fileNumber);
		}
	}
	checkSeason(mySeason) {
		var AgenceNotation = this.horizontalStepperStep2.get('AgenceNotation');
		var DateRating = this.horizontalStepperStep2.get('DateRating');
		var NotationEmprunt = this.horizontalStepperStep2.get('NotationEmprunt');
		var SecondRatingCourt = this.horizontalStepperStep2.get('SecondRatingCourt');
		var GradeInput = this.horizontalStepperStep2.get('RatingRadio');
		console.log(GradeInput.value)
		if (GradeInput.value == "Rating") {
			this.secondRating = true;
			this.garantie = false;
			AgenceNotation.setValidators([Validators.required]);
			DateRating.setValidators([Validators.required]);
			// NotationEmprunt.setValidators([Validators.required]);
			SecondRatingCourt.setValidators([Validators.required]);
		} else {
			this.secondRating = false;
			this.garantie = true;
			AgenceNotation.setValidators(null);
			DateRating.setValidators(null);
			// NotationEmprunt.setValidators(null);
			SecondRatingCourt.setValidators(null);
		}
		AgenceNotation.updateValueAndValidity();
		DateRating.updateValueAndValidity();
		// NotationEmprunt.updateValueAndValidity();
		SecondRatingCourt.updateValueAndValidity();
	}


	constructor(
		private _formBuilder: FormBuilder,
		private _serviceHelper: ServiceHelperService,
		private _router: Router,
		private _toastr: ToastrService
	) { }

	ngOnInit(): void {
		this.listSecteur.push({ label: "Tout", value: undefined });
		this.listCategories.push({ label: "Tout", value: undefined });

		this.todayDate = new Date();
		this.firstDate = new Date("01/01/1970");
		this.yesterdayDate = new Date(Date.now() - 864e5);
		this.statusChecked = false;
		this.toggleChecked = true;
		this.empruntModifiable = false;
		this.garantie = false;
		this.appelPublic = false;

		this.addConseiller = false;
		this.tauxFixeBoolean = false;
		this.toggleCheckedPublic = false;
		this.NoteOperation = [];
		this.DocumentJuridique = [];
		this.DocumentComptable = [];
		/*		this.favoriteSeason="season"*/
		this.categoryNameList = ["A", "B", "C", "D", "E"];
		this.regexWebsite = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
		this.regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		//Init form filter
		this.initFormFilter();
		//Get All Contact
		this.getAllContact();

		this._serviceHelper.__get("users/profile").subscribe((res: any) => {
			if (res.categorie == "Conseil") {

			} else {
				this.horizontalStepperStep1.get('RaisonSocial').setValue(res.societe);
				this.horizontalStepperStep1.get('Secteur').setValue(res.categorie);
				this.horizontalStepperStep1.get('Adresse').setValue(res.adresse);
				this.horizontalStepperStep1.get('LEI').setValue(res.lei);
				this.horizontalStepperStep1.get('RatingLong').setValue(res.ratinglong);
				this.horizontalStepperStep1.get('RatingCourt').setValue(res.ratingcourt);
				this.horizontalStepperStep1.get('AgenceDeNotation').setValue(res.agencenotation);
				this.horizontalStepperStep1.get('MatriculeFiscal').setValue(res.mf);
				this.horizontalStepperStep1.get('DateNotation').setValue(res.infoDateNotation);
				this.horizontalStepperStep1.get('SiteWeb').setValue(res.website);
				this.horizontalStepperStep1.get('JuridicForm').setValue(res.infoJuridicForm);
				this.horizontalStepperStep1.get('perspective').setValue(res.perspective);


				/*ListRatingForm*/
				let obj = this.ListRatingForm.find(o => o.value === res.agencenotation);
				this.RatingForm = obj.ratingLong;
				this.RatingFormCourt = obj.ratingCourt;
			}
		},
			error => {
				console.log(error);
			})


		// Horizontal Stepper form steps
		this.horizontalStepperStep1 = this._formBuilder.group({
			RaisonSocial: ['', Validators.compose([Validators.required, Validators.maxLength(30)])],
			Secteur: ['', Validators.required],
			JuridicForm: ['', Validators.required],
			Adresse: ['', Validators.compose([Validators.required, Validators.maxLength(60)])],
			SiteWeb: ['http://www', Validators.compose([Validators.required, Validators.pattern(new RegExp(this.regexWebsite))])],
			MatriculeFiscal: ['', Validators.compose([Validators.required, Validators.pattern(new RegExp(/^[0-9]{6}[a-zA-Z]$/))])],
			DateDecisionAGO: [''],
			DateConseilAdd: [''],
			AgenceDeNotation: ['', Validators.required],
			perspective: ['', Validators.required],
			RatingLong: ['', Validators.required],
			RatingCourt: ['', Validators.required],
			DateNotation: ['', Validators.required],
			StatusObligatoire: [this.statusChecked, Validators.required],
			Rating: [this.toggleChecked, Validators.required],
			LEI: ['', Validators.compose([Validators.pattern(new RegExp(/^[a-zA-Z0-9]{18}[0-9]{2}$/))])],
		});

		this.horizontalStepperStep2 = this._formBuilder.group({
			nameEmprunt: ['', Validators.required],
			Montant: [1000000, Validators.compose([Validators.required, Validators.pattern(new RegExp(/^[1-9]\d*000000/))])],

			MontantModifiable: [100, Validators.required],

			EmpruntModifiable: [this.empruntModifiable, Validators.required],
			PrixEmission: [100, Validators.required],
			DateDemmarage: ['', Validators.required],
			DateCloture: ['', Validators.required],
			DateJouissance: ['', Validators.required],
			AgenceNotation: [''],
			DateRating: [''],
			NotationEmprunt: [''],
			SecondRatingCourt: [''],
			AppelPublic: [this.appelPublic, Validators.required],
			VisaCMF: ['',],
			Jort: ['',],
			DataVisa: [''],
			DateJort: [''],
			Grade: [''],
			Qualité: [''],
			miniInvest: ['', [Validators.required, Validators.min(100)]],
			RatingRadio: [undefined, Validators.required],
			CategoryTitre: ['Ordinaires']

		});

		this.horizontalStepperStep3 = this._formBuilder.group({
			categoryNumber: [1, Validators.required],
			categories: this._formBuilder.array([
				this.initCategory(),

			])
		});

		this.horizontalStepperStep4 = this._formBuilder.group({
			AddConseiller: [this.addConseiller, Validators.required],
			ConseillerSociete: [''],
			ConseillerNom: [''],
			ConseillerPrenom: [''],
			ConseillerPhone: [''],
			ConseillerEmail: [''],
			ConseillerSite: [''],
			ConseillerId: ['']

		});

		this.horizontalStepperStep5 = this._formBuilder.group({
			ToggleCheckedPublic: [this.toggleCheckedPublic],
			myContacts: new FormControl(''),
			myExtrenalContacts: new FormControl(this.externalContacts)
		});
		this.horizontalStepperStep6 = this._formBuilder.group({

		});
	}
	//Get All contact
	getAllContact() {
		this._serviceHelper.__post(Constants.GET_ALL_CONTACT_EM, { searchQuerry: this.formFilter.value }).subscribe((res: any) => {
			this.myContacts = res.data;
			this.myContacts.forEach((el) => {
				el.badge = el.lastname.substring(0, 1) + el.firstname.substring(0, 1);
			});
		},
			error => {
				console.log(error);
			})
	}

	/**
	 * On destroy
	 */
	ngOnDestroy(): void {
		// Unsubscribe from all subscriptions
		/* 	this._unsubscribeAll.next();
		this._unsubscribeAll.complete();*/
	}

	finishHorizontalStepper(): void {
		alert('You have finished the horizontal stepper!');
	}
	loging() {
		console.log(this.horizontalStepperStep5.value)
	}
	addLanguage() {
		console.log('rrrr')
		let control = <FormArray>this.horizontalStepperStep3.controls['categories'];
		control.controls = [];
		var numberCategory = this.horizontalStepperStep3.get('categoryNumber').value;
		var myArray = []
		for (var i = 0; i < numberCategory; i++) {
			control.push(this.initCategory())
		}
		/*this.horizontalStepperStep3.get('categories').setValue(myArray)*/
		//this.horizontalStepperStep3.controls['categories']= this._formBuilder.array(myArray);
		/*control.push(this.initCategory());*/
	}
	checkValue(checkStatus: any) {

		var DateDecisionAGO = this.horizontalStepperStep1.get('DateDecisionAGO');
		var DateConseilAdd = this.horizontalStepperStep1.get('DateConseilAdd');
		if (checkStatus) {

			DateDecisionAGO.setValidators([Validators.required]);
			DateConseilAdd.setValidators([Validators.required]);
		} else {
			DateDecisionAGO.setValidators(null);
			DateConseilAdd.setValidators(null);
		}
		DateDecisionAGO.updateValueAndValidity();
		DateConseilAdd.updateValueAndValidity();
	}


	changeValueEmprunt(checkStatus: any) {

		var MontantModifiable = this.horizontalStepperStep2.get('MontantModifiable');
		var Montant = this.horizontalStepperStep2.get('Montant');
		if (checkStatus) {
			MontantModifiable.setValue(this.horizontalStepperStep2.get('Montant').value);
			MontantModifiable.setValidators(Validators.compose([Validators.required, Validators.maxLength(30), Validators.max(Montant.value * 2)]));
		} else {
			MontantModifiable.setValidators(null);
		}
		MontantModifiable.updateValueAndValidity();
	}

	checkValidityMontant() {
		var MontantModifiable = this.horizontalStepperStep2.get('MontantModifiable').value;
		var Montant = this.horizontalStepperStep2.get('Montant').value;
		if ((MontantModifiable > 2 * Montant) || (MontantModifiable < 0)) {
			this.horizontalStepperStep2.get('MontantModifiable').setErrors({ 'incorrect': true });
		}
	}

	checkValidityTauxFixe(index) {
		console.log('rrrr55')
		console.log(this.horizontalStepperStep3.get('categories'))
		console.log(this.horizontalStepperStep3.get('categories').value[index].TauxFixeValue)
		var MontantModifiable = this.horizontalStepperStep3.get('categories').value[index].TauxFixeValue;
		if (MontantModifiable < 0) {
			/*this.horizontalStepperStep3.controls[0]*/
		}
	}

	checkValidityPrixEmission() {
		var MontantModifiable = this.horizontalStepperStep2.get('PrixEmission').value;
		if (MontantModifiable < 100) {
			this.horizontalStepperStep2.get('PrixEmission').setErrors({ 'incorrect': true });
		}
	}


	checkChangeRating(checkStatus: any) {
		var AgenceNotation = this.horizontalStepperStep2.get('AgenceNotation');
		var DateRating = this.horizontalStepperStep2.get('DateRating');
		var NotationEmprunt = this.horizontalStepperStep2.get('NotationEmprunt');
		var SecondRatingCourt = this.horizontalStepperStep2.get('SecondRatingCourt');

		if (checkStatus) {
			AgenceNotation.setValidators([Validators.required]);
			DateRating.setValidators([Validators.required]);
			// NotationEmprunt.setValidators([Validators.required]);
			SecondRatingCourt.setValidators([Validators.required]);
		} else {
			AgenceNotation.setValidators(null);
			DateRating.setValidators(null);
			// NotationEmprunt.setValidators(null);
			SecondRatingCourt.setValidators(null);
		}
		AgenceNotation.updateValueAndValidity();
		DateRating.updateValueAndValidity();
		// NotationEmprunt.updateValueAndValidity();
		SecondRatingCourt.updateValueAndValidity();


	}


	checkToggleValue(checkStatus: any) {
		var AgenceDeNotation = this.horizontalStepperStep1.get('AgenceDeNotation');
		var DateNotation = this.horizontalStepperStep1.get('DateNotation');
		var RatingLong = this.horizontalStepperStep1.get('RatingLong');
		var RatingCourt = this.horizontalStepperStep1.get('RatingCourt');
		if (checkStatus) {

			AgenceDeNotation.setValidators([Validators.required]);
			DateNotation.setValidators([Validators.required]);
			RatingLong.setValidators([Validators.required]);
			RatingCourt.setValidators([Validators.required]);
		} else {
			AgenceDeNotation.setValidators(null);
			DateNotation.setValidators(null);
			RatingLong.setValidators(null);
			RatingCourt.setValidators(null);
		}
		AgenceDeNotation.updateValueAndValidity();
		DateNotation.updateValueAndValidity();
		RatingLong.updateValueAndValidity();
		RatingCourt.updateValueAndValidity();
	}

	checkToggleAppelPublicValue(checkStatus: any) {
		var VisaCMF = this.horizontalStepperStep2.get('VisaCMF');
		var Jort = this.horizontalStepperStep2.get('Jort');
		var DataVisa = this.horizontalStepperStep2.get('DataVisa');
		var DateJort = this.horizontalStepperStep2.get('DateJort');

		if (checkStatus) {
			VisaCMF.setValidators(Validators.compose([Validators.required, Validators.pattern(new RegExp(/^[0-9]{6}$/))]));
			Jort.setValidators(Validators.compose([Validators.required, Validators.pattern(new RegExp(/^[0-9]{3} du [0-9]{2}\/[0-9]{2}\/[0-9]{4}$/))]));
			DataVisa.setValidators([Validators.required]);
			DateJort.setValidators([Validators.required]);
		} else {
			VisaCMF.setValidators(null);
			Jort.setValidators(null);
			DataVisa.setValidators(null);
			DateJort.setValidators(null);
		}
		VisaCMF.updateValueAndValidity();
		Jort.updateValueAndValidity();
		DataVisa.updateValueAndValidity();
		DateJort.updateValueAndValidity();
	}

	displayName() {
		var serviceHelp = this._serviceHelper;
		var emailValue = this.horizontalStepperStep4.get('ConseillerEmail').value;
		var horizontalStepperStep4 = this.horizontalStepperStep4;
		clearTimeout(this.timeout);

		// Make a new timeout set to go off in 800ms
		this.timeout = setTimeout(function () {
			console.log('Input Value:');
			serviceHelp.__post("users/getUserByEmail", { email: emailValue }).subscribe((res: any) => {
				console.log("Contact")
				console.log(res)
				if (res != null) {
					horizontalStepperStep4.get('ConseillerPrenom').setValue(res.firstname);
					horizontalStepperStep4.get('ConseillerNom').setValue(res.lastname);
					horizontalStepperStep4.get('ConseillerPhone').setValue(res.tel);
					horizontalStepperStep4.get('ConseillerSociete').setValue(res.societe);
					horizontalStepperStep4.get('ConseillerId').setValue(res._id);
				}
			},
				error => {
					console.log(error);
				})
		}, 500);
	}

	checkToggleAddConseiller(checkStatus: any) {
		var ConseillerSociete = this.horizontalStepperStep4.get('ConseillerSociete');
		var ConseillerNom = this.horizontalStepperStep4.get('ConseillerNom');
		var ConseillerPrenom = this.horizontalStepperStep4.get('ConseillerPrenom');
		var ConseillerPhone = this.horizontalStepperStep4.get('ConseillerPhone');
		var ConseillerEmail = this.horizontalStepperStep4.get('ConseillerEmail');
		var ConseillerSite = this.horizontalStepperStep4.get('ConseillerSite');

		if (checkStatus) {
			ConseillerSociete.setValidators([Validators.required]);
			ConseillerNom.setValidators([Validators.required]);
			ConseillerPrenom.setValidators([Validators.required]);
			ConseillerPhone.setValidators([Validators.required]);
			ConseillerEmail.setValidators([Validators.compose([Validators.required, Validators.pattern(new RegExp(this.regexEmail))])]);
			ConseillerSite.setValidators([Validators.compose([Validators.required, Validators.pattern(new RegExp(this.regexWebsite))])]);
			ConseillerSite.setValue("http://www.");
		} else {
			ConseillerSociete.setValidators(null);
			ConseillerNom.setValidators(null);
			ConseillerPrenom.setValidators(null);
			ConseillerPhone.setValidators(null);
			ConseillerEmail.setValidators(null);
			ConseillerSite.setValidators(null);
		}
		ConseillerSociete.updateValueAndValidity();
		ConseillerNom.updateValueAndValidity();
		ConseillerPrenom.updateValueAndValidity();
		ConseillerPhone.updateValueAndValidity();
		ConseillerEmail.updateValueAndValidity();
		ConseillerSite.updateValueAndValidity();
	}

	eventCertif(publicity) {
		if (!publicity) {
			this.horizontalStepperStep5.get('ToggleCheckedPublic').setValue(true);
		} else {
			this.horizontalStepperStep5.get('ToggleCheckedPublic').setValue(false);
		}
		Swal.fire({
			title: 'Voulez-vous confirmer votre émission ?',
			type: 'success',
			showCancelButton: true,
			confirmButtonText: 'Oui, Confirmer',
			cancelButtonText: 'Annuler'
		}).then((result) => {
			if (result.value) {
				this._serviceHelper.__post(Constants.CREATE_EMISSION_EO, { 'info': this.horizontalStepperStep1.value, emprunt: this.horizontalStepperStep2.value, remboursement: this.horizontalStepperStep3.value, emetteur: this.horizontalStepperStep4.value, acces: this.horizontalStepperStep5.value, noteOperation: this.NoteOperation, documentJuridique: this.DocumentJuridique, documentComptable: this.DocumentComptable }).subscribe((res: any) => {
					console.log(res);
					Swal.fire({
						position: 'top-end',
						type: 'success',
						title: 'Votre émission a été publiée avec succés',
						showConfirmButton: false,
						timer: 1500
					}).then(() => {
						this._router.navigate(["/listing/emprunt-obligataire"]);
					})
				},
					error => {
						console.log("Erreur");
						console.log("Error");
						console.log(error)
					});

			}
		})
	}
	//donwload pdf
	donwloadPdf() {
		var data = document.getElementById('contentToConvert');
		html2canvas(data).then(canvas => {
			// Few necessary setting options  
			var imgWidth = 208;
			var pageHeight = 295;
			var imgHeight = canvas.height * imgWidth / canvas.width;
			var heightLeft = imgHeight;

			const contentDataURL = canvas.toDataURL('image/png')
			let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
			var position = 20;
			pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
			let currentDate = new Date();
			let dateFile = (currentDate.getDate() + "-" + (currentDate.getMonth() < 9 ? ("0" + (currentDate.getMonth() + 1)) : (currentDate.getMonth() + 1)) + "-" + currentDate.getFullYear()).toString()
			this.generateBase64().then((data) => {
				pdf.addImage(data, 'PNG', 0, 5, 15, 15);
				pdf.setFontSize(13);
				pdf.text(17, 13, 'Yanvestee')
				pdf.save('emprunt-obligataire' + dateFile + '.pdf'); // Generated PDF   
			});
		});
	}
	//Convert Image to Base64
	generateBase64() {
		return new Promise(function (resolve, reject) {
			var xhr = new XMLHttpRequest();
			xhr.open("GET", "/assets/Logos/logo.png", true);
			xhr.responseType = "blob";
			xhr.onload = function (e) {
				console.log(this.response);
				var reader = new FileReader();
				reader.onload = function (event) {
					var res = event.target['result'];
					resolve(res);
				}
				var file = this.response;
				console.log("file");
				console.log(file);
				reader.readAsDataURL(file)
			};
			xhr.send();
		});
	}
	/**
	 * Finish the vertical stepper
	 */
	finishVerticalStepper(): void {
		alert('You have finished the vertical stepper!');
	}
	//Event change
	changeMaturity(e, index) {
		debugger;
		this.horizontalStepperStep3.get('categories')['controls'][index]['controls']['GraceYearValue'].setValidators(Validators.compose([Validators.required, Validators.min(0), Validators.max(this.horizontalStepperStep3.get('categories')['controls'][index].value.maturity)]));
	}
	//Event change search query
	changeSearchQuery(e, field) {
		switch (field) {
			case 'name':
				this.formFilter.patchValue({
					name: e.srcElement.value
				});
				break;
			case 'category':
				this.formFilter.patchValue({
					category: e.value
				});
				break;
			case 'company':
				this.formFilter.patchValue({
					company: e.srcElement.value
				});
				break;
			case 'sector':
				this.formFilter.patchValue({
					sector: e.value
				});
				break;
			case 'fonction':
				this.formFilter.patchValue({
					fonction: e.srcElement.value
				});
				break;
		}
		this.getAllContact();
	}
	//Init form filter
	initFormFilter() {
		this.formFilter = this._formBuilder.group({
			tags: [[]],
			name: [''],
			category: [''],
			company: [''],
			sector: [''],
			fonction: ['']
		});
	}
}
