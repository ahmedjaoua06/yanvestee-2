import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompteTermeAddComponent } from './compte-terme-add.component';

describe('CompteTermeAddComponent', () => {
  let component: CompteTermeAddComponent;
  let fixture: ComponentFixture<CompteTermeAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompteTermeAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompteTermeAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
