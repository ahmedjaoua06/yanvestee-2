import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarketCardsComponent } from './marketCards.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';
import { CountdownModule } from 'ngx-countdown';

import { RouterModule } from '@angular/router';


import {
	MatButtonModule, MatChipsModule, MatExpansionModule, MatFormFieldModule, MatIconModule, MatInputModule, MatPaginatorModule, MatRippleModule, MatSelectModule, MatSnackBarModule,
	MatSortModule,
	MatTableModule, MatTabsModule, MatDialogModule, MatButtonToggleModule
} from '@angular/material';


const routes = [
	{
		path: '',
		component: MarketCardsComponent
	}
];

@NgModule({
	declarations: [MarketCardsComponent],
	imports: [
		CommonModule,
		MatFormFieldModule,
		MatInputModule,
		MatButtonModule,
		MatChipsModule,
		MatIconModule,
		MatExpansionModule,
		MatDialogModule,
		CountdownModule,
		MatButtonToggleModule,
		MatTableModule,
		MatRippleModule,
		MatSelectModule,
		MatSnackBarModule,
		MatPaginatorModule,
		FuseSharedModule,
		FuseSidebarModule,
		FuseWidgetModule,
		RouterModule.forChild(routes),

	]
})
export class MarketCardsModule { }
