import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator, MatSort, MatChipInputEvent, MatTabGroup } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject, } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { ServiceHelperService } from '@serviceHelper/service-helper.service';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

/*import { EcommerceProductsService } from 'app/main/apps/e-commerce/products/products.service';*/
import { takeUntil } from 'rxjs/internal/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { Constants } from '@helper/constants';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import Swal from 'sweetalert2';
import { FuseConfigService } from '@fuse/services/config.service';
import { environment } from 'environments/environment';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { saveAs } from 'file-saver';
import { ToastrService } from 'ngx-toastr';

import { interval } from 'rxjs';
import * as moment from 'moment';
import * as _ from 'lodash';

@Component({
    selector: 'invoice-modern',
    templateUrl: './emissionDetails.component.html',
    styleUrls: ['./emissionDetails.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class EmissionDetailsComponent implements OnInit {
    fileProduct = Constants.FileProduct;
    listSecteur = Constants.ListSecteur;
    listCategories = Constants.ListCategoriesContact;
    @ViewChild(MatTabGroup) tabGroup: MatTabGroup;
    private urlLogo = "";
    displayedColumns: string[] = ['firstname', 'amount', 'rate', 'status', 'Accepter/Refuser'];
    //List my investments
    listmyinvestment = [];
    investmentsColumns: string[] = ['amount', 'createdDate', 'status'];
    isEmissionOwner = false;
    isDisableBtn = false;
    listInvestment = [];
    dataSource: any;
    emissionId: string;
    isInvested = false;
    investmentMessages = [];
    limitPage = 10;
    pageIndex = 1;
    total = 0;

    gaugeType = "full";
    gaugeValue = 0;
    gaugeLabel = "en cours";
    gaugeAppendText = "TND";
    thick = 12;

    //Invite Form
    invitForm: FormGroup;
    externalContacts: any[] = [];
    removable = true;
    addOnBlur = true;
    selectedContacts: any;
    max = 0;
    readonly separatorKeysCodes: number[] = [ENTER, COMMA];

    //Form investor EO
    formInvEO: FormGroup;
    formFilter: FormGroup;

    folders: any[] = [
        {
            name: 'Photos',
            updated: new Date('1/1/16'),
        },
        {
            name: 'Recipes',
            updated: new Date('1/17/16'),
        },
        {
            name: 'Work',
            updated: new Date('1/28/16'),
        }
    ];
    notes: any[] = [
        {
            name: 'Vacation Itinerary',
            updated: new Date('2/20/16'),
        },
        {
            name: 'Kitchen Remodel',
            updated: new Date('1/18/16'),
        }
    ];
    // Private
    private _unsubscribeAll: Subject<any>;

    private amountForm: FormGroup;
    myContacts: any;
    _trialEndsAt: string;
    Time: any;
    ValueProgress = 0;
    NbInves: any;
    ResultDate: boolean;
    empruntMontant: any;
    dataInv: any;
    somme: number;
    nbinvestment: number;


    constructor(
        private _formBuilder: FormBuilder,
        private _serviceHelper: ServiceHelperService,
        private _activateRoute: ActivatedRoute,
        private _fuseConfigService: FuseConfigService,
        private _toastr: ToastrService,
        private _router: Router
    ) {


        // Set the private defaults
        this._unsubscribeAll = new Subject();
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    folded: false
                }
            }
        };



    }


    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit() {
        this.listSecteur.push({ label: "Tout", value: undefined });
        this.listCategories.push({ label: "Tout", value: undefined });

        //Get Params
        this._activateRoute.queryParams.subscribe((params) => {
            this.emissionId = params.emissionId;
        });
        this.initForm()
        //Init form filter
        this.initFormFilter();
        this._serviceHelper.__get("emissions/getEmission", this.emissionId).subscribe((res: any) => {
            this.dataSource = res;
            //Children
            var objChildren = {
                categoryA: undefined,
                categoryB: undefined,
                categoryC: undefined,
                categoryD: undefined,
                categoryE: undefined
            }
            //Init Form
            if (this.dataSource.type != "Emprunt obligataire") {
                this.amountForm.controls["amount"].setValidators([Validators.required, Validators.min(this.dataSource.miniInvest)]);
                this.fileProduct = res.fileProduct;
            }

            this.initFormIntEO();
            if (this.dataSource.children != undefined) {
                this.dataSource.children['0'] != undefined ? (objChildren.categoryA = this.dataSource.children['0'], this.addCategory("Categorie A", this.dataSource.children['0'])) : objChildren.categoryA = undefined;
                this.dataSource.children['1'] != undefined ? (objChildren.categoryB = this.dataSource.children['1'], this.addCategory("Categorie B", this.dataSource.children['1'])) : objChildren.categoryB = undefined;
                this.dataSource.children['2'] != undefined ? (objChildren.categoryC = this.dataSource.children['2'], this.addCategory("Categorie C", this.dataSource.children['2'])) : objChildren.categoryC = undefined;
                this.dataSource.children['3'] != undefined ? (objChildren.categoryD = this.dataSource.children['3'], this.addCategory("Categorie D", this.dataSource.children['3'])) : objChildren.categoryD = undefined;
                this.dataSource.children['4'] != undefined ? (objChildren.categoryE = this.dataSource.children['4'], this.addCategory("Categorie E", this.dataSource.children['4'])) : objChildren.categoryE = undefined;


                this.dataSource.children = objChildren;
            }

            if (this.dataSource.miniInvest)
                this.dataSource.miniInvest = this.dataSource.miniInvest.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ')
            this.empruntMontant = res.EmpruntMontant;
            //Check if logo user exist
            var defaultImage = res.emetteurId.role == Constants.emetteur ? Constants.default_image_user_em : Constants.default_image_user_iv;
            if (res.emetteurId.logo != undefined) {
                this.urlLogo = res.emetteurId.logo.path != undefined ? environment.api + res.emetteurId.logo.path : defaultImage;
            }
            else {
                this.urlLogo = defaultImage;
            }
            //Set id emission in form
            this.amountForm.patchValue({
                emissionId: res._id
            });
            //If rate rate fix
            if (res.RembourseTauxFixeAmount)
                this.amountForm.patchValue({ rate: 1 })
            //If rate variable
            if (res.RembourseTauxVariableAmount)
                this.amountForm.patchValue({ rate: 2 })
            //If rate acturial
            if (res.rateFixedNominal)
                this.amountForm.patchValue({ rate: 3 })


            if (this.dataSource.noteOperation != undefined)
                this.dataSource.noteOperation.find(a => a.name = a.name.substring(0, 10));
            if (this.dataSource.documentJuridique != undefined)
                this.dataSource.documentJuridique.find(a => a.name = a.name.substring(0, 10));
            if (this.dataSource.documentComptable != undefined)
                this.dataSource.documentComptable.find(a => a.name = a.name.substring(0, 10));



            this._trialEndsAt = res.endEmission;

            //Create current Date
            var currentDate = moment();
            //Get end emission
            var endEmission = moment(this._trialEndsAt);
            //Set countdown to datasource
            var diffSec = endEmission.diff(currentDate, 'seconds');
            var numdays = Math.floor(diffSec / 86400);

            var numhours = Math.floor((diffSec % 86400) / 3600);

            var numminutes = Math.floor(((diffSec % 86400) % 3600) / 60);

            this.Time = numdays + " Jours " + numhours + " Heures " + numminutes + " Minutes ";

            let todayDate = new Date(); //Today Date  
            let dateOne = new Date(this._trialEndsAt);
            if (todayDate > dateOne) {
                this.ResultDate = true;
            } else {
                this.ResultDate = false;
            }
            if (res.type != "Emprunt obligataire") {
                this.dataSource.anticipatedTerm = Constants.ListAnticipatedTermination.find(a => a.value == res.anticipatedTerm).label;
                this.dataSource.taxationInter = Constants.ListTaxationInter.find(a => a.value == res.taxationInter).label;
                this.dataSource.paymentInterest = Constants.ListPaymentInterest.find(a => a.value == res.paymentInterest).label;
            }

            this.getInvestments();
            this.getMyListInvestment();
            this.initInviteForm();
            //Get all contact
            this.getAllContact();
        })
    }
    //Init Form
    initForm() {
        this.amountForm = this._formBuilder.group({
            emissionId: new FormControl(),
            amount: ['', [Validators.required]],
            rate: new FormControl([], Validators.required),
        });
    }

    getInvestments() {
        return new Promise((resolve, reject) => {

            //Check if type emisison is EO
            var listIdEmission = [];
            if (this.dataSource.type == "Emprunt obligataire") {
                if (this.dataSource.children['categoryA'] != undefined)
                    listIdEmission = _.concat(listIdEmission, this.dataSource.children.categoryA.map(a => a.idEmission));
                if (this.dataSource.children['categoryB'] != undefined)
                    listIdEmission = _.concat(listIdEmission, this.dataSource.children.categoryB.map(a => a.idEmission));
                if (this.dataSource.children['categoryC'] != undefined)
                    listIdEmission = _.concat(listIdEmission, this.dataSource.children.categoryC.map(a => a.idEmission));
                if (this.dataSource.children['categoryD'] != undefined)
                    listIdEmission = _.concat(listIdEmission, this.dataSource.children.categoryD.map(a => a.idEmission));
                if (this.dataSource.children['categoryE'] != undefined)
                    listIdEmission = _.concat(listIdEmission, this.dataSource.children.categoryE.map(a => a.idEmission));
            }




            this._serviceHelper.__post("investments/myInvestments", { emissionId: this.emissionId, listEmission: listIdEmission }).subscribe((res: any) => {
                this.dataInv = res;
                this.gaugeValue = res.somme;
                this.NbInves = res.nbinvestment;
                this.ValueProgress = (this.gaugeValue / this.empruntMontant) * 100
                // this.ValueProgress = (12000/12000)*100 //For Test
                if (this.ValueProgress < 1) { this.ValueProgress = 0; }

                if (res.message == "success") {
                    this.isEmissionOwner = true;
                    this.listInvestment = res.data.docs;
                    this.listInvestment.map(a => a.status = Constants.typeStatusEmission.find(t => t.value == a.status).label);
                    // this.listInvestment.map(a => a.rate = Constants.typeRateEmission.find(t => t.value == a.rate).label);
                    this.gaugeValue = res.somme;


                } else {
                    this.isEmissionOwner = false;
                    resolve();
                }
            });
        });
    }

    onInvestir(f) {

        Swal.fire({
            title: 'Voulez-vous confirmer votre investissement ?',
            type: 'success',
            showCancelButton: true,
            confirmButtonText: 'Oui, accepter',
            cancelButtonText: 'Annuler'
        }).then((result) => {
            if (result.value) {
                //Check if form valid
                if (f.valid) {
                    var dataToSend = JSON.parse(JSON.stringify(f.value));
                    if (this.dataSource.type == "Emprunt obligataire") {
                        dataToSend = dataToSend.investissement.map(a => a.values.filter(f => f.amount != null));
                    }
                    this._serviceHelper.__post("investments/createInvestment", { investment: dataToSend, type: this.dataSource.type }).subscribe((res: any) => {
                        Swal.fire({
                            position: 'top-end',
                            type: 'success',
                            title: 'Investissement enregistré avec succès',
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                            this.getInvestments().then(() => {
                                this.getMyListInvestment();
                            });
                            this.isInvested = true;
                        })
                    },
                        error => {

                        });
                }
                this.getInvestments();
            }
        });

    }

    accept(id) {
        Swal.fire({
            title: 'Voulez vous accepter cet investissement',
            type: 'success',
            showCancelButton: true,
            confirmButtonText: 'Oui, accepter',
            cancelButtonText: 'Refuser'
        }).then((result) => {
            if (result.value) {
                //Check if form valid
                this._serviceHelper.__post("investments/updateInvestment", { investissementId: id, status: 3 }).subscribe(async (res: any) => {
                    Swal.fire({
                        position: 'top-end',
                        type: 'success',
                        title: 'Investissement accepté',
                        showConfirmButton: false,
                        timer: 1500
                    }).then(async () => {
                        this.getInvestments().then(() => {
                            this.getMyListInvestment();
                        });
                        await this._serviceHelper.__post("mailer/sendEmail", { emissionId: this.emissionId, investissementId: id, message: "Votre demande d'investissement a été accepté ", state: true }).subscribe((res: any) => {
                            console.log("mail done");
                        });
                    })
                });
            }
        });
    }
    refuse(id) {

        Swal.fire({
            title: 'Voulez vous refuser cet investissement ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Oui, Confirmer',
            cancelButtonText: 'Annuler'
        }).then((result) => {
            if (result.value) {
                //Check if form valid
                this._serviceHelper.__post("investments/updateInvestment", { investissementId: id, status: 2 }).subscribe(async (res: any) => {
                    Swal.fire({
                        position: 'top-end',
                        type: 'success',
                        title: 'Investissement supprimé',
                        showConfirmButton: false,
                        timer: 1500
                    }).then(async () => {
                        this.getInvestments().then(() => {
                            this.getMyListInvestment();
                        });
                        await this._serviceHelper.__post("mailer/sendEmail", { emissionId: this.emissionId, investissementId: id, message: "Votre demande d'investissement a été refusé", state: false }).subscribe((res: any) => {
                            console.log("mail done");
                        });
                    })
                });
            }
        });

    }
    //Invite investor
    initInviteForm() {
        this.invitForm = this._formBuilder.group({
            myContacts: new FormControl(''),
            myExtrenalContacts: new FormControl(this.externalContacts)
        });
    }

    invitContacts() {
        this._serviceHelper.__post("emissions/invitContacts", { contacts: this.invitForm.value, emission: this.dataSource._id }).subscribe((res: any) => {
            console.log("mail done");
            this._toastr.success("Les invitations on été envoyées avec succès)")
        });
    }
    //Get All contact
    getAllContact() {
        this._serviceHelper.__post(Constants.GET_ALL_CONTACT_EM, { searchQuerry: this.formFilter.value }).subscribe((res: any) => {
            this.myContacts = res.data;
            this.myContacts.forEach((el) => {
                el.badge = el.lastname.substring(0, 1) + el.firstname.substring(0, 1);
            });
        },
            error => {
                console.log(error);
            })
    }
    //on selection contact user
    onSelection(e, t) {
    }
    addContact(event: MatChipInputEvent): void {
        const input = event.input;
        const value = event.value;

        // Add our fruit
        if ((value || '').trim()) {
            this.externalContacts.push({ email: value.trim() });
        }

        // Reset the input value
        if (input) {
            input.value = '';
        }
    }

    removeContact(contact: any): void {
        const index = this.externalContacts.indexOf(contact);

        if (index >= 0) {
            this.externalContacts.splice(index, 1);
        }
    }
    //Donwload file pdf
    downloadFile(d) {
        let currentDate = new Date();
        let dateFile = (currentDate.getDate() + "-" + (currentDate.getMonth() < 9 ? ("0" + (currentDate.getMonth() + 1)) : (currentDate.getMonth() + 1)) + "-" + currentDate.getFullYear()).toString()
        saveAs(environment.api + d.uploadPath, d.name + '-' + dateFile + '.pdf');
    }
    //Get my list investments
    getMyListInvestment(_pageIndex = this.pageIndex, _limitPage = this.limitPage) {

        //Check if type emisison is EO
        var listIdEmission = [];
        if (this.dataSource.type == "Emprunt obligataire") {
            if (this.dataSource.children['categoryA'] != undefined)
                listIdEmission = _.concat(listIdEmission, this.dataSource.children.categoryA.map(a => a.idEmission));
            if (this.dataSource.children['categoryB'] != undefined)
                listIdEmission = _.concat(listIdEmission, this.dataSource.children.categoryB.map(a => a.idEmission));
            if (this.dataSource.children['categoryC'] != undefined)
                listIdEmission = _.concat(listIdEmission, this.dataSource.children.categoryC.map(a => a.idEmission));
            if (this.dataSource.children['categoryD'] != undefined)
                listIdEmission = _.concat(listIdEmission, this.dataSource.children.categoryD.map(a => a.idEmission));
            if (this.dataSource.children['categoryE'] != undefined)
                listIdEmission = _.concat(listIdEmission, this.dataSource.children.categoryE.map(a => a.idEmission));
        }

        this._serviceHelper.__post(Constants.GET_MY_INVESTMENTS_IV, { pageIndex: _pageIndex, limitPage: _limitPage, emissionId: this.emissionId, listEmission: listIdEmission }).subscribe((res) => {
            this.listmyinvestment = res.data.docs;
            this.listmyinvestment.forEach((el, index) => {
                el.status = Constants.typeStatusEmission.find(s => s.value == el.status).label;
            });
            this.total = res.data.total;
        },
            error => { });
    }
    /***********************
    *  Method Paginator
    **********************/
    getServerData(e) {
        this.getMyListInvestment((e.pageIndex + 1), e.pageSize)
    }
    backTo() {
        this._router.navigate(['/marketPlace/marketCards']);
    }
    //Init Form Inv EO
    initFormIntEO(cat?: String) {
        this.formInvEO = this._formBuilder.group({
            investissement: this._formBuilder.array([])
        });
    }
    addCategory(cat, data) {
        let control = <FormArray>this.formInvEO.controls['investissement'];
        control.push(this.initFieldFormEo(cat, data[0]));
        let controlsValues = control.controls.find(a => a.value.categorie == cat)['controls']['values'];
        controlsValues.controls = [];
        controlsValues.value = [];
        data.forEach(el => {
            if (el.RembourseTauxFixeAmount) {
                controlsValues.push(this.initFieldValue(el.RembourseTauxFixeAmount, 'fix', el.idEmission));
            }
            else {
                controlsValues.push(this.initFieldValue(el.RembourseTauxVariableAmount, 'variable', el.idEmission));
            }
        });
    }
    //Init field form EO
    initFieldFormEo(cat?: String, data?: any) {
        return this._formBuilder.group({
            values: this._formBuilder.array([
                this._formBuilder.group({})
            ]),
            categorie: [cat],
            maturite: [data.RembourseMaturity]
        })
    }
    initFieldValue(rate, type, id) {
        this.max = this.dataSource.EmpruntMontantModifiableBool ? this.dataSource.EmpruntMontantModifiable : this.dataSource.EmpruntMontant;
        return this._formBuilder.group({
            rate: [rate],
            amount: [, [Validators.required, Validators.min(Constants.minimumInvestment_EO), Validators.max(this.max)]],
            type: [type],
            idEmission: [id]
        })
    }
    changeamountValue(e, item) {
        if (item.value.amount != null) {
            this.removeValidators(this.formInvEO)
        }
        else {
            this.addValidators(this.formInvEO);
        }
    }
    removeValidators(form: FormGroup) {
        form.controls.investissement['controls'].forEach(element => {
            element.controls.values.controls.forEach(el => {
                el.get("amount").setValidators([Validators.min(Constants.minimumInvestment_EO), Validators.max(this.max)]);
                el.get("amount").updateValueAndValidity();
            });
        });

    }
    addValidators(form: FormGroup) {
        var isEmpty = true;
        form.controls.investissement['controls'].forEach(element => {
            var amount = element.controls.values.controls.find(a => a.value.amount != null);
            if (amount != undefined && isEmpty) {
                isEmpty = false;
            }
        });

        if (isEmpty) {
            form.controls.investissement['controls'].forEach(element => {
                element.controls.values.controls.forEach(el => {
                    el.get("amount").setValidators([Validators.required, Validators.min(Constants.minimumInvestment_EO), Validators.max(this.max)]);
                    el.get("amount").updateValueAndValidity();
                });
            });
        }


    }
    increaseValue(i): void {
        var currentValue = i.value.amount;
        currentValue = currentValue + this.setpsInput(currentValue, 1);
        if (currentValue <= this.max) {
            i.patchValue({
                amount: currentValue
            });
        }
        else {
            i.patchValue({
                amount: this.max
            });
        }
    }
    //Setps
    setpsInput(v, s) {
        var steps = 0;
        if (s == 1) {
            if (v >= 0 && v < 100)
                steps = 10;
            else if (v >= 100 && v < 1000)
                steps = 100;
            else if (v >= 1000 && v < 10000)
                steps = 1000;
            else {
                steps = 1500;
            }
            return steps;
        }
        else {
            var arr = v.toString(10).replace(/\D/g, '0').split('').map(Number);
            if (arr.length <= 2)
                steps = 10;
            else if (arr.length <= 3)
                steps = 100
            else if (arr.length <= 4) {
                steps = 1000;
                steps = (v - steps) < 1000 ? 100 : 1000;
            }
            else {
                steps = 1500;
            }
            return steps;
        }
    }
    decreaseValue(i): void {
        var currentValue = i.value.amount;
        currentValue = currentValue - this.setpsInput(currentValue, -1);
        if (currentValue >= Constants.minimumInvestment_EO) {
            i.patchValue({
                amount: currentValue
            });
        }
        else {
            i.patchValue({
                amount: Constants.minimumInvestment_EO
            });
        }
    }
    //Event change search query
    changeSearchQuery(e, field) {
        switch (field) {
            case 'name':
                this.formFilter.patchValue({
                    name: e.srcElement.value
                });
                break;
            case 'category':
                this.formFilter.patchValue({
                    category: e.value
                });
                break;
            case 'company':
                this.formFilter.patchValue({
                    company: e.srcElement.value
                });
                break;
            case 'sector':
                this.formFilter.patchValue({
                    sector: e.value
                });
                break;
            case 'fonction':
                this.formFilter.patchValue({
                    fonction: e.srcElement.value
                });
                break;
        }
        this.getAllContact();
    }
    //Init form filter
    initFormFilter() {
        this.formFilter = this._formBuilder.group({
            tags: [[]],
            name: [''],
            category: [''],
            company: [''],
            sector: [''],
            fonction: ['']
        });
    }
    //Go to investor tab
    goToInvestorTab() {
        this.tabGroup.selectedIndex = 2;

    }
    onLinkClick(e) {
        const element = document.querySelector("body");
        element.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
    }
}