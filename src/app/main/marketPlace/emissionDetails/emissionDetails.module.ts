import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmissionDetailsComponent } from './emissionDetails.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';

import { RouterModule } from '@angular/router';
import { MomentModule } from 'ngx-moment';


//Import Material Modules
import {
	MatButtonModule, MatChipsModule, MatExpansionModule, MatFormFieldModule, MatIconModule, MatInputModule, MatPaginatorModule, MatRippleModule, MatSelectModule, MatSnackBarModule,
	MatTableModule,MatCheckboxModule, MatListModule,MatProgressBarModule, MatTabsModule
  } from '@angular/material';



const routes = [
{
	path: '',
	component: EmissionDetailsComponent
}
];

@NgModule({
	declarations: [EmissionDetailsComponent],
	imports: [
	CommonModule,
	MatFormFieldModule,
	MatInputModule,
	MatButtonModule,
	MatChipsModule,
	MatListModule,
	MomentModule,
	MatIconModule,
	MatExpansionModule,
	MatTableModule,
	MatTabsModule,
	MatRippleModule,
	MatCheckboxModule,
	MatSelectModule,
	MatSnackBarModule,
	MatPaginatorModule,
	FuseSharedModule,
    FuseSidebarModule,
	FuseWidgetModule,
	MatProgressBarModule,
	RouterModule.forChild(routes),

	]
})
export class MarketCardsModule { }
 