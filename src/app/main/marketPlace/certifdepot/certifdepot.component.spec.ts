import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertifdepotComponent } from './certifdepot.component';

describe('CertifdepotComponent', () => {
  let component: CertifdepotComponent;
  let fixture: ComponentFixture<CertifdepotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertifdepotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertifdepotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
