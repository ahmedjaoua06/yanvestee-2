import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { ServiceHelperService } from '@serviceHelper/service-helper.service';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

/*import { EcommerceProductsService } from 'app/main/apps/e-commerce/products/products.service';*/
import { takeUntil } from 'rxjs/internal/operators';

@Component({
    selector     : 'e-commerce-products',
    templateUrl  : './certifdepot.component.html',
    styleUrls    : ['./certifdepot.component.scss'],
    animations   : fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class CertifdepotComponent implements OnInit
{
    dataSource: [] | null;
    displayedColumns = ['id', 'image', 'name', 'category', 'price', 'quantity', 'active'];

    @ViewChild(MatPaginator)
    paginator: MatPaginator;

    @ViewChild(MatSort)
    sort: MatSort;

    @ViewChild('filter')
    filter: ElementRef;
    filteredCourses: any[];
    triOptions: any[];
    currentTri: String;
    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
       private _serviceHelper: ServiceHelperService,
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {   this._serviceHelper.__post("emissions/emissionMarket", { type: "Certificat de dépôt", pageIndex: 1, limitPage: 30 }).subscribe((res) => {
            this.dataSource = res.docs;
            console.log(res)
        })
    this.triOptions = [{label : "Tri par taux nominal", value : "rateFixedActuarial"},{label : "Tri par periode", value : "period"}]
    }

    changeTri() {
        console.log(this.currentTri);
        this._serviceHelper.__post("emissions/emissionMarket", { type: "Certificat de dépôt", pageIndex: 1, limitPage: 30,sortType: this.currentTri,sortOrder:-1}).subscribe((res) => {
            this.dataSource = res.docs;
            console.log(res)
        })
    }
}
