import { Component, OnInit, Inject } from '@angular/core';
import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';
import { Constants } from '@helper/constants';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-contact-dialog',
  templateUrl: './contact-dialog.component.html',
  styleUrls: ['./contact-dialog.component.scss']
})
export class ContactDialogComponent implements OnInit {
  formContact: FormGroup;
  constructor(private _serviceHelper: ServiceHelperService, private _formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<ContactDialogComponent>) { }

  //Send event message
  eventSendMessage(e) {

  }
  ngOnInit() {
    //Init Form 
    this.initFormContact();
  }
  //Event save change outStanding
  eventSendMessageContact(f) {
    Swal.fire({
      title: 'Voulez-vous vraiment envoyer ce courriel ?',
      type: 'question',
      showCancelButton: true,
      confirmButtonText: 'Oui, Envoyer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        if (f.valid) {
          console.log("=======  eventSendMessageContact  =======");
          console.log(f.value);
          console.log(this.data);
          this._serviceHelper.__post(Constants.SEND_MESSAGE_CONTACT_MARKETPLACE, { message: f.value.message, email: this.data.emetteurId.email }).subscribe((res: any) => {
            Swal.fire({
              position: 'top-end',
              type: 'success',
              title: 'Votre message a été envoyé avec succès',
              showConfirmButton: false,
              timer: 1500
            }).then(() => {
              this.dialogRef.close(true);
            })
          },
            error => {
              console.log("Erreur");
              console.log("Error");
            });
        }
      }
    })
  }
  //Init Form outStanding
  initFormContact() {
    this.formContact = this._formBuilder.group({
      message: ['', [Validators.required, Validators.minLength(4)]]
    });
  }
}
