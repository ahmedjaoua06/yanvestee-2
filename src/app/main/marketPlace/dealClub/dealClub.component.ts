import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { ServiceHelperService } from '@serviceHelper/service-helper.service';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';
import { Router, ActivatedRoute } from '@angular/router';

/*import { EcommerceProductsService } from 'app/main/apps/e-commerce/products/products.service';*/
import { takeUntil } from 'rxjs/internal/operators';
//Import momentjs
import * as moment from 'moment';
import { Constants } from '@helper/constants';
import { environment } from 'environments/environment';

@Component({
    selector: 'e-commerce-products',
    templateUrl: './dealClub.component.html',
    styleUrls: ['./dealClub.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class DealClubComponent implements OnInit {
    dataSource: any[];
    displayedColumns = ['id', 'image', 'name', 'category', 'price', 'quantity', 'active'];

    @ViewChild(MatPaginator)
    paginator: MatPaginator;

    @ViewChild(MatSort)
    sort: MatSort;

    @ViewChild('filter')
    filter: ElementRef;
    triOptions: any[];
    typeArray: any[];
    dureeArray: any[];
    emissionByPeriod : any[];
    currentTri: String;
    currentType: String;
    currentDuree:any;
    pageIndex: Number;
    limitPage: Number;
    sortOrder: Number;
    sortType: String;
    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _serviceHelper: ServiceHelperService,
        private _router: Router
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
    this.pageIndex = 1;
        this.limitPage = 50;
        this.sortType = "creationDate";
        this.sortOrder = -1;
        this.getCards();
        this.triOptions = [{ label: "Investissement min", value: "miniInvest" }, { label: "Nouveauté", value: "creationDate" }, { label: "Taux", value: "rateFixedNominal" }];
        this.typeArray = [{label:"Tout",value:"Tout"},{label:"Comptes à terme",value:"Comptes à terme"},{label:"Certificats de dépôt",value:"Certificat de dépôt"},{label:"Billets de trésorerie",value:"Billets de trésorerie"},{label:"Obligations",value:"Emprunt obligataire"}];
       /* this.dureeArray = [,]*/
        this.dureeArray = [{groupName:"Court terme", valeurs:[{label:3,value:3},{label:6,value:6},{label:9,value:9}]},{groupName:"Long terme", valeurs:[{label:12,value:12},{label:24,value:24},{label:36,value:36},{label:48,value:48},{label:60,value:60}]}];
    }

    changeTri() {
        this.sortType = this.currentTri;
        this.getCards();
    }
    getCards() {
        this._serviceHelper.__post("emissions/dealClub", { pageIndex: 1, limitPage: 30, sortType: this.sortType, sortOrder: this.sortOrder, type: this.currentType, duree:this.currentDuree }).subscribe((res: any) => {
            console.log(res)
            this.dataSource = res.docs;
            for (var i = 0; i < this.dataSource.length; i++) {
                this.dataSource[i].typeSlug = this.dataSource[i].type.replace(/ /g, '-');
                //Create current Date
                var currentDate = moment();
                //Get end emission
                var endEmission = moment(this.dataSource[i].endEmission);
                //Set countdown to datasource
                var diffSec = endEmission.diff(currentDate, 'seconds');
                var numdays = Math.floor(diffSec / 86400);

                var numhours = Math.floor((diffSec % 86400) / 3600);

                var numminutes = Math.floor(((diffSec % 86400) % 3600) / 60);


                this.dataSource[i].countdown = numdays + " jrs " + numhours + " h " + numminutes + " m ";
                this.dataSource[i].type == 'Emprunt obligataire' ? this.dataSource[i].EmpruntMontant = this.dataSource[i].EmpruntMontant.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : this.dataSource[i].miniInvest = this.dataSource[i].miniInvest.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ');
                //Horizon
                if (this.dataSource[i].horizon != undefined) {
                    this.dataSource[i].horizon = Constants.ListHorizon.find(a => a.value == this.dataSource[i].horizon).label;
                }
                this.dataSource[i].emetteurId.logo = this.dataSource[i].emetteurId.logo != undefined ? environment.api + this.dataSource[i].emetteurId.logo.path : Constants.default_image_user_em;

            }
        })
        this._serviceHelper.__post("emissions/emissionByPeriod", { type: this.currentType, searchType:"deal" }).subscribe((res: any) => {
            this.emissionByPeriod=res;
        })
    }
    filterByType() {
        this.getCards();
    }

    //Redirect to details card
    detailsCard(emissionId) {
        this._router.navigate(['/marketPlace/emissionDetails'], { queryParams: { 'emissionId': emissionId } })
    }

}
