import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DealClubComponent } from './dealClub.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';

import { RouterModule } from '@angular/router';
import {
    MatButtonModule, MatChipsModule, MatExpansionModule, MatFormFieldModule, MatIconModule, MatInputModule, MatPaginatorModule, MatRippleModule, MatSelectModule, MatSnackBarModule,
    MatSortModule,
    MatTableModule, MatTabsModule
} from '@angular/material';


const routes = [
{
	path: '',
	component: DealClubComponent
}
];

@NgModule({
	declarations: [DealClubComponent],
	imports: [
	CommonModule,
	MatFormFieldModule,
	MatInputModule,
	MatButtonModule,
	MatChipsModule,
	MatIconModule,
	MatExpansionModule,
	MatTableModule,
	MatRippleModule,
	MatSelectModule,
	MatSnackBarModule,
	MatPaginatorModule,
	FuseSharedModule,
    FuseSidebarModule,
    FuseWidgetModule,
	RouterModule.forChild(routes),

	]
})
export class MarketCardsModule { }
