import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketCardsComponent } from './marketCards.component';

describe('BilletTresoComponent', () => {
  let component: MarketCardsComponent;
  let fixture: ComponentFixture<MarketCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarketCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
