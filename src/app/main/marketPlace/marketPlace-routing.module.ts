import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Import Compnents

import { CertifdepotComponent } from './certifdepot/certifdepot.component';
import { BilletTresoComponent } from './billet-treso/billet-treso.component';
import { CompteTermeComponent } from './compte-terme/compte-terme.component';
import { MarketCardsComponent } from './marketCards/marketCards.component';
import { DealClubComponent } from './dealClub/dealClub.component';
import { EmissionDetailsComponent } from './emissionDetails/emissionDetails.component';


const routes: Routes = [
  {
    path: "certif", 
    component: CertifdepotComponent
  },
  {
    path: "compte-terme", 
    component: CompteTermeComponent
  },
  {
    path: "billet-tresorie", 
    component: BilletTresoComponent
  },
  {
    path: "marketCards", 
    component: MarketCardsComponent
  },
  {
    path: "emissionDetails", 
    component: EmissionDetailsComponent
  },
  {
    path: "dealClub", 
    component: DealClubComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarketPlaceRoutingModule { }
