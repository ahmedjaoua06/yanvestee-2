import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompteTermeComponent } from './compte-terme.component';

describe('CertifdepotComponent', () => {
  let component: CompteTermeComponent;
  let fixture: ComponentFixture<CompteTermeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompteTermeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompteTermeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
