import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarketPlaceRoutingModule } from './marketPlace-routing.module';
/*import { CompteTermeListComponent } from './compte-terme/compte-terme-list/compte-terme-list.component';
import { CompteTermeAddComponent } from './compte-terme/compte-terme-add/compte-terme-add.component';*/

import { NgxGaugeModule } from 'ngx-gauge';

//Import Material Modules
import {
  MatButtonModule, MatChipsModule, MatExpansionModule, MatFormFieldModule, MatIconModule, MatInputModule, MatPaginatorModule, MatRippleModule, MatSelectModule, MatSnackBarModule,
  MatSortModule, MatTableModule, MatTabsModule, MatCheckboxModule, MatTooltipModule, MatDividerModule, MatRadioModule, MatListModule, MatPaginatorIntl
  , MatProgressBarModule,
  MatButtonToggleModule
} from '@angular/material';

import { MatDialogModule } from '@angular/material/dialog';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared/shared.module';
/*import { BilletTresoListComponent } from './billet-treso/billet-treso-list/billet-treso-list.component';*/
import { CountdownModule } from 'ngx-countdown';
import { CertifdepotComponent } from './certifdepot/certifdepot.component';
import { BilletTresoComponent } from './billet-treso/billet-treso.component';
import { CompteTermeComponent } from './compte-terme/compte-terme.component';
import { MarketCardsComponent } from './marketCards/marketCards.component';
import { DealClubComponent } from './dealClub/dealClub.component';
import { EmissionDetailsComponent } from './emissionDetails/emissionDetails.component';
import { MomentModule } from 'ngx-moment';
import { ContactDialogComponent } from './contact-dialog/contact-dialog.component';


const FrenchRangeLabel = (page: number, pageSize: number, length: number) => {
  if (length == 0 || pageSize == 0) { return `0 de ${length}`; }

  length = Math.max(length, 0);

  const startIndex = page * pageSize;

  // If the start index exceeds the list length, do not try and fix the end index to the end.
  const endIndex = startIndex < length ?
    Math.min(startIndex + pageSize, length) :
    startIndex + pageSize;

  return `${startIndex + 1} - ${endIndex} de ${length}`;
}


export function getFrenchPaginatorIntl() {
  const paginatorIntl = new MatPaginatorIntl();

  paginatorIntl.itemsPerPageLabel = 'Articles par page:';
  paginatorIntl.nextPageLabel = 'Page suivante';
  paginatorIntl.previousPageLabel = 'Page précédente';
  paginatorIntl.getRangeLabel = FrenchRangeLabel;

  return paginatorIntl;
}

@NgModule({
  declarations: [CertifdepotComponent, BilletTresoComponent, CompteTermeComponent, MarketCardsComponent, EmissionDetailsComponent, DealClubComponent, ContactDialogComponent],
  imports: [
    FuseSharedModule,
    FuseWidgetModule,
    SharedModule,
    TranslateModule,
    MarketPlaceRoutingModule,
    MatButtonModule,
    MatChipsModule,
    MatExpansionModule,
    MatFormFieldModule,
    CountdownModule,
    MomentModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatListModule,
    MatDialogModule,
    MatButtonToggleModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSelectModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTooltipModule,
    MatTabsModule,
    MatCheckboxModule,
    MatDividerModule,
    MatRadioModule,
    NgxGaugeModule,
    MatProgressBarModule
  ],
  providers: [
    { provide: MatPaginatorIntl, useValue: getFrenchPaginatorIntl() }
  ],
  entryComponents: [ContactDialogComponent]
})
export class MarketPlaceModule { }
