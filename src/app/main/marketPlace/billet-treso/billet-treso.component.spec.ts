import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BilletTresoComponent } from './billet-treso.component';

describe('BilletTresoComponent', () => {
  let component: BilletTresoComponent;
  let fixture: ComponentFixture<BilletTresoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BilletTresoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BilletTresoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
