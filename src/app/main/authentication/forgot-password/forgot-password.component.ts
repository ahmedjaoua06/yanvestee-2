import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import { Router } from '@angular/router';

import Swal from 'sweetalert2';

@Component({
    selector: 'forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ForgotPasswordComponent implements OnInit {
    forgotPasswordForm: FormGroup;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _serviceHelper: ServiceHelperService,
        private _router: Router
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.forgotPasswordForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });
    }
    //Send Email
    eventForgotPassword(f) {
        if (f.valid) {
            this._serviceHelper.__post("users/sendTokenForgotPassword", f.value).subscribe((res: any) => {
                //Check if message exist
                if (res.message) {
                    //Show sucess alert
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        title: res.message,
                        showConfirmButton: false,
                        timer: 2000
                    }).then((result) => {
                        //Redirect to login page
                        this._router.navigate(['/login'])
                    });
                }
            },
                (error: any) => {
                    if (error.error.error) {
                        Swal.fire({
                            title: 'Erreur',
                            text: error.error.error,
                            type: 'error'
                        });
                    }
                });
        }
    }
}
