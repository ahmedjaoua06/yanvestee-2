import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { Router, ActivatedRoute } from '@angular/router';

import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
    resetPasswordForm: FormGroup;
    params: any;
    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _router: Router,
        private _activeRoute: ActivatedRoute,
        private _serviceHelper: ServiceHelperService,
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        //Get Params
        this._activeRoute.queryParamMap.subscribe((res: any) => {
            if (res.params) {
                this.params = res.params;
            }
        });

        this.resetPasswordForm = this._formBuilder.group({
            password: ['', [Validators.required, Validators.minLength(5), Validators.pattern('[a-z]+[A-Z]+[0-9]+|[A-Z]+[a-z]+[0-9]+|[0-9]+[A-Z]+[a-z]+|[0-9]+[a-z]+[A-Z]+|[A-Z]+[0-9]+[a-z]+|[a-z]+[0-9]+[A-Z]+')]],
            passwordConfirm: ['', [Validators.required, confirmPasswordValidator]]
        });

        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        this.resetPasswordForm.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.resetPasswordForm.get('passwordConfirm').updateValueAndValidity();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    //Reset Password
    eventRestPassword(f) {
        //Set Token & Permalink
        f.value.token = this.params.token;
        f.value.permalink = this.params.permalink;
        if (f.valid) {
            //Delete PasswordConfirm
            delete f.value.passwordConfirm;
            this._serviceHelper.__post("users/forgotPassword", f.value).subscribe((res) => {
                //Check if message exist
                if (res.message) {
                    //Show sucess alert
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        title: res.message,
                        showConfirmButton: false,
                        timer: 2000
                    }).then((result) => {
                        //Redirect to dashboard page
                        this._router.navigate(['/login'])
                    });
                }
            },
                error => {
                    if (error.error.error) {
                        Swal.fire({
                            title: 'Erreur',
                            text: error.error.error,
                            type: 'error'
                        });
                    }
                });
        }
    }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return { 'passwordsNotMatching': true };
};
