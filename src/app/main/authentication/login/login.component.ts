import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { TranslateService } from '@ngx-translate/core';
import { locale as navigationEnglish } from 'app/navigation/i18n/en';
import { locale as navigationTurkish } from 'app/navigation/i18n/tr';
import { locale as navigationFrench } from 'app/navigation/i18n/fr';

import { Router, ActivatedRoute } from '@angular/router';
import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import { AuthenticationServiceService } from '@serviceHelper/authentication.service.service';
import Swal from 'sweetalert2';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { Constants } from '@helper/constants';
import { environment } from 'environments/environment';
@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private activeRoute: ActivatedRoute,
        private _serviceHelper: ServiceHelperService,
        private route: Router,
        private authService: AuthenticationServiceService,
        private _fuseNavigationService: FuseNavigationService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _translateService: TranslateService,

    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this.activeRoute.queryParams.subscribe((params) => {
            //Check if param exist
            if (params.verif != undefined) {
                //Verif Account User
                this._serviceHelper.__post("users/confirm", { userId: params.verif }).subscribe((res) => {
                    //Show Success Alert
                    Swal.fire({
                        title: 'Confirmation',
                        text: 'Votre compte a été confirmé ',
                        type: 'success'
                    }).then(() => {
                        //Redirect to login
                        this.route.navigate([]);
                    });

                },
                    (error: any) => {
                        if (error.error.error) {
                            Swal.fire({
                                title: 'Erreur',
                                text: error.error.error,
                                type: 'error'
                            });
                        }

                    })
            }
        });

        this.loginForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
        // reset login status
        this.authService.logout();
    }
    //Login
    login(f) {
        //Check if form valid
        if (f.valid) {
            //Send Data
            this.authService.login(f.value)
                .pipe(first())
                .subscribe(
                    res => {
                        //Check if message exist
                        if (res.message) {
                            //Show sucess alert
                            /* Swal.fire({
                                 position: 'center',
                                 type: 'success',
                                 title: res.message,
                                 showConfirmButton: false,
                                 timer: 2000
                             }*/
                            /* ).then((result) => {*/
                            // Get default navigation
                            const currentUser = this.authService.currentUserValue;
                            if (currentUser && currentUser.token) {

                                this._serviceHelper.__get("users/getMenu").subscribe(resMenu => {
                                    // Get default navigation
                                    let _navigation = resMenu;
                                    // Register the navigation to the service
                                    this._fuseNavigationService.register('main', _navigation);

                                    // Set the main navigation as our current navigation
                                    this._fuseNavigationService.setCurrentNavigation('main');


                                    // Add languages
                                    this._translateService.addLangs(['en', 'tr', 'fr']);

                                    // Set the default language
                                    this._translateService.setDefaultLang('fr');

                                    // Set the navigation translations
                                    this._fuseTranslationLoaderService.loadTranslations(navigationFrench, navigationEnglish, navigationTurkish);

                                    // Use a language
                                    this._translateService.use('fr');

                                    //Redirect to dashboard page
                                    this._serviceHelper.__get("users/profile").subscribe((user: any) => {

                                        var defaultImage = res.role == Constants.emetteur ? Constants.default_image_user_em : Constants.default_image_user_iv;
                                        //Check if user has logo
                                        if (user.logo != undefined) {
                                            user.logo.path = user.logo.path != undefined ? environment.api + user.logo.path : defaultImage;
                                        }
                                        else {
                                            user.logo = res.role == Constants.emetteur ? { path: defaultImage } : { path: defaultImage };
                                        }
                                        this._serviceHelper.userProfil = user;

                                        localStorage.setItem("role", user.role);
                                        localStorage.setItem("categorie", user.categorie);
                                        this.authService.getListNotification().then(() => {
                                            this.route.navigate(['/marketPlace/marketCards']);
                                        });
                                    })
                                }, err => {
                                    console.log(err);
                                });
                            }

                            /*          });*/
                        }
                    },
                    error => {
                        if (error.error.error) {
                            Swal.fire({
                                title: 'Erreur',
                                text: error.error.error,
                                type: 'error'
                            });
                        }
                    });
        }
    }
}
