import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { Constants } from '@helper/constants';

import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import Swal from 'sweetalert2'
import { ToastrService } from 'ngx-toastr';
@Component({
    selector: 'register-2',
    templateUrl: './register-2.component.html',
    styleUrls: ['./register-2.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class Register2Component implements OnInit, OnDestroy {
    //Section Role is Active first time
    SRisActive = true;
    currentRole = "";
    //Init Data List role, List categories from Constant helpers
    listRole = Constants.roleUser;
    listCategoriesByinvestor = Constants.categoriesUser.filter(a => a.parent == 1);
    listCategoriesBytransmitter = Constants.categoriesUser.filter(a => a.parent == 2);
    //Init Form Group
    registerForm: FormGroup;

    roleUser: FormGroup;
    informationUser: FormGroup;
    passwordUser: FormGroup;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _serviceHelper: ServiceHelperService,
        private route: Router,
        private _toastr: ToastrService
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        //Init Form Role User
        this.roleUser = this._formBuilder.group({
            role: ['', Validators.required],
            categorie: ['', Validators.required]
        })
        //Init Form Information User
        this.informationUser = this._formBuilder.group({
            societe: ['', Validators.required],
            fonction: ['', Validators.required],
            firstname: ['', Validators.required],
            lastname: ['', Validators.required],
            email: ['', Validators.required],
            tel: ['', [Validators.required, Validators.minLength(8)]],
            mobile: ['', [Validators.required, Validators.minLength(8)]]
        });
        //Init Form Password User
        this.passwordUser = this._formBuilder.group({
            password: ['', [Validators.required, Validators.minLength(5), Validators.pattern('[a-z]+[A-Z]+[0-9]+|[A-Z]+[a-z]+[0-9]+|[0-9]+[A-Z]+[a-z]+|[0-9]+[a-z]+[A-Z]+|[A-Z]+[0-9]+[a-z]+|[a-z]+[0-9]+[A-Z]+')]],
            passwordConfirm: ['', [Validators.required, confirmPasswordValidator]]
        });
        this.registerForm = this._formBuilder.group({
            role: ['', Validators.required],
            categorie: ['', Validators.required],
            societe: ['', Validators.required],
            fonction: ['', Validators.required],
            firstname: ['', Validators.required],
            lastname: ['', Validators.required],
            email: ['', Validators.required],
            tel: ['', [Validators.required, Validators.minLength(8)]],
            mobile: ['', [Validators.required, Validators.minLength(8)]],
            password: ['', Validators.required]
        });

        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        this.passwordUser.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.passwordUser.get('passwordConfirm').updateValueAndValidity();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    //Register Event
    register() {
        //Check if forms 
        if (this.roleUser.valid && this.informationUser.valid && this.passwordUser.valid) {
            this.registerForm.patchValue({
                role: this.roleUser.value.role,
                categorie: this.roleUser.value.categorie,
                societe: this.informationUser.value.societe,
                fonction: this.informationUser.value.fonction,
                firstname: this.informationUser.value.firstname,
                lastname: this.informationUser.value.lastname,
                email: this.informationUser.value.email,
                tel: this.informationUser.value.tel,
                mobile: this.informationUser.value.mobile,
                password: this.passwordUser.value.password
            });
            //Send data
            this._serviceHelper.__post("users", { user: this.registerForm.value }).subscribe((res: any) => {

                this._toastr.success("Merci pour votre inscription. Un email vous a été envoyé afin de valider votre compte");
                this.route.navigate(['/login']);

            },
                error => {
                    if (error.error.error) {
                        //Show Error Alert
                        Swal.fire({
                            title: 'Erreur',
                            text: error.error.error,
                            type: 'error'
                        });
                    }
                    else {
                        console.log("Erreur");
                    }
                });
        }
    }
    //Change Role
    changeRole(e, value, label) {
        //Set Value Role
        this.roleUser.patchValue({
            role: value
        });
        //Remove Category
        this.roleUser.patchValue({
            categorie: ""
        });
        this.currentRole = label;
    }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return { 'passwordsNotMatching': true };
};
