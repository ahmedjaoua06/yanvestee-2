import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatIconModule, MatInputModule, MatStepperModule, MatRadioModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { Register2Component } from './register-2.component';
import { ToastrModule } from 'ngx-toastr';
const routes = [
    {
        path     : '',
        component: Register2Component
    }
];

@NgModule({
    declarations: [
        Register2Component
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        ToastrModule.forRoot(),
        MatIconModule,
        MatInputModule,
        MatStepperModule,
        FuseSharedModule,
        MatRadioModule
    ]
})
export class Register2Module
{
}
