import { Component, OnInit } from '@angular/core';
//Import Service Helper
import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import { Constants } from '@helper/constants';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.scss']
})
export class NotificationListComponent implements OnInit {
  listNotif = [];
  path = environment.api;
  constructor(private serviceHelper: ServiceHelperService, private router: Router) { }

  ngOnInit() {
    //Init list
    this.getListNotification();
  }
  //Get List notifications

  getListNotification() {
    this.serviceHelper.__post(Constants.GET_ALL_NOTIFICATION, {}).subscribe((res) => {
      this.listNotif = res.data;
      var defaultImage = Constants.default_image_user_em;
      //Check if logo user exist
      this.listNotif.map(a => a.notifId.userSender.logo = a.notifId.userSender.logo != undefined ? this.path + a.notifId.userSender.logo.path : defaultImage);
      console.log(this.listNotif);
    },
      error => {
      });
  }
  //Event click item notif
  eventItemNotif(item) {
    var keyIc = item.notifId.key.split('_')[1];

    if (!item.seen) {
      item.seen = true;
      this.serviceHelper.__post(Constants.CHANGE_STATUS_NOTIF, { idNotif: item._id }).subscribe((res) => {
        if (keyIc == "IC") {
          this.router.navigateByUrl('/instit-club/details?emissionId=' + item.notifId.idEmission)
        }
        else {
          this.router.navigateByUrl('/marketPlace/emissionDetails?emissionId=' + item.notifId.idEmission)
        }
      },
        error => {

        });
    }
    else {
      if (keyIc == "IC") {
        this.router.navigateByUrl('/instit-club/details?emissionId=' + item.notifId.idEmission)
      }
      else {
        this.router.navigateByUrl('/marketPlace/emissionDetails?emissionId=' + item.notifId.idEmission)
      }
    }

  }

}
