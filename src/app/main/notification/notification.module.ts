import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationListComponent } from './notification-list/notification-list.component';
import { MatListModule, MatButtonModule, MatChipsModule, MatExpansionModule, MatFormFieldModule, MatIconModule, MatInputModule, MatPaginatorModule, MatRippleModule, MatSelectModule, MatSnackBarModule, MatSortModule, MatTableModule, MatTabsModule, MatCheckboxModule, MatTooltipModule } from '@angular/material';
//Import Routing
import { NotificationRoutingModule } from './notification-routing.module';
import { MomentModule } from 'ngx-moment';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components';

@NgModule({
  declarations: [NotificationListComponent],
  imports: [
    FuseSharedModule,
    FuseWidgetModule,
    CommonModule,
    MatListModule,
    NotificationRoutingModule,
    MatButtonModule,
    MatChipsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSelectModule,
    MatSnackBarModule,
    MomentModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatCheckboxModule,
    MatTooltipModule
  ]
})
export class NotificationModule { }
