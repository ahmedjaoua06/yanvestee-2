import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitClubOutstandingComponent } from './instit-club-outstanding.component';

describe('InstitClubOutstandingComponent', () => {
  let component: InstitClubOutstandingComponent;
  let fixture: ComponentFixture<InstitClubOutstandingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstitClubOutstandingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitClubOutstandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
