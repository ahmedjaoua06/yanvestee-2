import { Component, OnInit, Inject } from '@angular/core';
import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';
import { Constants } from '@helper/constants';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-instit-club-outstanding',
  templateUrl: './instit-club-outstanding.component.html',
  styleUrls: ['./instit-club-outstanding.component.scss']
})
export class InstitClubOutstandingComponent implements OnInit {
  //Form outStanding
  formOutStanding: FormGroup;

  constructor(private _serviceHelper: ServiceHelperService, private _formBuilder: FormBuilder,
     @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<InstitClubOutstandingComponent>) { }

  ngOnInit() {
    //Init Form 
    this.initFormOutStanding();
    this.setDataForm();
  }
  setDataForm() {

    //Check if exist values encours
    if (this.data.outstandingEO != undefined && this.data.outstandingBT && this.data.outstandingCT && this.data.outstandingCD) {
      this.formOutStanding.patchValue({
        outstandingEO: this.data.outstandingEO,
        outstandingBT: this.data.outstandingBT,
        outstandingCT: this.data.outstandingCT,
        outstandingCD: this.data.outstandingCD
      });
    }


  }
  //Event save change outStanding
  eventOutStandingProfil(f) {
    Swal.fire({
      title: 'Mise à jour des encours',
      type: 'success',
      showCancelButton: true,
      confirmButtonText: 'Oui, Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        if (f.valid) {
          this._serviceHelper.__post(Constants.UPDATE_OUT_STANDING, { outstandingAmounts: f.value }).subscribe((res: any) => {
            Swal.fire({
              position: 'top-end',
              type: 'success',
              title: "La mise à jour de vos encours a été effectué avec succès, l'option acceptation partielle est désormais active",
              showConfirmButton: false,
              timer: 1500
            }).then(() => {
              this._serviceHelper.userProfil.outstandingAmounts = res.data.outstandingAmounts;
              this.dialogRef.close(true);
            })
          },
            error => {
              console.log("Erreur");
              console.log("Error");
            });
        }
      }
    })
  }
  //Init Form outStanding
  initFormOutStanding() {
    this.formOutStanding = this._formBuilder.group({
      outstandingEO: ['', [Validators.required, Validators.min(1), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      outstandingBT: ['', [Validators.required, Validators.min(1), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      outstandingCT: ['', [Validators.required, Validators.min(1), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      outstandingCD: ['', [Validators.required, Validators.min(1), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]]
    });
  }
}
