import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Import Compnents
import { InstitClubListComponent } from './instit-club-list/instit-club-list.component';
import { InstitClubAddComponent } from './instit-club-add/instit-club-add.component';
import { InstitClubMarketComponent } from './instit-club-market/instit-club-market.component';
import { InstitClubDetailsComponent } from './instit-club-details/instit-club-details.component';
import { InstitClubMyOffersComponent } from './instit-club-my-offers/instit-club-my-offers.component';
//Import Guard
import { EmAllGuard } from '@guradEmetteur/em-all.guard';
import { IvAssetmanagerGuard } from '@guradInvestor/iv-assetmanager.guard';
import { IvAssuranceGuard } from '@guradInvestor/iv-assurance.guard';
import { InstitMarketGuard } from '@guard/instit/instit-market.guard';

const routes: Routes = [
  {
    path: "",
    component: InstitClubMarketComponent,
    canActivate: [EmAllGuard]
  },
  {
    path: "list",
    component: InstitClubListComponent,
    canActivate: [InstitMarketGuard]
  },
  {
    path: "details",
    component: InstitClubDetailsComponent
  },
  {
    path: "offers",
    component: InstitClubMyOffersComponent,
    canActivate: [EmAllGuard]
  },
  {
    path: "add",
    component: InstitClubAddComponent,
    canActivate: [InstitMarketGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InstitClubRoutingModule { }
