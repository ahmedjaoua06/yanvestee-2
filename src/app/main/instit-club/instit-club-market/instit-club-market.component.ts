import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { Subject } from 'rxjs';
import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import { AuthenticationServiceService } from '@serviceHelper/authentication.service.service';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';
import { Router } from '@angular/router';
import { Constants } from '@helper/constants';
import { MatSnackBar } from '@angular/material';
import { environment } from 'environments/environment';
//import dialog encours
import { InstitClubOutstandingComponent } from '../instit-club-outstanding/instit-club-outstanding.component';
//Import momentjs
import * as moment from 'moment';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-instit-club-market',
  templateUrl: './instit-club-market.component.html',
  styleUrls: ['./instit-club-market.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class InstitClubMarketComponent implements OnInit {
  dataSource: any[];
  isActive = false;
  triOptions: any[];
  listStatus = [];
  typeArray: any[];
  listTri = [];
  currentStatus = 1;
  pageIndex: Number;
  total: Number;
  limitPage: Number;
  sortType: any;
  info: any;
  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _serviceHelper: ServiceHelperService,
    private _router: Router,
    private snackBar: MatSnackBar,
    private authService: AuthenticationServiceService,
    public dialog: MatDialog
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.listStatus = Constants.typeEmissionStatusIC;
    this.listTri = Constants.ListTriIc;
    this.pageIndex = 1;
    this.limitPage = 10;
    this.sortType = {
      creationDate: -1,
    }
    this.getList().then(() => {
      this.datesCompare(this._serviceHelper.userProfil.outstandingAmounts);
    });
  }

  //Get List offre
  getList(_pageIndex = this.pageIndex, _limitPage = this.limitPage) {

    return new Promise((resolve, reject) => {
      this._serviceHelper.__post(Constants.GET_LIST_IC_OFFRE_EM, { pageIndex: _pageIndex, limitPage: _limitPage, sortType: this.sortType, status: this.currentStatus }).subscribe((res: any) => {
        this.info = res.data.info;
        this.dataSource = res.data.docs;
        //Current date
        var currentDate = moment();
        this.dataSource.forEach((el) => {
          var endEmission = moment(el.endEmission);
          //Set countdown to datasource
          var diffSec = endEmission.diff(currentDate, 'seconds');
          el.availableDate = diffSec;
        });
        //Check if logo exist
        this.dataSource.map(a => {
          //get role investor
          a.investorId.logo = a.investorId.logo != undefined ? environment.api + a.investorId.logo.path : Constants.default_image_user_iv;
        })
        this.dataSource.forEach((elt) => {
          elt.type.map((a, i) => {
            elt.type[i] = Constants.ListTypeCard.find(t => t.value == a).label
          });
        });
        this.total = res.data.total;
        resolve();
      },
        error => {
          this.snackBar.open(error.error.message, '', {
            duration: 2 * 1000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
            panelClass: ['alert-snackbar']
          });
          reject();
        });
    });
  }

  //Redirect my list
  redirectToMylist() {
    this._router.navigate(['/instit-club/offers']);
  }
  //Event Change tri
  changeTri(e) {

    switch (e.value) {
      //Sort by amount
      case 1:
        this.sortType = { amount: -1 }
        break;
      //Sort by period
      case 2:
        this.sortType = { period: -1 }
        break;
      //Sort by availableDate
      case 3:
        this.sortType = { availableDate: -1 }
        break;
      //Sort by creation date
      default:
        this.sortType = { creationDate: -1 }
        break;
    }


    this.getList();
  }
  //Redirect to details
  detailsOffre(o) {
    this._router.navigate(['/instit-club/details'], { queryParams: { 'emissionId': o._id } });
    console.log(o);
  }
  //Event Filter type
  filterByType() {
    this.getList();
  }
  //Event Filter Status
  filterByStatus(e) {
    console.log(e);
    if (e.index != 4) {
      e.index++
    }
    else {
      e.index = undefined;
    }
    this.currentStatus = e.index;
    this.getList();
  }
  //Cancel Offer
  cancelOffer(e) {
    Swal.fire({
      title: 'Voulez-vous annuler votre offre ?',
      type: 'success',
      showCancelButton: true,
      confirmButtonText: 'Oui, Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this._serviceHelper.__post(Constants.CANCEL_INVESTMENT_IC, { idEmission: e._id}).subscribe((res: any) => {
          Swal.fire({
            position: 'top-end',
            type: 'success',
            title: 'votre demande d"annulation a éte envoyée avec succès',
            showConfirmButton: false,
            timer: 1500
          }).then(() => {
            this.getList();
          })
        },
          error => {});
      }
    });
  }
  /***********************
*  Method Paginator
**********************/
  getServerData(e) {
    this.getList((e.pageIndex + 1), e.pageSize)
  }

  //Compare dates
  datesCompare(d) {
    //Set time to 0
    d.updateDate = new Date(d.updateDate).setHours(0, 0, 0, 0);
    var currentDate = new Date().setHours(0, 0, 0, 0);
    if (d.updateDate < currentDate || d.outstandingEO == undefined) {
      this.showListBlockedUser(d);
    }

  }
  //Show list blocked user
  showListBlockedUser(data) {
    this.dialog.open(InstitClubOutstandingComponent, {
      data: data,
      height: '450px',
      width: '400px',
    });
  }
}
