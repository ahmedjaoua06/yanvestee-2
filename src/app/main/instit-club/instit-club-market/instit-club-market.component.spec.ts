import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitClubMarketComponent } from './instit-club-market.component';

describe('InstitClubMarketComponent', () => {
  let component: InstitClubMarketComponent;
  let fixture: ComponentFixture<InstitClubMarketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstitClubMarketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitClubMarketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
