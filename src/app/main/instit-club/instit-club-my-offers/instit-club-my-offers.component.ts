import { Component, OnInit } from '@angular/core';
//Import service helper
import { ServiceHelperService } from '@serviceHelper/service-helper.service'
import { Constants } from '@helper/constants';

@Component({
  selector: 'app-instit-club-my-offers',
  templateUrl: './instit-club-my-offers.component.html',
  styleUrls: ['./instit-club-my-offers.component.scss']
})
export class InstitClubMyOffersComponent implements OnInit {

  displayedColumns: string[] = ['creationDate', 'amount', 'period', 'availableDate', 'rateIC', 'type', 'investorId', 'status'];
  private listOffers = [];
  limitPage = 10;
  pageIndex = 1;
  total = 0;
  infoEmission:any;
  constructor(private _serviceHelper: ServiceHelperService) { }

  ngOnInit() {
    this.getInformationInvest();
    this.getListOffres();
  }

  //Get information investments
  getInformationInvest() {
    this._serviceHelper.__post(Constants.GET_INFORMATION_INVES, {}).subscribe((res) => {
      this.infoEmission = res.data;
    },
      error => {
        console.log(error);
      });
  }
  //Get List offres
  getListOffres(_pageIndex = this.pageIndex, _limitPage = this.limitPage) {

    this._serviceHelper.__post(Constants.GET_ALL_INVESTMENTS_EM, { pageIndex: _pageIndex, limitPage: _limitPage }).subscribe((res) => {
      this.listOffers = res.data.docs;
      this.listOffers.forEach((el, index) => {

        //Get label type
        el.type.forEach((e, index) => {
          el.type[index] = Constants.ListTypeCard.find(t => t.value == e).label;
        });

        el.status = Constants.typeStatusEmission.find(s => s.value == el.status).label;
      });
      this.total = res.data.docs.total;
    },
      error => {
      });

  }
  /***********************
  *  Method Paginator
  **********************/
  getServerData(e) {
    this.getListOffres((e.pageIndex + 1), e.pageSize);
  }
}
