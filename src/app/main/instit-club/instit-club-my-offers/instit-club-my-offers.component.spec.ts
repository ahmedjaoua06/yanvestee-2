import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitClubMyOffersComponent } from './instit-club-my-offers.component';

describe('InstitClubMyOffersComponent', () => {
  let component: InstitClubMyOffersComponent;
  let fixture: ComponentFixture<InstitClubMyOffersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstitClubMyOffersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitClubMyOffersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
