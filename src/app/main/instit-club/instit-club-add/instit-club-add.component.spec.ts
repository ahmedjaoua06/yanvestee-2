import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitClubAddComponent } from './instit-club-add.component';

describe('InstitClubAddComponent', () => {
  let component: InstitClubAddComponent;
  let fixture: ComponentFixture<InstitClubAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstitClubAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitClubAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
