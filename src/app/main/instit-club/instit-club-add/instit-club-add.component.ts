import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators, FormArray, FormControl } from '@angular/forms';
//Import Service Helper
import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import { Constants } from '@helper/constants';
import Swal from 'sweetalert2'
import { Router } from '@angular/router';
//Import Model
import { User } from '@helper/models/user.model';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material';
import * as _ from "lodash";
@Component({
  selector: 'app-instit-club-add',
  templateUrl: './instit-club-add.component.html',
  styleUrls: ['./instit-club-add.component.scss']
})
export class InstitClubAddComponent implements OnInit {
  //Init Datable
  dataSource = new MatTableDataSource<User>();
  displayedColumns: string[] = ['select', 'firstname', 'lastname', 'email', 'tel'];
  minDate = new Date();
  maxDate = new Date();
  formAdd: FormGroup;
  ListType = [];
  listBlacklist = [];
  currentBlacklist = [];
  blackListTables = {
    table_1: new MatTableDataSource<User>(),
    table_2: new MatTableDataSource<User>(),
    table_3: new MatTableDataSource<User>(),
    table_4: new MatTableDataSource<User>(),
    table_5: new MatTableDataSource<User>()
  };
  constructor(
    private _formBuilder: FormBuilder,
    private _serviceHelper: ServiceHelperService,
    private _router: Router
  ) { }

  ngOnInit() {
    //Set min max Datepicker
    this.configDatepicker();
    //Init Form
    this.initForm();
    //Get List Role
    this.getListRole();
  }
  //Config Datepicker
  configDatepicker() {
    //Min Date
    this.minDate = new Date()
    this.minDate.setDate(this.minDate.getDate() + 1);
    //Max Date 15 days
    this.maxDate.setDate(this.minDate.getDate() + 14);

  }
  //Init Form
  initForm() {
    this.formAdd = this._formBuilder.group({
      amount: ['', Validators.required],
      period: ['', [Validators.required, Validators.min(1), Validators.max(365), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      availableDate: ['', Validators.required],
      type: [[], Validators.required],
      blacklist: this._formBuilder.array([]),
    });
  }
  //Get list Role
  getListRole() {
    this.listBlacklist = Constants.categoriesUser.filter(a => a.parent == 2);
  }
  //Event select block user
  selectBlockedUser(e) {
    if (e.isUserInput) {
      //Check if item selected
      if (e.source._selected) {
        this.currentBlacklist.push(e.source.value);
        this.getListUserByRole(e.source.value);
      }
      //Item uncheked
      else {
        //Get blacklist values
        this.currentBlacklist = this.currentBlacklist.filter(a => a != e.source.value);
        const control = <FormArray>this.formAdd.controls['blacklist'];
        let table = this.blackListTables["table_" + e.source.value.id];
        table.forEach(a => {
          //Remove elements from array
          const i = control.controls.findIndex(x => x === a);
          control.removeAt(i);
        });
      }
    }
  }
  //Get List User By Role
  getListUserByRole(role) {
    this._serviceHelper.__post(Constants.GET_LIST_USERS_INSTIT_CLUB, { categorie: role.value }).subscribe((res) => {
      this.blackListTables["table_" + role.id] = res.data;
    },
      (error) => {
        console.log(error)
      }
    );
  }
  //Event Add Compte Terme
  eventAdd(f) {
    Swal.fire({
      title: 'Voulez-vous confirmer votre publication du l"offre ?',
      type: 'success',
      showCancelButton: true,
      confirmButtonText: 'Oui, Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        if (f.valid) {
          this._serviceHelper.__post(Constants.CREATE_INSTIT_CLUB, f.value).subscribe((res: any) => {
            Swal.fire({
              position: 'top-end',
              type: 'success',
              title: 'Votre émission a été publiée avec succés',
              showConfirmButton: false,
              timer: 1500
            }).then(() => {
              this._router.navigate(["/instit-club/list"]);
            })
          },
            error => {
              console.log("Erreur");
              console.log("Error");
            });
        }
      }
    })
  }
  //Event Select blacklist user
  selectUserBlocked(e, element, i) {
    const control = <FormArray>this.formAdd.controls['blacklist'];
    //If element checked
    if (e.checked) {
      //Push element in array blacklist
      control.push(new FormControl(element._id));
    }
    //element unchecked
    else {
      //Remove element from array
      const i = control.controls.findIndex(x => x.value === element._id);
      control.removeAt(i);
    }
  }
  //Event change period
  changeListTypeCard(e) {
    this.ListType = Constants.ListTypeCard;

    let period = this.formAdd.get("period").value;
    let amount = this.formAdd.get("amount").value;

    //Remove BT CD
    if ((period % 10) != 0) {
      this.ListType = this.ListType.filter(a => a.value != 2 && a.value != 3);
    }
    //Remove CAT
    if (period < 90) {
      this.ListType = this.ListType.filter(a => a.value != 1);
    }
    //if amount != null
    if (amount != "" && amount != null) {
      //Remove BT
      if (!Number.isInteger((amount / 0.05))) {
        this.ListType = this.ListType.filter(a => a.value != 3);
      }
      //Remove CD
      if (!Number.isInteger((amount % 0.5))) {
        this.ListType = this.ListType.filter(a => a.value != 2);
      }
    }
  }
}