import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InstitClubRoutingModule } from './instit-club-routing.module';
import { InstitClubListComponent } from './instit-club-list/instit-club-list.component';
import { InstitClubAddComponent } from './instit-club-add/instit-club-add.component';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';

import { MomentModule } from 'ngx-moment';
import { CountdownModule } from 'ngx-countdown';

//Import material modules
import {
  MatButtonModule,
  MatChipsModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatButtonToggleModule,
  MatRippleModule,
  MatSelectModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatDialogModule,
  MatCheckboxModule,
  MatTooltipModule,
  MatDividerModule,
  MatSliderModule,
  MatDatepickerModule,
  MatCardModule,
  MatRadioModule,
  MatPaginatorIntl

} from '@angular/material';

import { InstitClubMarketComponent } from './instit-club-market/instit-club-market.component';
import { BlockedUserDialogComponent } from './blocked-user-dialog/blocked-user-dialog.component';
import { InstitClubDetailsComponent } from './instit-club-details/instit-club-details.component';
import { InstitClubMyOffersComponent } from './instit-club-my-offers/instit-club-my-offers.component';
import { InstitClubOutstandingComponent } from './instit-club-outstanding/instit-club-outstanding.component';



const FrenchRangeLabel = (page: number, pageSize: number, length: number) => {
  if (length == 0 || pageSize == 0) { return `0 de ${length}`; }

  length = Math.max(length, 0);

  const startIndex = page * pageSize;

  // If the start index exceeds the list length, do not try and fix the end index to the end.
  const endIndex = startIndex < length ?
    Math.min(startIndex + pageSize, length) :
    startIndex + pageSize;

  return `${startIndex + 1} - ${endIndex} de ${length}`;
}


export function getFrenchPaginatorIntl() {
  const paginatorIntl = new MatPaginatorIntl();

  paginatorIntl.itemsPerPageLabel = 'Articles par page:';
  paginatorIntl.nextPageLabel = 'Page suivante';
  paginatorIntl.previousPageLabel = 'Page précédente';
  paginatorIntl.getRangeLabel = FrenchRangeLabel;

  return paginatorIntl;
}


@NgModule({
  declarations: [InstitClubListComponent, InstitClubAddComponent, InstitClubMarketComponent, BlockedUserDialogComponent, InstitClubDetailsComponent, InstitClubMyOffersComponent, InstitClubOutstandingComponent],
  imports: [
    FuseSharedModule,
    FuseWidgetModule,
    MatButtonModule,
    MomentModule,
    MatChipsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatRippleModule,
    MatDialogModule,
    CountdownModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatRadioModule,
    MatTabsModule,
    MatCardModule,
    MatDividerModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatSliderModule,
    MatDatepickerModule,
    InstitClubRoutingModule
  ],
  entryComponents: [BlockedUserDialogComponent, InstitClubOutstandingComponent],
  providers: [
    { provide: MatPaginatorIntl, useValue: getFrenchPaginatorIntl() }
  ]
})
export class InstitClubModule { }
