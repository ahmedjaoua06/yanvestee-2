import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitClubDetailsComponent } from './instit-club-details.component';

describe('InstitClubDetailsComponent', () => {
  let component: InstitClubDetailsComponent;
  let fixture: ComponentFixture<InstitClubDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstitClubDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitClubDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
