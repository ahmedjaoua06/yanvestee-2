
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ActivatedRoute, Router } from '@angular/router';
import { Constants } from '@helper/constants';
import { Subject } from 'rxjs';
//Import service helper
import { ServiceHelperService } from '@serviceHelper/service-helper.service'
import { environment } from 'environments/environment';
import { FuseConfigService } from '@fuse/services/config.service';
import Swal from 'sweetalert2';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InstitClubOutstandingComponent } from '../instit-club-outstanding/instit-club-outstanding.component';
import { MatDialog } from '@angular/material';
@Component({
  selector: 'app-instit-club-details',
  templateUrl: './instit-club-details.component.html',
  styleUrls: ['./instit-club-details.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class InstitClubDetailsComponent implements OnInit {
  outstandingAmounts:any;
  isDisabled = false;
  // Private
  private _unsubscribeAll: Subject<any>;
  //Details Emission
  private emissionId: string;
  private emission: any;
  private urlLogo = "";
  // End Details Emission
  // Investment
  isEmissionOwner = false;
  investForm: FormGroup;
  ListType = [];
  isInvested = false;
  // End Investment
  // Details investments
  displayedColumns: string[] = ['firstname', 'rateIC', 'type', 'typerate', 'amountPercent', 'creationDate', 'status', 'Accepter/Refuser'];
  private listInvestment = [];
  limitPage = 10;
  pageIndex = 1;
  total = 0;
  // End Details investments
  constructor(
    private _formBuilder: FormBuilder,
    private _fuseConfigService: FuseConfigService,
    private _serviceHelper: ServiceHelperService,
    private _activateRoute: ActivatedRoute,
    private _router: Router,
    public dialog: MatDialog
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          folded: false
        }
      }
    };
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit() {
    //Get params
    this._activateRoute.queryParams.subscribe((params) => {
      this.emissionId = params.emissionId;
    });
    //Init Form
    this.intInvestForm();
    //Get emission details
    this.getEmission(this.emissionId).then(() => {
      this.bankupdateProfilEncours();
    });
    this.checkIcOwner(this.emissionId).then((res: any) => {
      this.isEmissionOwner = res.data;
      if (this.isEmissionOwner)
        this.getInvestments()
    }, () => {

      this.checkInvested(this.emissionId)

    });

  }
  //Init Form Investment
  intInvestForm() {
    this.investForm = this._formBuilder.group({
      rateIC: ['', [Validators.required, Validators.min(0), Validators.max(99.99)]],
      type: [[], Validators.required],
      typerate: [1, Validators.required],
      amountPercent: [75, [Validators.min(75), Validators.max(100)]],
      offreicID: [this.emissionId]
    });
  }
  //On change type rate
  onChangeTypeRate(e) {
    //Check if value 1 remove validator field else add validator
    e.value == '1' ? this.ClearValidatorField('amountPercent') : this.setValidatorField('amountPercent')
  }
  //Get emission by id
  getEmission(id) {
    return new Promise((resolve, reject) => {

      if (id) {
        this._serviceHelper.__post(Constants.GET_INSTIT_OFFRE_ID, { emissionId: id }).subscribe((res) => {
          //Set data emission
          this.emission = res.data;
          var defaultImage = res.data.investorId.role == Constants.emetteur ? Constants.default_image_user_em : Constants.default_image_user_iv;
          //Check if logo user exist
          if (this.emission.investorId.logo != undefined) {
            this.urlLogo = res.data.investorId.logo.path != undefined ? environment.api + res.data.investorId.logo.path : defaultImage;
          }
          else {
            this.urlLogo = defaultImage;
          }
          //Get Type
          this.emission.type.map((a, i) => {
            this.emission.type[i] = Constants.ListTypeCard.find(t => t.value == a).label;
            this.ListType.push(Constants.ListTypeCard.find(t => t.value == a));
          });
          resolve();
        },
          error => {
            console.log("GetEmission Error");
            console.log(error);
            reject();
          });
      }
    });

  }
  // Event Investment
  onInvestment(f) {

    Swal.fire({
      title: 'Voulez-vous confirmer votre offre ?',
      type: 'success',
      showCancelButton: true,
      confirmButtonText: 'Oui, Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        //Check if form valid
        if (f.valid) {

          if (f.value.typerate == '1')

            delete f.value.amountPercent;

          this._serviceHelper.__post(Constants.ADD_INVESTMENT_IC, f.value).subscribe((res: any) => {
            Swal.fire({
              position: 'top-end',
              type: 'success',
              title: 'Votre offre a éte envoyée avec succès',
              showConfirmButton: false,
              timer: 1500
            }).then(() => {
              this.isInvested = true;
            })
          },
            error => {

            });
        }
      }
    });

  }
  //Set validator required
  setValidatorField(field) {
    this.investForm.controls[field].setValidators([Validators.required]);
    this.investForm.controls[field].updateValueAndValidity();
  }
  //Clear Validator
  ClearValidatorField(field) {
    this.investForm.controls[field].clearValidators();
    this.investForm.controls[field].updateValueAndValidity();
  }
  //Check if current user is owner of IC offre
  checkIcOwner(idOffre) {
    return new Promise((resolve, reject) => {
      this._serviceHelper.__post(Constants.CHECK_OWNER_OFFRE, { offreId: idOffre }).subscribe((res) => {
        resolve(res)
      },
        error => {
          console.log(error);
          reject(error);
        })
    });

  }
  //Check if already invested
  checkInvested(idOffre) {
    this._serviceHelper.__post(Constants.CHECK_INVESTED, { offreId: idOffre }).subscribe((res) => {
      this.isInvested = res.data;
    },
      error => {

      })
  }
  //Accept offre
  acceptOrRefuseOffre(id, status) {

    let title = status == "3" ? "Acceptation de l'offre" : "Refus l'offre";
    Swal.fire({
      title: title,
      type: 'success',
      showCancelButton: true,
      confirmButtonText: 'Oui, Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this._serviceHelper.__post(Constants.ACCEPT_OR_REFUSE_OFFRE, { status: status, offreId: this.emissionId, idInvest: id }).subscribe((res) => {
          Swal.fire({
            position: 'top-end',
            type: 'success',
            title: status == "3" ? "Offre acceptée" : "Offre refusée",
            showConfirmButton: false,
            timer: 1500
          }).then(() => {
            this.isInvested = true;
            this.getInvestments();
          })
        },
          error => {
            console.log(error);
          });
      }
    });
  }
  //Get List investments
  getInvestments(_pageIndex = this.pageIndex, _limitPage = this.limitPage) {
    this._serviceHelper.__post(Constants.GET_LIST_INV_INVESTOR, { offreId: this.emissionId, pageIndex: _pageIndex, limitPage: _limitPage }).subscribe((res) => {
      this.listInvestment = res.data.docs;
      this.listInvestment.map(a => a.amountPercent = a.amountPercent != undefined ? a.amountPercent : '-');

      this.listInvestment.forEach((el, index) => {
        el.type.forEach((elt, index) => {
          el.type[index] = Constants.ListTypeCard.find(t => t.value == elt).label;
        });
        el.status = Constants.typeStatusEmission.find(s => s.value == el.status).label;
      })
      this.total = res.data.total;
    },
      error => {
        console.log(error);
      });
  }
  //Back
  backTo() {
    if (this.isEmissionOwner) {
      this._router.navigate(['/instit-club/list'])
    }
    else {
      this._router.navigate(['/instit-club'])
    }

  }
  /***********************
 *  Method Paginator
 **********************/
  getServerData(e) {
    this.getInvestments((e.pageIndex + 1), e.pageSize)
  }
  //vos encours
  bankupdateProfilEncours() {
    //Check if current user is bank
    debugger;
    if (this._serviceHelper.userProfil.role == "Emetteur") {
      this.datesCompare(this._serviceHelper.userProfil.outstandingAmounts);
    }
  }
  //Compare dates
  datesCompare(d) {
    this.outstandingAmounts = d;
    //Set time to 0
    d.updateDate = new Date(d.updateDate).setHours(0, 0, 0, 0);
    var currentDate = new Date().setHours(0, 0, 0, 0);
    debugger;
    if (d.updateDate < currentDate || d.outstandingEO == undefined) {
      this.isDisabled = true;
    }

  }
  //Show list blocked user
  showDialog() {
    this.dialog.open(InstitClubOutstandingComponent, {
      data: this.outstandingAmounts,
      height: '450px',
      width: '400px',
    }).afterClosed().subscribe(result => {
      if(result){
        this.isDisabled = false;
      }
    });
  }
}
