import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitClubListComponent } from './instit-club-list.component';

describe('InstitClubListComponent', () => {
  let component: InstitClubListComponent;
  let fixture: ComponentFixture<InstitClubListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstitClubListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitClubListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
