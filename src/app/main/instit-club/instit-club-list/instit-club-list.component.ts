import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Location } from '@angular/common';
import { MatSnackBar, PageEvent } from '@angular/material';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';
//Import Service Helper
import { ServiceHelperService } from '@serviceHelper/service-helper.service';
//Import Constant
import { Constants } from '@helper/constants';
//Components
import { BlockedUserDialogComponent } from '../blocked-user-dialog/blocked-user-dialog.component'
import * as moment from 'moment';
import { environment } from 'environments/environment';
@Component({
  selector: 'app-instit-club-list',
  templateUrl: './instit-club-list.component.html',
  styleUrls: ['./instit-club-list.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class InstitClubListComponent implements OnInit {
  pageEvent: PageEvent;
  datasourceInstit: any[];
  displayedColumns = ["amount", "period", "availableDate", "type", "nbrOffer", "creationDate", "endDate", "actions"];

  pageType: string;
  limitPage = 10;
  pageIndex = 1;
  total = 0;
  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _formBuilder: FormBuilder,
    private _location: Location,
    private _matSnackBar: MatSnackBar,
    private _serviceHelper: ServiceHelperService,
    private _router: Router,
    public dialog: MatDialog
  ) {

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {

    //Get List
    this.getListInstit();
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Get List instit club
   */
  getListInstit(_pageIndex = this.pageIndex, _limitPage = this.limitPage) {
    let _type = Constants.TypeListing.find(a => a.label == "BT").value;
    this._serviceHelper.__post(Constants.GET_LIST_IC_MY_OFFRE, { pageIndex: _pageIndex, limitPage: _limitPage }).subscribe((res) => {
      this.datasourceInstit = res.data.docs;
      this.datasourceInstit.map(a => {
        //Get label type
        a.type.forEach((el, index) => {
          a.type[index] = Constants.ListTypeCard.find(t => t.value == el).label;
        });
      });
      //Current date
      var currentDate = moment();
      this.datasourceInstit.forEach((el) => {
        var endEmission = moment(el.endEmission);
        //Set countdown to datasource
        var diffSec = endEmission.diff(currentDate, 'seconds');
        el.availableDate = diffSec;
      })
      //Check if logo exist
      this.datasourceInstit.map(a => {
        //get role investor
        a.investorId.logo = a.investorId.logo != undefined ? environment.api + a.investorId.logo.path : Constants.default_image_user_iv;
      })
      //Get Total
      this.total = res.data.total;
    },
      error => {
        console.log(error);
      })
  }

  //Redirect to add offre
  redirectToAdd() {
    this._router.navigate(['/instit-club/add']);
  }
  //Redirect to details
  detailsOffre(o) {
    this._router.navigate(['/instit-club/details'], { queryParams: { 'emissionId': o._id } });
  }
  /***********************
 *  Method Paginator
 **********************/
  getServerData(e) {
    this.getListInstit((e.pageIndex + 1), e.pageSize)
  }
  //Show list blocked user
  showListBlockedUser(el) {
    console.log(el);
    this.dialog.open(BlockedUserDialogComponent, {
      data: el.blacklist,
      height: '400px',
      width: '800px',
    });
  }
}