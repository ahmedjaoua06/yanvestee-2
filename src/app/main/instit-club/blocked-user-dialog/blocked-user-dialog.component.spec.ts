import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockedUserDialogComponent } from './blocked-user-dialog.component';

describe('BlockedUserDialogComponent', () => {
  let component: BlockedUserDialogComponent;
  let fixture: ComponentFixture<BlockedUserDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockedUserDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockedUserDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
