import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-blocked-user-dialog',
  templateUrl: './blocked-user-dialog.component.html',
  styleUrls: ['./blocked-user-dialog.component.scss']
})
export class BlockedUserDialogComponent implements OnInit {

  datasource: any[];
  displayedColumns = ["nom","role","categorie","email", "societe", "mobile"];

  
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }
  ngOnInit() {
    this.datasource = this.data;
  }

}
