import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatFormFieldModule, MatInputModule, MatDividerModule, MatIconModule, MatTabsModule, MatDatepickerModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { ProfileComponent } from './profile.component';
import { ProfileTimelineComponent } from './tabs/timeline/timeline.component';
import { ProfileAboutComponent } from './tabs/about/about.component';
import { ProfilePhotosVideosComponent } from './tabs/photos-videos/photos-videos.component';
import {MatSelectModule} from '@angular/material/select';
import {NgxMaskModule} from 'ngx-mask';
import { SecurityComponent } from './tabs/security/security.component';
import { InformationCompanyComponent } from './tabs/information-company/information-company.component';
import { InformationProComponent } from './tabs/information-pro/information-pro.component';
import { EncoursComponent } from './tabs/encours/encours.component'



const routes = [
    {
        path: '',
        component: ProfileComponent
    }
];

@NgModule({
    declarations: [
        ProfileComponent,
        ProfileTimelineComponent,
        ProfileAboutComponent,
        ProfilePhotosVideosComponent,
        SecurityComponent,
        InformationCompanyComponent,
        InformationProComponent,
        EncoursComponent,
    ],
    imports: [
        RouterModule.forChild(routes),
        MatDatepickerModule,
        MatButtonModule,
        MatDividerModule,
        MatIconModule,
        MatTabsModule,
        MatFormFieldModule,
        MatInputModule,
        FuseSharedModule,
        MatSelectModule,
        NgxMaskModule.forRoot()

    ]
})
export class ProfileModule {
}
