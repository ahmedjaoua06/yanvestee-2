import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';

import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import { ProfileService } from '../../service/profile.service';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { AuthenticationServiceService } from '@serviceHelper/authentication.service.service';
import { Constants } from '@helper/constants';

import Swal from 'sweetalert2';
@Component({
    selector: 'app-encours',
    templateUrl: './encours.component.html',
    styleUrls: ['./encours.component.scss']
})
export class EncoursComponent implements OnInit {
    isEmetteur = false;
    showFormUser = false;
    showFormProUser = false;
    formChangePassword: FormGroup;
    formInformationUser: FormGroup;
    formInformationProUser: FormGroup;
    user: any;
    VerifAgenceDeNotationLongTerme: boolean;
    VerifAgenceDeNotationCourtTerme: boolean;
    VerifAgenceDeNotationLongTermeMooday: boolean;
    VerifAgenceDeNotationCourtTermeMooday: boolean;
    // Private
    private _unsubscribeAll: Subject<any>;
    ListRatingForm = Constants.ListRatingForm;
    RatingFormLongFitch_SP = Constants.RatingFormLongFitch_SP;
    RatingFormCourtFitch_SP = Constants.RatingFormCourtFitch_SP;

    RatingFormPerspective = Constants.RatingFormPerspective;
    ListSecteur = Constants.ListSecteur;
    RatingFormCourtMoody = Constants.RatingFormCourtMoody;
    RatingFormLongMoody = Constants.RatingFormLongMoody;
    FormAddNewInfo: FormGroup;
    //Form outStanding
    formOutStanding: FormGroup;
    showFormOutStadingUser = false;
    /**
     * Constructor
     *
     * 
     */
    constructor(
        private _serviceHelper: ServiceHelperService,
        private _serviceProfile: ProfileService,
        private _formBuilder: FormBuilder,
        private authService: AuthenticationServiceService,

    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        //Get User
        this._serviceProfile.getUserProfile().subscribe((res: any) => {
            //Set User
            this._serviceProfile.user = res;
            this.isEmetteur = res.role != 'Emetteur' ? false : true;
            if (this.isEmetteur)
                this.initFormOutStanding();

            //Check if exist values encours
            if (res.outstandingAmounts.outstandingEO != undefined && res.outstandingAmounts.outstandingBT && res.outstandingAmounts.outstandingCT && res.outstandingAmounts.outstandingCD) {
                this.formOutStanding.patchValue({
                    outstandingEO: res.outstandingAmounts.outstandingEO,
                    outstandingBT: res.outstandingAmounts.outstandingBT,
                    outstandingCT: res.outstandingAmounts.outstandingCT,
                    outstandingCD: res.outstandingAmounts.outstandingCD,
                });
            }
        },
            error => {
                console.log("Error");
            })

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    //Init Form outStanding
    initFormOutStanding() {
        this.formOutStanding = this._formBuilder.group({
            outstandingEO: ['', [Validators.required, Validators.min(1), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
            outstandingBT: ['', [Validators.required, Validators.min(1), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
            outstandingCT: ['', [Validators.required, Validators.min(1), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
            outstandingCD: ['', [Validators.required, Validators.min(1), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]]
        });
    }
    //Event save change outStanding
    eventOutStandingProfil(f) {
        Swal.fire({
            title: 'Mise à jour des encours',
            type: 'success',
            showCancelButton: true,
            confirmButtonText: 'Oui, Confirmer',
            cancelButtonText: 'Annuler'
        }).then((result) => {
            if (result.value) {
                if (f.valid) {
                    this._serviceHelper.__post(Constants.UPDATE_OUT_STANDING, { outstandingAmounts: f.value }).subscribe((res: any) => {
                        Swal.fire({
                            position: 'top-end',
                            type: 'success',
                            title: 'mise a jour avec succès',
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                            this._serviceProfile.user.outstandingAmounts = res.data.outstandingAmounts;
                            this.showFormOutStadingUser = false;
                        })
                    },
                        error => {
                            console.log("Erreur");
                            console.log("Error");
                        });
                }
            }
        })
    }
    //Mark Form Touched
    private markFormGroupTouched(formGroup: FormGroup) {
        (<any>Object).values(this.formInformationUser.controls).forEach(control => {
            control.markAsTouched();

            if (control.controls) {
                this.markFormGroupTouched(control);
            }
        });
    }
}
