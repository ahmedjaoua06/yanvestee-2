import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';

import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import { ProfileService } from '../../service/profile.service';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { AuthenticationServiceService } from '@serviceHelper/authentication.service.service';
import { Constants } from '@helper/constants';

import Swal from 'sweetalert2';

@Component({
    selector: 'profile-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ProfileAboutComponent implements OnInit, OnDestroy {
    isbank = false;
    showFormUser = false;
    showFormProUser = false;
    formChangePassword: FormGroup;
    formInformationUser: FormGroup;
    formInformationProUser: FormGroup;
    user: any;
    VerifAgenceDeNotationLongTerme: boolean;
    VerifAgenceDeNotationCourtTerme: boolean;
    VerifAgenceDeNotationLongTermeMooday: boolean;
    VerifAgenceDeNotationCourtTermeMooday: boolean;
    // Private
    private _unsubscribeAll: Subject<any>;
    ListRatingForm = Constants.ListRatingForm;
    RatingFormLongFitch_SP = Constants.RatingFormLongFitch_SP;
    RatingFormCourtFitch_SP = Constants.RatingFormCourtFitch_SP;

    RatingFormPerspective = Constants.RatingFormPerspective;
    ListSecteur = Constants.ListSecteur;
    RatingFormCourtMoody = Constants.RatingFormCourtMoody;
    RatingFormLongMoody = Constants.RatingFormLongMoody;
    FormAddNewInfo: FormGroup;
    //Form outStanding
    formOutStanding: FormGroup;
    showFormOutStadingUser = false;
    /**
     * Constructor
     *
     * 
     */
    constructor(
        private _serviceHelper: ServiceHelperService,
        private _serviceProfile: ProfileService,
        private _formBuilder: FormBuilder,
        private authService: AuthenticationServiceService,

    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        //Init Form change password
        this.initFormPassword();
        //Get User
        this._serviceProfile.getUserProfile().subscribe((res: any) => {
            console.log(res);
            //Set User
            this._serviceProfile.user = res;
            this.isbank = res.categorie != 'Banque' ? false : true;
            if (this.isbank)
                this.initFormOutStanding();


            //Check if exist values encours
            if (res.outstandingAmounts.outstandingEO != undefined && res.outstandingAmounts.outstandingBT && res.outstandingAmounts.outstandingCT && res.outstandingAmounts.outstandingCD) {
                this.formOutStanding.patchValue({
                    outstandingEO: res.outstandingAmounts.outstandingEO,
                    outstandingBT: res.outstandingAmounts.outstandingBT,
                    outstandingCT: res.outstandingAmounts.outstandingCT,
                    outstandingCD: res.outstandingAmounts.outstandingCD,
                });
            }


            //Init Form Profil
            this.initFormProfil(res);
            this.updateFormProfil(res);
            console.log(res);
            if (res.agencenotation != null && res.agencenotation != undefined) {
                this.SelectAgenceDeNotation(res.agencenotation);
            }
        },
            error => {
                console.log("Error");
            })
        this.VerifAgenceDeNotationLongTerme = false;
        this.VerifAgenceDeNotationCourtTerme = false;
        this.VerifAgenceDeNotationLongTermeMooday = false;
        this.VerifAgenceDeNotationCourtTermeMooday = false;

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    //Init Form Change Profil
    initFormProfil(res) {
        //Init data
        this.formInformationUser = this._formBuilder.group({
            societe: ['', Validators.required],
            firstname: ['', Validators.required],
            lastname: ['', Validators.required],
            tel: ['', [Validators.required, Validators.minLength(8)]],
            mobile: ['', [Validators.required, Validators.minLength(8)]],
        });
        this.formInformationProUser = this._formBuilder.group({
            //Set personnal information
            societe: [res.societe],
            firstname: [res.firstname],
            lastname: [res.firstname],
            tel: [res.tel],
            mobile: [res.mobile],

            agencenotation: ['', Validators.required],
            ratinglong: ['', Validators.required],
            ratingcourt: ['', Validators.required],
            perspective: ['', Validators.required],
            infoJuridicForm: ['', [Validators.required, Validators.maxLength(50)]],
            sharecapital: ['', [Validators.required, Validators.maxLength(50)]],
            mf: ['', Validators.required],
            lei: [''],
            secteur: ['', [Validators.required, Validators.maxLength(100)]],
            adresse: ['', [Validators.required, Validators.maxLength(100)]],
            website: ['', [Validators.required, Validators.pattern(new RegExp(/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/))]],

        });
    }
    //Init Form outStanding
    initFormOutStanding() {
        this.formOutStanding = this._formBuilder.group({
            outstandingEO: ['', [Validators.required, Validators.min(1), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
            outstandingBT: ['', [Validators.required, Validators.min(1), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
            outstandingCT: ['', [Validators.required, Validators.min(1), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
            outstandingCD: ['', [Validators.required, Validators.min(1), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]]
        });
    }
    //Update Form Profil
    updateFormProfil(values) {
        //Set data information user
        this.formInformationUser.patchValue({
            societe: values.societe,
            firstname: values.firstname,
            lastname: values.lastname,
            tel: values.tel,
            mobile: values.mobile,

        });

        //Set data information pro user
        this.formInformationProUser.patchValue({
            agencenotation: values.agencenotation,
            ratinglong: values.ratinglong,
            ratingcourt: values.ratingcourt,
            perspective: values.perspective,
            infoJuridicForm: values.infoJuridicForm,
            sharecapital: values.sharecapital,
            mf: values.mf,
            lei: values.lei,
            secteur: values.secteur,
            adresse: values.adresse,
            website: values.values
        });
    }
    //Event Change Profil
    eventChangeProfil(f) {
        if (f.valid) {
            this._serviceHelper.__put('users/profile', '', f.value).subscribe((res: any) => {
                //Check if message exist
                if (res.message) {
                    //Show sucess alert
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        title: res.message,
                        showConfirmButton: false,
                        timer: 2000
                    }).then((result) => {
                        this.showFormUser = false;
                        this.showFormProUser = false;
                        this._serviceProfile.user = res.data;
                    });
                }
            },
                error => {
                    if (error.error.error) {
                        Swal.fire({
                            title: 'Erreur',
                            text: error.error.error,
                            type: 'error'
                        });
                    }
                });
        }

    }
    //Init Form Change Password
    initFormPassword() {
        this.formChangePassword = this._formBuilder.group({
            password: ['', [Validators.required, Validators.minLength(5), Validators.pattern('[a-z]+[A-Z]+[0-9]+|[A-Z]+[a-z]+[0-9]+|[0-9]+[A-Z]+[a-z]+|[0-9]+[a-z]+[A-Z]+|[A-Z]+[0-9]+[a-z]+|[a-z]+[0-9]+[A-Z]+')]],
            passwordConfirm: ['', [Validators.required, confirmPasswordValidator]]
        });
    }
    //Event Change Password
    eventChangePassword(f) {
        if (f.valid) {
            delete f.value.passwordConfirm;
            this._serviceHelper.__post("users/updatePassword", f.value).subscribe((res: any) => {
                //Check if message exist
                if (res.message) {
                    //Show sucess alert
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        title: res.message,
                        showConfirmButton: false,
                        timer: 2000
                    }).then((result) => {
                    });
                }
            },
                error => {
                    if (error.error.error) {
                        Swal.fire({
                            title: 'Erreur',
                            text: error.error.error,
                            type: 'error'
                        });
                    }
                });
        }
    }
    //Event save change outStanding
    eventOutStandingProfil(f) {
        Swal.fire({
            title: 'Mise à jour des encours',
            type: 'success',
            showCancelButton: true,
            confirmButtonText: 'Oui, Confirmer',
            cancelButtonText: 'Annuler'
        }).then((result) => {
            if (result.value) {
                if (f.valid) {
                    this._serviceHelper.__post(Constants.UPDATE_OUT_STANDING, { outstandingAmounts: f.value }).subscribe((res: any) => {
                        Swal.fire({
                            position: 'top-end',
                            type: 'success',
                            title: 'mise a jour avec succès',
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                            this._serviceProfile.user.outstandingAmounts = res.data.outstandingAmounts;
                            this.showFormOutStadingUser = false;
                        })
                    },
                        error => {
                            console.log("Erreur");
                            console.log("Error");
                        });
                }
            }
        })
    }
    //Mark Form Touched
    private markFormGroupTouched(formGroup: FormGroup) {
        (<any>Object).values(this.formInformationUser.controls).forEach(control => {
            control.markAsTouched();

            if (control.controls) {
                this.markFormGroupTouched(control);
            }
        });
    }
    //Event Show Form User
    eventShowForm() {
        this.showFormUser = !this.showFormUser;
        //Get User
        this._serviceProfile.getUserProfile().subscribe((res) => {
            //Set User
            this._serviceProfile.user = res;
            this.updateFormProfil(res);
        },
            error => {
                console.log("Error");
            })

    }
    SelectAgenceDeNotation(value) {
        console.log(value)
        if (value == "Fitch" || value == "S&P") {
            this.VerifAgenceDeNotationLongTerme = true;
            this.VerifAgenceDeNotationCourtTerme = true;
            this.VerifAgenceDeNotationLongTermeMooday = false;
            this.VerifAgenceDeNotationCourtTermeMooday = false;

        } else if (value == "Moody's") {
            this.VerifAgenceDeNotationLongTerme = false;
            this.VerifAgenceDeNotationCourtTerme = false;
            this.VerifAgenceDeNotationLongTermeMooday = true;
            this.VerifAgenceDeNotationCourtTermeMooday = true;

        }

    }
}
/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return { 'passwordsNotMatching': true };

};
