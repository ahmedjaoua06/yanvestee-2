import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';

import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import { ProfileService } from '../../service/profile.service';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { AuthenticationServiceService } from '@serviceHelper/authentication.service.service';
import { Constants } from '@helper/constants';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.scss']
})
export class SecurityComponent implements OnInit {
  formChangePassword: FormGroup;
  private _unsubscribeAll: Subject<any>;
  /**
   * Constructor
   *
   * 
   */
  constructor(
      private _serviceHelper: ServiceHelperService,
      private _formBuilder: FormBuilder,

  ) {
      // Set the private defaults
      this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
      //Init Form change password
      this.initFormPassword();
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }
  //Init Form Change Password
  initFormPassword() {
      this.formChangePassword = this._formBuilder.group({
          password: ['', [Validators.required, Validators.minLength(5), Validators.pattern('[a-z]+[A-Z]+[0-9]+|[A-Z]+[a-z]+[0-9]+|[0-9]+[A-Z]+[a-z]+|[0-9]+[a-z]+[A-Z]+|[A-Z]+[0-9]+[a-z]+|[a-z]+[0-9]+[A-Z]+')]],
          passwordConfirm: ['', [Validators.required, confirmPasswordValidator]]
      });
  }
  //Event Change Password
  eventChangePassword(f) {
      if (f.valid) {
          delete f.value.passwordConfirm;
          this._serviceHelper.__post("users/updatePassword", f.value).subscribe((res: any) => {
              //Check if message exist
              if (res.message) {
                  //Show sucess alert
                  Swal.fire({
                      position: 'center',
                      type: 'success',
                      title: res.message,
                      showConfirmButton: false,
                      timer: 2000
                  }).then((result) => {
                  });
              }
          },
              error => {
                  if (error.error.error) {
                      Swal.fire({
                          title: 'Erreur',
                          text: error.error.error,
                          type: 'error'
                      });
                  }
              });
      }
  }
}
/**
* Confirm password validator
*
* @param {AbstractControl} control
* @returns {ValidationErrors | null}
*/
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

  if (!control.parent || !control) {
      return null;
  }

  const password = control.parent.get('password');
  const passwordConfirm = control.parent.get('passwordConfirm');

  if (!password || !passwordConfirm) {
      return null;
  }

  if (passwordConfirm.value === '') {
      return null;
  }

  if (password.value === passwordConfirm.value) {
      return null;
  }

  return { 'passwordsNotMatching': true };

};