import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';

import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import { ProfileService } from '../../service/profile.service';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { AuthenticationServiceService } from '@serviceHelper/authentication.service.service';
import { Constants } from '@helper/constants';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-information-pro',
  templateUrl: './information-pro.component.html',
  styleUrls: ['./information-pro.component.scss']
})
export class InformationProComponent implements OnInit {
  isbank = false;
  showFormUser = false;
  showFormProUser = false;
  showFormCompany = false;
  formInformationCompte: FormGroup;
  formInformationProUser: FormGroup;
  formInformationCompany: FormGroup;
  user: any;
  // Private
  private _unsubscribeAll: Subject<any>;
  RatingFormPerspective = Constants.RatingFormPerspective;
  ListSecteur = Constants.ListSecteur;
  /**
   * Constructor
   *
   * 
   */
  constructor(
    private _serviceHelper: ServiceHelperService,
    private _serviceProfile: ProfileService,
    private _formBuilder: FormBuilder,
    private authService: AuthenticationServiceService,

  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    //Get User
    this._serviceProfile.getUserProfile().subscribe((res: any) => {
      console.log(res);
      //Set User
      this._serviceProfile.user = res;
      this.isbank = res.categorie != 'Banque' ? false : true;


      //Init Form Profil
      this.initFormProfil(res);
      this.updateFormProfil(res);
    },
      error => {
        console.log("Error");
      })
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
  //Init Form Change Profil
  initFormProfil(res) {
    //Init form information pro user
    this.formInformationProUser = this._formBuilder.group({
      infoDateNotation: [res.infoDateNotation],
      agencenotation: [res.agencenotation],
      ratinglong: [res.ratinglong],
      ratingcourt: [res.ratingcourt],
      perspective: [res.perspective],
      infoJuridicForm: [res.infoJuridicForm],
      sharecapital: [res.sharecapital],
      mf: [res.mf],
      lei: [res.lei],
      secteur: [res.secteur],
      website: [res.website],
      adresse: [res.adresse],
      societe: [res.societe],

      //Set personnal information
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      tel: ['', [Validators.required, Validators.minLength(8)]],
      mobile: ['', [Validators.required, Validators.minLength(8)]],
    });
    //Init form information account
    this.formInformationCompte = this._formBuilder.group({
      infoDateNotation: [res.infoDateNotation],
      agencenotation: [res.agencenotation],
      ratinglong: [res.ratinglong],
      ratingcourt: [res.ratingcourt],
      perspective: [res.perspective],
      infoJuridicForm: [res.infoJuridicForm],
      sharecapital: [res.sharecapital],
      mf: [res.mf],
      lei: [res.lei],
      firstname: [res.firstname],
      lastname: [res.lastname],
      tel: [res.tel],
      mobile: [res.mobile],
      secteur: [res.secteur],
      adresse: [res.adresse],
      website: [res.website],
      //Set personnal information
      societe: ['', Validators.required],
    });
    //Init form information company
    this.formInformationCompany = this._formBuilder.group({
      infoDateNotation: [res.infoDateNotation],
      agencenotation: [res.agencenotation],
      ratinglong: [res.ratinglong],
      ratingcourt: [res.ratingcourt],
      perspective: [res.perspective],
      infoJuridicForm: [res.infoJuridicForm],
      sharecapital: [res.sharecapital],
      mf: [res.mf],
      lei: [res.lei],
      firstname: [res.firstname],
      lastname: [res.lastname],
      tel: [res.tel],
      mobile: [res.mobile],

      //Set company information
      societe: ['', Validators.required],
      secteur: ['', [Validators.required, Validators.maxLength(100)]],
      adresse: ['', [Validators.required, Validators.maxLength(100)]],
      website: ['', [Validators.required, Validators.pattern(new RegExp(/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/))]],
    });
  }

  //Update Form Profil
  updateFormProfil(values) {
    //Set data information pro user
    this.formInformationProUser.patchValue({
      firstname: values.firstname,
      lastname: values.lastname,
      tel: values.tel,
      mobile: values.mobile,
    });
    //Information company
    this.formInformationCompany.patchValue({
      societe: values.societe,
      secteur: values.secteur,
      adresse: values.adresse,
      website: values.website
    });
    //Information 
    this.formInformationCompte.patchValue({
      societe: values.societe
    });
  }
  //Event Show Form User
  eventShowForm(index) {
    if (index == 1) {
      this.showFormUser = !this.showFormUser;
    }
    else if (index == 2) {
      this.showFormCompany = !this.showFormCompany;
    }
    else {
      this.showFormProUser = !this.showFormProUser;
    }
    //Get User
    this._serviceProfile.getUserProfile().subscribe((res) => {
      //Set User
      this._serviceProfile.user = res;
      this.updateFormProfil(res);
    },
      error => {
        console.log("Error");
      })

  }
  //Event Change Profil
  eventChangeProfil(f) {
    if (f.valid) {
      this._serviceHelper.__put('users/profile', '', f.value).subscribe((res: any) => {
        //Check if message exist
        if (res.message) {
          //Show sucess alert
          Swal.fire({
            position: 'center',
            type: 'success',
            title: res.message,
            showConfirmButton: false,
            timer: 2000
          }).then((result) => {
            this.showFormUser = false;
            this.showFormProUser = false;
            this.showFormCompany = false;
            this._serviceProfile.user = res.data;
          });
        }
      },
        error => {
          if (error.error.error) {
            Swal.fire({
              title: 'Erreur',
              text: error.error.error,
              type: 'error'
            });
          }
        });
    }

  }
}