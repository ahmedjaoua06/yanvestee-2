import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationProComponent } from './information-pro.component';

describe('InformationProComponent', () => {
  let component: InformationProComponent;
  let fixture: ComponentFixture<InformationProComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformationProComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
