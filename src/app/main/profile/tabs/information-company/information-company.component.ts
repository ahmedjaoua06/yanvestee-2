import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';

import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import { ProfileService } from '../../service/profile.service';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { AuthenticationServiceService } from '@serviceHelper/authentication.service.service';
import { Constants } from '@helper/constants';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-information-company',
  templateUrl: './information-company.component.html',
  styleUrls: ['./information-company.component.scss']
})
export class InformationCompanyComponent implements OnInit {
  ListJuridicForm = Constants.ListJuridicForm;
  todayDate: Date;
  firstDate: Date;
  yesterdayDate: Date;

  formInformationCompanyRating: FormGroup;
  showFormCompanyRating = false;

  formInformationCompany: FormGroup;
  showFormCompany = false;
  user: any;
  VerifAgenceDeNotationLongTerme: boolean;
  VerifAgenceDeNotationCourtTerme: boolean;
  VerifAgenceDeNotationLongTermeMooday: boolean;
  VerifAgenceDeNotationCourtTermeMooday: boolean;
  // Private
  private _unsubscribeAll: Subject<any>;
  ListRatingForm = Constants.ListRatingForm;
  RatingFormLongFitch_SP = Constants.RatingFormLongFitch_SP;
  RatingFormCourtFitch_SP = Constants.RatingFormCourtFitch_SP;

  RatingFormPerspective = Constants.RatingFormPerspective;
  RatingFormCourtMoody = Constants.RatingFormCourtMoody;
  RatingFormLongMoody = Constants.RatingFormLongMoody;
  /**
   * Constructor
   *
   * 
   */
  constructor(
    private _serviceHelper: ServiceHelperService,
    private _serviceProfile: ProfileService,
    private _formBuilder: FormBuilder,
    private authService: AuthenticationServiceService,

  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.firstDate = new Date("01/01/1970");
		this.yesterdayDate = new Date(Date.now() - 864e5);
    //Get User
    this._serviceProfile.getUserProfile().subscribe((res: any) => {
      console.log(res);
      //Set User
      this._serviceProfile.user = res;

      //Init Form Profil
      this.initFormProfil(res);
      this.updateFormProfil(res);
      console.log(res);
      if (res.agencenotation != null && res.agencenotation != undefined) {
        this.SelectAgenceDeNotation(res.agencenotation);
      }
    },
      error => {
        console.log("Error");
      })
    this.VerifAgenceDeNotationLongTerme = false;
    this.VerifAgenceDeNotationCourtTerme = false;
    this.VerifAgenceDeNotationLongTermeMooday = false;
    this.VerifAgenceDeNotationCourtTermeMooday = false;

  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
  //Init Form Change Profil
  initFormProfil(res) {
    //Init data
    this.formInformationCompanyRating = this._formBuilder.group({
      societe: [res.societe],
      firstname: [res.firstname],
      lastname: [res.firstname],
      fonction: [res.fonction],
      tel: [res.tel],
      mobile: [res.mobile],
      infoJuridicForm: [res.infoJuridicForm],
      sharecapital: [res.sharecapital],
      mf: [res.mf],
      lei: [res.lei],
      secteur: [res.secteur],
      adresse: [res.adresse],
      website: [res.website],

      infoDateNotation: ['', Validators.required],
      agencenotation: ['', Validators.required],
      ratinglong: ['', Validators.required],
      ratingcourt: ['', Validators.required],
      perspective: ['', Validators.required]
    });

    this.formInformationCompany = this._formBuilder.group({
      fonction: [res.fonction],
      infoDateNotation: [res.infoDateNotation],
      secteur: [res.secteur],
      adresse: [res.adresse],
      website: [res.website],
      firstname: [res.firstname],
      lastname: [res.firstname],
      tel: [res.tel],
      mobile: [res.mobile],
      agencenotation: [res.agencenotation],
      ratinglong: [res.ratinglong],
      ratingcourt: [res.ratingcourt],
      perspective: [res.perspective],

      societe: ['', [Validators.required, Validators.maxLength(50)]],
      infoJuridicForm: ['', [Validators.required, Validators.maxLength(50)]],
      sharecapital: ['', [Validators.required, Validators.maxLength(50)]],
      mf: ['', Validators.required],
      lei: ['']
    });
  }
  //Update Form Profil
  updateFormProfil(values) {
    //Set data information user
    this.formInformationCompanyRating.patchValue({
      infoDateNotation: values.infoDateNotation,
      agencenotation: values.agencenotation,
      ratinglong: values.ratinglong,
      ratingcourt: values.ratingcourt,
      perspective: values.perspective
    });

    //Set data information pro user
    this.formInformationCompany.patchValue({
      societe: values.societe,
      infoJuridicForm: values.infoJuridicForm,
      sharecapital: values.sharecapital,
      mf: values.mf,
      lei: values.lei
    });
  }
  //Event Change Profil
  eventChangeProfil(f) {
    if (f.valid) {
      this._serviceHelper.__put('users/profile', '', f.value).subscribe((res: any) => {
        //Check if message exist
        if (res.message) {
          //Show sucess alert
          Swal.fire({
            position: 'center',
            type: 'success',
            title: res.message,
            showConfirmButton: false,
            timer: 2000
          }).then((result) => {
            this.showFormCompany = false;
            this.showFormCompanyRating = false;
            this._serviceProfile.user = res.data;
          });
        }
      },
        error => {
          if (error.error.error) {
            Swal.fire({
              title: 'Erreur',
              text: error.error.error,
              type: 'error'
            });
          }
        });
    }

  }
  SelectAgenceDeNotation(value) {
    console.log(value)
    if (value == "Fitch" || value == "S&P") {
      this.VerifAgenceDeNotationLongTerme = true;
      this.VerifAgenceDeNotationCourtTerme = true;
      this.VerifAgenceDeNotationLongTermeMooday = false;
      this.VerifAgenceDeNotationCourtTermeMooday = false;

    } else if (value == "Moody's") {
      this.VerifAgenceDeNotationLongTerme = false;
      this.VerifAgenceDeNotationCourtTerme = false;
      this.VerifAgenceDeNotationLongTermeMooday = true;
      this.VerifAgenceDeNotationCourtTermeMooday = true;

    }

  }
}