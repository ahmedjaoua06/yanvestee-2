import { Injectable } from '@angular/core';

import { ServiceHelperService } from '@serviceHelper/service-helper.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  user: any;
  constructor(
    private _serviceHelper: ServiceHelperService
  ) { }

  //Get User Profile
  getUserProfile() {
    return this._serviceHelper.__get("users/profile");
  }
  //Set User Information
  setUserInformation(user){
    this.user = user;
  }
}
