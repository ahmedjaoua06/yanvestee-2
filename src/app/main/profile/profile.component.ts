import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ProfileService } from './service/profile.service';
import { Constants } from '@helper/constants';
import { ToastrService } from 'ngx-toastr';
//Import service helper
import { ServiceHelperService } from '@serviceHelper/service-helper.service';
import { environment } from 'environments/environment';
@Component({
    selector: 'profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ProfileComponent implements OnInit {
    url = "assets/images/avatars/Velazquez.jpg";

    /**
     * Constructor
     */
    constructor(
        private _profileService: ProfileService,
        private _serviceHelper: ServiceHelperService,
        private _toastr: ToastrService
    ) {

    }
    ngOnInit() {
        this._profileService.getUserProfile().subscribe((res: any) => {
            var defaultImage = res.role == Constants.emetteur ? Constants.default_image_user_em : Constants.default_image_user_iv;

            if (res.logo != undefined) {
                this.url = res.logo.path != undefined ? environment.api + res.logo.path : defaultImage;
            }
            else {
                this.url = res.role == Constants.emetteur ? defaultImage : defaultImage;
            }
        });
    }
    //Convert File to base64
    convertFileToBase64(file) {
        let base64data: any = "";
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = (e) => {
            base64data = reader.result;
            this.url = base64data;
            this.eventSendLogo(base64data, file.name)
        };
    }
    //Event File Selected
    fileSelected(file) {
        let validSizeFile = false;
        let validTypeFile = false;
        let acceptType = ["png", "jpeg", "jpg"];
        //Check size image lte 2 Mo
        file[0].size <= 2048576 ? validSizeFile = true : (validSizeFile = false, this._toastr.warning("Taille du fichier invalide (max 2mo)"));
        //Check type image accept only png jpg jpeg
        let type = file[0].type.split('/')[1];
        type = acceptType.find(a => a == type.toLocaleLowerCase());
        type != undefined ? validTypeFile = true : (validTypeFile = false, this._toastr.warning("Type du fichier invalide (types du fichiers autorisés: png, jpeg, jpg)"));
        //Check if file valid
        if (validSizeFile && validTypeFile) {
            this.convertFileToBase64(file[0]);
        }
    }
    //Event send logo
    eventSendLogo(f, fileName) {
        console.log("=== eventSendLogo ===");
        console.log(f);
        this._serviceHelper.__post(Constants.USER_LOGO, { image: f, fileName: fileName }).subscribe((res) => {
            this._serviceHelper.userProfil.logo.path = f;
        },
            error => {
            });
    }
}
