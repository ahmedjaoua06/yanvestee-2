import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';

import { FuseConfigService } from '@fuse/services/config.service';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

import { navigation } from 'app/navigation/navigation';
import { AuthenticationServiceService } from '@serviceHelper/authentication.service.service';
import { Router } from '@angular/router';
import { ServiceHelperService } from '@serviceHelper/service-helper.service';
//Import momentjs
import * as moment from 'moment';
import { Constants } from '@helper/constants';
import { environment } from 'environments/environment';

@Component({
    selector: 'toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ToolbarComponent implements OnInit, OnDestroy {
    currentDate = moment();
    horizontalNavbar: boolean;
    rightNavbar: boolean;
    hiddenNavbar: boolean;
    languages: any;
    navigation: any;
    selectedLanguage: any;
    userStatusOptions: any[];
    role = "";
    categorie = "";
    user: any;
    userProfil: any;
    listNotifications = [];
    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {TranslateService} _translateService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _fuseSidebarService: FuseSidebarService,
        private _translateService: TranslateService,
        private _authService: AuthenticationServiceService,
        private _Router: Router,
        private _serviceHelper: ServiceHelperService
    ) {
        // Set the defaults
        this.userStatusOptions = [
            {
                'title': 'Online',
                'icon': 'icon-checkbox-marked-circle',
                'color': '#4CAF50'
            },
            {
                'title': 'Away',
                'icon': 'icon-clock',
                'color': '#FFC107'
            },
            {
                'title': 'Do not Disturb',
                'icon': 'icon-minus-circle',
                'color': '#F44336'
            },
            {
                'title': 'Invisible',
                'icon': 'icon-checkbox-blank-circle-outline',
                'color': '#BDBDBD'
            },
            {
                'title': 'Offline',
                'icon': 'icon-checkbox-blank-circle-outline',
                'color': '#616161'
            }
        ];

        this.languages = [
            {
                id: 'en',
                title: 'English',
                flag: 'us'
            },
            {
                id: 'tr',
                title: 'Turkish',
                flag: 'tr'
            },
            {
                id: 'fr',
                title: 'French',
                flag: 'fr'
            }
        ];

        this.navigation = navigation;

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this.initListNotification();
        this._authService.currentUser.subscribe((res) => {
            console.log(res)
            this.user = res;

            this.userProfil = this._serviceHelper.userProfil;

        });
        //Get Role
        this.role = localStorage.getItem("role");
        this.categorie = localStorage.getItem("categorie");
        // Subscribe to the config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((settings) => {
                this.horizontalNavbar = settings.layout.navbar.position === 'top';
                this.rightNavbar = settings.layout.navbar.position === 'right';
                this.hiddenNavbar = settings.layout.navbar.hidden === true;
            });

        // Set the selected language from default languages
        this.selectedLanguage = _.find(this.languages, { 'id': 'fr' });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle sidebar open
     *
     * @param key
     */
    toggleSidebarOpen(key): void {
        this._fuseSidebarService.getSidebar(key).toggleOpen();
    }

    /**
     * Search
     *
     * @param value
     */
    search(value): void {
        // Do your search here...
        console.log(value);
    }

    /**
     * Set the language
     *
     * @param lang
     */
    setLanguage(lang): void {
        // Set the selected language for the toolbar
        this.selectedLanguage = lang;

        // Use the selected language for translations
        this._translateService.use(lang.id);
    }
    //Logout User
    logout() {
        this._authService.logout();
    }
    profil() {
        this._Router.navigate(['/profil']);
    }
    //Redirect to notifications page

    goToNotifications() {
        this._Router.navigate(['/notifications']);
    }
    //Init List notification
    initListNotification() {
        this.listNotifications = [
            {
                username: 'Anis mnejja',
                seen: false,
                content: 'a publié une offer EO'
            },
            {
                username: 'Ahmed mzoughi',
                seen: true,
                content: 'a publié une offer EO'
            },
            {
                username: 'Saber mahbouli',
                seen: false,
                content: 'a publié une offer EO'
            },
            {
                username: 'Anis mnejja',
                seen: true,
                content: 'a publié une offer EO'
            }
        ]
    }
    //Event click item notif
    eventItemNotif(item) {
        var keyIc = item.notifId.key.split('_')[1];
        if (!item.seen) {
            item.seen = true;
            this._serviceHelper.__post(Constants.CHANGE_STATUS_NOTIF, { idNotif: item._id }).subscribe((res) => {

                if(keyIc == "IC"){
                    this._Router.navigateByUrl('/instit-club/details?emissionId=' + item.notifId.idEmission)
                }
                else{
                    this._Router.navigateByUrl('/marketPlace/emissionDetails?emissionId=' + item.notifId.idEmission)
                }

            },
                error => {

                });
        }
        else {
            if(keyIc == "IC"){
                this._Router.navigateByUrl('/instit-club/details?emissionId=' + item.notifId.idEmission)
            }
            else{
                this._Router.navigateByUrl('/marketPlace/emissionDetails?emissionId=' + item.notifId.idEmission)
            }
        }

    }
}
